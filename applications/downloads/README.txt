This is an alpha release of the Vanilla Downloads Application. It should not be used in a live environment and is in a preparation stage.

HOW TO INSTALL
1. Extract the "downloads" folder into the "/applications/" directory from the root of your Vanilla installation.
--- It will look like this: "/vanilla/applications/downloads/"

Once you extract the folder in, be sure to run the update script at "/utility/structure" through your web browser after you enable the application from the administrative dashboard.

//-----------------------------------------------------------------------------
// License: GPL2
//
// Copyright 2013  Livid Tech  (website : http://lividtech.com)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License, version 2, as 
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------