<?php if (!defined('APPLICATION')) exit();
/*
Copyright 2008, 2009 Vanilla Forums Inc.
This file is part of Garden.
Garden is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Garden is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Garden.  If not, see <http://www.gnu.org/licenses/>.
Contact Vanilla Forums Inc. at support [at] vanillaforums [dot] com
*/

class DownloadCommentModel extends Gdn_Model {
   public function __construct() {
      parent::__construct('DownloadComment');
   }
   
   public function DownloadCommentQuery() {
      $this->SQL->Select('c.*')
         ->Select('iu.Name', '', 'InsertName')
         ->Select('iu.Photo', '', 'InsertPhoto')
         ->From('DownloadComment c')
         ->Join('User iu', 'c.InsertUserID = iu.UserID', 'left');
   }
   
   public function Get($DownloadID, $Limit, $Offset = 0) {
      $this->DownloadCommentQuery();
      $this->FireEvent('BeforeGet');
      return $this->SQL
         ->Where('c.DownloadID', $DownloadID)
         ->OrderBy('c.DateInserted', 'asc')
         ->Limit($Limit, $Offset)
         ->Get();
   }
   
   public function GetID($DownloadCommentID) {
      $this->CommentQuery();
      return $this->SQL
         ->Where('c.DownloadCommentID', $DownloadCommentID)
         ->Get()
         ->FirstRow();
   }
   
   public function GetNew($DownloadID, $LastCommentID) {
      $this->CommentQuery(); 
      return $this->SQL
         ->Where('c.DownloadID', $DownloadID)
         ->Where('c.DownloadCommentID >', $LastCommentID)
         ->OrderBy('c.DateInserted', 'asc')
         ->Get();
   }
   
   /// <summary>
   /// Returns the offset of the specified comment in it's related discussion.
   /// </summary>
   /// <param name="CommentID" type="int">
   /// The comment id for which the offset is being defined.
   /// </param>
   public function GetOffset($DownloadCommentID) {
      return $this->SQL
         ->Select('c2.DownloadCommentID', 'count', 'CountComments')
         ->From('DownloadComment c')
         ->Join('Download a', 'c.DownloadID = a.DownloadID')
         ->Join('DownloadComment c2', 'a.DownloadID = c2.DownloadID')
         ->Where('c2.DownloadCommentID <=', $DownloadCommentID)
         ->Where('c.DownloadCommentID', $DownloadCommentID)
         ->Get()
         ->FirstRow()
         ->CountComments;
   }
   
   public function Save($FormPostValues) {
      $Session = Gdn::Session();
      
      // Define the primary key in this model's table.
      $this->DefineSchema();
      
      // Add & apply any extra validation rules:      
      $this->Validation->ApplyRule('Body', 'Required');
      $MaxCommentLength = Gdn::Config('Vanilla.Comment.MaxLength');
      if (is_numeric($MaxCommentLength) && $MaxCommentLength > 0) {
         $this->Validation->SetSchemaProperty('Body', 'Length', $MaxCommentLength);
         $this->Validation->ApplyRule('Body', 'Length');
      }
      
      $DownloadCommentID = ArrayValue('DownloadCommentID', $FormPostValues);
      $DownloadCommentID = is_numeric($DownloadCommentID) && $DownloadCommentID > 0 ? $DownloadCommentID : FALSE;
      $Insert = $DownloadCommentID === FALSE;
      if ($Insert)
         $this->AddInsertFields($FormPostValues);
      else
         $this->AddUpdateFields($FormPostValues);
      
      // Validate the form posted values
      if ($this->Validate($FormPostValues, $Insert)) {
         // If the post is new
         $Fields = $this->Validation->SchemaValidationFields();
         $Fields = RemoveKeyFromArray($Fields, $this->PrimaryKey);
         $DownloadID = ArrayValue('DownloadID', $Fields);
         if ($Insert === FALSE) {
            $this->SQL->Put($this->Name, $Fields, array('DownloadCommentID' => $DownloadCommentID));
         } else {
            // Make sure that the comments get formatted in the method defined by Garden
            $Fields['Format'] = Gdn::Config('Garden.InputFormatter', '');
            $DownloadCommentID = $this->SQL->Insert($this->Name, $Fields);
            
            // Notify any users who were mentioned in the comment
            $Usernames = GetMentions($Fields['Body']);
            $UserModel = Gdn::UserModel();
            foreach ($Usernames as $Username) {
               $User = $UserModel->GetByUsername($Username);
               if ($User && $User->UserID != $Session->UserID) {
                  AddActivity(
                     $Session->UserID,
                     'DownloadCommentMention',
                     '',
                     $User->UserID,
                     'download/'.$DownloadID.'/#Comment_'.$DownloadCommentID
                  );
               }
            }
         }
         // Record user-comment activity
         if ($DownloadID !== FALSE)
            $this->RecordActivity($DownloadID, $Session->UserID, $DownloadCommentID);
      }
      return $DownloadCommentID;
   }
      
   public function RecordActivity($DownloadID, $ActivityUserID, $DownloadCommentID) {
      // Get the author of the discussion
      $DownloadModel = new DownloadModel();
      $Download = $DownloadModel->GetID($DownloadID);
      if ($Download->InsertUserID != $ActivityUserID) 
         AddActivity(
            $ActivityUserID,
            'DownloadComment',
            '',
            $Download->InsertUserID,
            'download/'.$DownloadID.'/'.Gdn_Format::Url($Download->Name).'/#Comment_'.$DownloadCommentID
         );
   }
   
   public function Delete($DownloadCommentID) {
      $this->SQL->Delete('DownloadComment', array('DownloadCommentID' => $DownloadCommentID));
      return TRUE;
   }   
}