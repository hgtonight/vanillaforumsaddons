<?php if (!defined('APPLICATION')) exit();
/*
Copyright 2008, 2009 Vanilla Forums Inc.
This file is part of Garden.
Garden is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Garden is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Garden.  If not, see <http://www.gnu.org/licenses/>.
Contact Vanilla Forums Inc. at support [at] vanillaforums [dot] com
*/

require_once PATH_APPLICATIONS.'/dashboard/models/class.updatemodel.php';

class DownloadModel extends Gdn_Model {
   public static $Types = array(
          'plugin' => ADDON_TYPE_PLUGIN,
          'theme' => ADDON_TYPE_THEME,
          'locale' => ADDON_TYPE_LOCALE,
          'application' => ADDON_TYPE_APPLICATION,
          'core' => ADDON_TYPE_CORE
      );
   public static $TypesPlural = array(
          'plugins' => ADDON_TYPE_PLUGIN,
          'themes' => ADDON_TYPE_THEME,
          'locales' => ADDON_TYPE_LOCALE,
          'applications' => ADDON_TYPE_APPLICATION,
          'core' => ADDON_TYPE_CORE
      );

   protected $_DownloadCache = array();

   public function __construct() {
      parent::__construct('Download');
   }
   
   public function DownloadQuery($VersionSlug = FALSE) {
      $this->SQL
         ->Select('a.*')
         ->Select('t.Label', '', 'Type')
         ->Select('v.DownloadVersionID, v.File, v.Version, v.DateReviewed, v.TestedWith, v.MD5, v.FileSize')
         ->Select('v.DateInserted', '', 'DateUploaded')
         ->Select('iu.Name', '', 'InsertName')
         ->From('Download a')
         ->Join('DownloadType t', 'a.DownloadTypeID = t.DownloadTypeID')
         ->Join('User iu', 'a.InsertUserID = iu.UserID')
         ->Where('a.Visible', '1');

      if (!$VersionSlug) {
         // Join in the current download version.
         $this->SQL->Join('DownloadVersion v', 'a.CurrentDownloadVersionID = v.DownloadVersionID', 'left');
      } else {
         // Join in the version based on the slug.
         if (is_int($VersionSlug)) {
            $On = $this->SQL->ConditionExpr('v.DownloadVersionID', $VersionSlug);
         } else {
            $On = 'v.Deleted = 0 and a.DownloadID = v.DownloadID and '.$this->SQL->ConditionExpr('v.Version', $VersionSlug);
         }

         $this->SQL->Join('DownloadVersion v', $On, 'left');
      }
   }
   
   public static function JoinDownloads(&$Data, $Field = 'DownloadID', $Columns = array('Name')) {
      $Columns = array_merge(array('table' => 'Download', 'column' => 'Download'), $Columns);
      Gdn_DataSet::Join($Data, $Columns, array('unique' => TRUE));
   }

   public static function Slug($Download, $IncludeVersion = TRUE) {
      if (GetValue('DownloadKey', $Download) && (GetValue('Version', $Download) || !$IncludeVersion)) {
         $Key = GetValue('DownloadKey', $Download);
         $Type = GetValue('Type', $Download);
         if (!$Type) {
            $Type = GetValue(GetValue('DownloadTypeID', $Download), array_flip(self::$Types));
         }
         
         //$Slug = strtolower(GetValue('DownloadKey', $Data).'-'.GetValue('Type', $Data).'-'.GetValue('Version', $Data));
         $Slug = strtolower($Key).'-'.strtolower($Type);
         if ($IncludeVersion === TRUE)
            $Slug .= '-'.GetValue('Version', $Download, '');
         elseif (is_string($IncludeVersion))
            $Slug .= '-'.$IncludeVersion;
         elseif (is_array($IncludeVersion))
            $Slug .= '-'.$IncludeVersion['Version'];
         return urlencode($Slug);
      } else {
         return GetValue('DownloadID', $Download).'-'.Gdn_Format::Url(GetValue('Name', $Download));
      }
   }

   public function DeleteVersion($VersionID) {
      $this->SQL->Put('DownloadVersion', array('Deleted' => 1), array('DownloadVersionID' => $VersionID));
   }
   
   public function Get($Offset = '0', $Limit = '', $Wheres = '') {
      if ($Limit == '') 
         $Limit = Gdn::Config('Vanilla.Discussions.PerPage', 50);

      $Offset = !is_numeric($Offset) || $Offset < 0 ? 0 : $Offset;
      
      $this->DownloadQuery();
      
      if (is_array($Wheres))
         $this->SQL->Where($Wheres);

      return $this->SQL
         ->Limit($Limit, $Offset)
         ->Get();
   }

   /*
    * @return Gdn_DataSet
    */
   public function GetWhere($Where = FALSE, $OrderFields = '', $OrderDirection = 'asc', $Limit = FALSE, $Offset = FALSE) {
      $this->DownloadQuery();
      
      if ($Where !== FALSE)
         $this->SQL->Where($Where);

      if ($OrderFields != '')
         $this->SQL->OrderBy($OrderFields, $OrderDirection);

      if ($Limit !== FALSE) {
         if ($Offset == FALSE || $Offset < 0)
            $Offset = 0;

         $this->SQL->Limit($Limit, $Offset);
      }

      $Result = $this->SQL->Get();
      $this->SetCalculatedFields($Result);
      return $Result;
   }
   
   public function GetCount($Wheres = '') {
      if (!is_array($Wheres))
         $Wheres = array();
         
      $Wheres['a.Visible'] = '1';
      return $this->SQL
         ->Select('a.DownloadID', 'count', 'CountDownloads')
         ->From('Download a')
         ->Join('DownloadType t', 'a.DownloadTypeID = t.DownloadTypeID')
         ->Where($Wheres)
         ->Get()
         ->FirstRow()
         ->CountDownloads;
   }

   /**
    * Get an download by ID or key.
    *
    * @param int|array $DownloadID The download ID which can be one of the following:
    *  - int: The DownloadID.
    *  - array: An array where the first element is the download's key and the second element is the download type id.
    * @param bool $GetVersions Whether or not to get an array of all of the download's versions.
    * @return object The download.
    */
   public function GetID($DownloadID, $GetVersions = FALSE) {
      // Look for the download in the cache.
      foreach ($this->_DownloadCache as $CachedDownload) {
         if (is_array($DownloadID) && $CachedDownload['Key'] == $DownloadID[0] && $CachedDownload['Type'] == $DownloadID[1]) {
            $Download = $CachedDownload;
            break;
         } elseif (is_numeric($DownloadID) && $CachedDownload['DownloadID'] == $DownloadID) {
            $Download = $CachedDownload;
            break;
         }
      }

      if (isset($Download)) {
         $Result = $Download;
      } else {
         $this->DownloadQuery(GetValue(2, $DownloadID, FALSE));

         if (is_array($DownloadID))
            $this->SQL->Where(array('a.DownloadKey' => $DownloadID[0], 'a.DownloadTypeID' => $DownloadID[1]));
         else
            $this->SQL->Where('a.DownloadID', $DownloadID);

         $Result = $this->SQL->Get()->FirstRow(DATASET_TYPE_ARRAY);
         if (!$Result)
            return FALSE;


         $this->SetCalculatedFields($Result);
         $this->_DownloadCache[] = $Result;
      }

      if ($GetVersions && !isset($Result['Versions'])) {
         $Versions = $this->SQL->GetWhere('DownloadVersion', array('DownloadID' => GetValue('DownloadID', $Result), 'Deleted' => 0))->ResultArray();
         usort($Versions, array($this, 'VersionCompare'));

         foreach ($Versions as $Index => &$Version) {
            $this->SetCalculatedFields($Version);
         }

         $Result['Versions'] = $Versions;
      }

      return $Result;
   }

   public function GetIDs($IDs) {
      $DownloadTypeIDs = array();
      $DownloadIDs = array();

      // Loop through all of the IDs and parse them out.
      foreach ($IDs as $ID) {
         $Parts = explode('-', $ID, 3);

         if (is_numeric($Parts[0])) {
            $DownloadIDs[] = $Parts[0];
         } else {
            $Key = $Parts[0];
            $Type = GetValue(1, $Parts);
            if (isset(self::$Types[$Type])) {
               $DownloadTypeIDs[self::$Types[$Type]][] = $Key;
            }
         }
      }
      $Result = array();

      // Get all of the Downloads by ID.
      if (count($DownloadIDs) > 0) {
         $this->DownloadQuery();
         $Downloads = $this->SQL->WhereIn('a.DownloadID', $DownloadIDs)->Get()->Result();
         $Result = array_merge($Result, $Downloads);
      }

      // Get all of the Downloads by type.
      foreach ($DownloadTypeIDs as $TypeID => $Keys) {
         $this->DownloadQuery();
         $Downloads = $this->SQL
            ->Where('a.DownloadTypeID', $TypeID)
            ->WhereIn('a.DownloadKey', $Keys)
            ->Get()->Result();
         $Result = array_merge($Result, $Downloads);
      }

      $this->SetCalculatedFields($Result);
      $DataSet = new Gdn_DataSet($Result);
      return $DataSet;
   }

   /**
    * Get an download based on its slug in the following form:
    *  - DownloadID[-DownloadName]
    *  - DownloadType-DownloadKey[-Version]
    *
    * @param string|int $Slug The slug to lookup
    * @param bool $GetVersions Whether or not to add an array of versions to the result.
    * @return array
    */
   public function GetSlug($Slug, $GetVersions = FALSE) {
      if (is_numeric($Slug)) {
         $Download = $this->GetID($Slug, $GetVersions);
      } else {
         // This is a string identifier for the download.
         $Parts = explode('-', $Slug, 3);
         $Key = GetValue(0, $Parts);

         if (is_numeric($Key)) {
            $Download = $this->GetID($Key, $GetVersions);
         } else {
            $Type = strtolower(GetValue(1, $Parts));
            $TypeID = GetValue($Type, self::$Types, 0);
            $Version = GetValue(2, $Parts);

            $Download = $this->GetID(array($Key, $TypeID, $Version), $GetVersions);
         }
      }
      
      if (!$Download)
         return FALSE;

      if ($GetVersions) {
         // Find the latest stable version.
            $MaxVersion = GetValueR('Versions.0', $Download);
            foreach ($Download['Versions'] as $Version) {
               if (DownloadModel::IsReleaseVersion($Version['Version'])) {
                  $MaxVersion = $Version;
                  break;
               }
            }

            // Find the version we are looking at.
            foreach ($Download['Versions'] as $Version) {
               $Slug2 = DownloadModel::Slug($Download, $Version);
               if ($Slug2 == $Slug) {
                  $ViewingVersion = $Version;
                  break;
               }
            }
            if (!isset($ViewingVersion))
               $ViewingVersion = $MaxVersion;

            $Download['CurrentDownloadVersionID'] = $MaxVersion['DownloadVersionID'];
            $Download = array_merge($Download, $ViewingVersion);
            $Download['Slug'] = DownloadModel::Slug($Download, $ViewingVersion);
      }

      return $Download;
   }

   public function VersionCompare($A, $B) {
      return -version_compare(GetValue('Version', $A), GetValue('Version', $B));
   }

   public function GetVersion($VersionID) {
      $Result = $this->SQL
         ->Select('a.*')
         ->Select('v.DownloadVersionID, v.Version, v.File, v.MD5, v.FileSize, v.Checked')
         ->From('Download a')
         ->Join('DownloadVersion v', 'a.DownloadID = v.DownloadID')
         ->Where('v.DownloadVersionID', $VersionID)
         ->Get()->FirstRow(DATASET_TYPE_ARRAY);
      return $Result;
   }

   public function SetCalculatedFields(&$Data, $Unset = TRUE) {
      if (!$Data)
         return;

      if (is_a($Data, 'Gdn_DataSet')) {
         $this->SetCalculatedFields($Data->Result());
      } elseif (is_object($Data) || !isset($Data[0])) {
         $File = GetValue('File', $Data);
         SetValue('Url', $Data, Url("/uploads/$File", TRUE));
         $Icon = GetValue('Icon', $Data, '');
         if ($Icon) {
            SetValue('IconUrl', $Data, Gdn_Upload::Url($Icon));
         } else {
            SetValue('IconUrl', $Data, '');
         }

         if (GetValue('DownloadKey', $Data) && GetValue('Checked', $Data)) {
            $Slug = strtolower(GetValue('DownloadKey', $Data).'-'.GetValue('Type', $Data).'-'.GetValue('Version', $Data));
            SetValue('Slug', $Data, $Slug);
         }

         // Fix the icon path.
         $Icon = GetValue('Icon', $Data);
         if ($Icon && strpos($Icon, '/') == FALSE) {
            $Icon = 'ai'.$Icon;
            SetValue('Icon', $Data, $Icon);
         }

         // Set the requirements.
         if (GetValue('Checked', $Data)) {
            $Requirements = GetValue('Requirements', $Data);
            try {
               $Requirements = unserialize($Requirements);
               if (is_array($Requirements))
                  SetValue('Requirements', $Data, $Requirements);
            } catch (Exception $Ex) {
            }
         }

         if ($Unset) {
//            unset($Data['File']);
         }

      } else {
         foreach ($Data as &$Row) {
            $this->SetCalculatedFields($Row);
         }
      }
   }

   public static function IsReleaseVersion($VersionString) {
      return !preg_match('`[a-z]`i', $VersionString);
   }

   public function Save($Stub, $V1 = FALSE) {
      Trace('DownloadModel->Save()');
      
      $Session = Gdn::Session();

      $this->DefineSchema();

      // Most of the values come from the file itself.
      if (isset($Stub['Path'])) {
         $Path = $Stub['Path'];
      } elseif (GetValue('Checked', $Stub)) {
         $Download = $Stub;
      } elseif (isset($Stub['File'])) {
         $Path = CombinePaths(array(PATH_UPLOADS, $Stub['File']));
      } else {
         if (!$Session->CheckPermission('Downloads.Download.Manage') && isset($Stub['Filename'])) {
            // Only admins can modify plugin attributes without the file.
            $this->Validation->AddValidationResult('Filename', 'ValidateRequired');
            return FALSE;
         }
      }
      
      // Analyze and fix the file.
      if (!isset($Download)) {
         if (isset($Path) && !$V1) {
            try {
               $Download = UpdateModel::AnalyzeDownload($Path, FALSE);
            } catch (Exception $Ex) {
               $Download = FALSE;
               $this->Validation->AddValidationResult('File', '@'.$Ex->getMessage());
            }
            if (!is_array($Download)) {
               $this->Validation->AddValidationResult('File', 'Could not analyze the download file.');
               return FALSE;
            }
            $Download = array_merge($Stub, $Download);
         } else {
            $Download = $Stub;
            if (isset($Path)) {
               $Download['MD5'] = md5_file($Path);
               $Download['FileSize'] = filesize($Path);
            }
         }
      }

      // Get an existing download.
      if (isset($Download['DownloadID']))
         $CurrentDownload = $this->GetID($Download['DownloadID'], TRUE);
      elseif (isset($Download['DownloadKey']) && isset($Download['DownloadTypeID']))
         $CurrentDownload = $this->GetID(array($Download['DownloadKey'], $Download['DownloadTypeID']), TRUE);
      else
         $CurrentDownload = FALSE;
      
      Trace($CurrentDownload, 'CurentDownload');

      $Insert = !$CurrentDownload;
      if ($Insert)
         $this->AddInsertFields ($Download);

      $this->AddUpdateFields($Download); // always add update fields

      if (!$this->Validate($Download, $Insert)) {
         Trace('Download did not validate');
         return FALSE;
      }

      // Search for the current version.
      $MaxVersion = FALSE;
      $CurrentVersion = FALSE;
      if ($CurrentDownload && isset($Download['Version'])) {
         // Search for a current version.
         foreach ($CurrentDownload['Versions'] as $Index => $Version) {
            if (isset($Download['DownloadVersionID'])) {
               if ($Download['DownloadVersionID'] == $Version['DownloadVersionID'])
                  $CurrentVersion = $Version;
            } elseif (version_compare($Download['Version'], $Version['Version']) == 0) {
               $CurrentVersion = $Version;
            }

            // Only check for a current version if the version has been checked.
            if (!$Version['Checked'])
               continue;

            if (!$MaxVersion || version_compare($MaxVersion['Version'], $Version['Version'], '<')) {
               $MaxVersion = $Version;
            }
         }
      }

      // Save the download.
      $Fields = $this->FilterSchema($Download);
      if ($Insert) {
         $DownloadID = $this->SQL->Insert($this->Name, $Fields);
         
         // Add the activity.
         $ActivityModel = new ActivityModel();
         $Activity = array(
             'ActivityType' => 'Download',
             'ActivityUserID' => $Fields['InsertUserID'],
             'NotifyUserID' => ActivityModel::NOTIFY_PUBLIC,
             'HeadlineFormat' => '{ActivityUserID,user} added the <a href="{Url,html}">{Data.Name}</a> download.',
             'Story' => Gdn_Format::Html($Fields['Description']),
             'Route' => '/download/'.rawurlencode(self::Slug($Fields, FALSE)),
             'Data' => array('Name' => $Fields['Name'])
         );
         $ActivityModel->Save($Activity);
      } else {
         $DownloadID = GetValue('DownloadID', $CurrentDownload);

         // Only save the download if it is the current version.
         if (!$MaxVersion || version_compare($Download['Version'], $MaxVersion['Version'], '>=')) {
            Trace('Uploaded version is the most recent version.');
            $this->SQL->Put($this->Name, $Fields, array('DownloadID' => $DownloadID));
         } else {
            $this->SQL->Reset();
         }
      }

      // Save the version.
      if ($DownloadID && isset($Path) || isset($Download['File'])) {
         Trace('Saving download version');
         $Download['DownloadID'] = $DownloadID;
         
         if (isset($Path)) {
            if (!StringBeginsWith($Path, PATH_UPLOADS.'/downloads/')) {
               // The download must be copied into the uploads folder.
               $NewPath = PATH_UPLOADS.'/downloads/'.basename($Path);
               //rename($Path, $NewPath);
               $Path = $NewPath;
               $this->_DownloadCache = array();
            }
            $File = substr($Path, strlen(PATH_UPLOADS.'/'));
            $Download['File'] = $File;
         }

         if ($CurrentVersion) {
            $Download['DownloadVersionID'] = GetValue('DownloadVersionID', $CurrentVersion);
         }

         // Insert or update the version.
         $VersionModel = new Gdn_Model('DownloadVersion');
         $DownloadVersionID = $VersionModel->Save($Download);
         $this->Validation->AddValidationResult($VersionModel->ValidationResults());

         if (!$DownloadVersionID) {
            return FALSE;
         }

         // Update the current version in the download.
         if (!$MaxVersion || version_compare($CurrentDownload['Version'], $Download['Version'], '<')) {
            $this->SQL->Put($this->Name,
               array('CurrentDownloadVersionID' => $DownloadVersionID),
               array('DownloadID' => $DownloadID));
         }
      }
      $this->_DownloadCache = array();

      return $DownloadID;
   }
   
   public function SaveBak($FormPostValues, $FileName = '') {
      $Session = Gdn::Session();
      
      // Define the primary key in this model's table.
      $this->DefineSchema();

      if (array_key_exists('DownloadKey', $FormPostValues))
         $this->Validation->ApplyRule('DownloadKey', 'Required');
      
      // Add & apply any extra validation rules:
      if (array_key_exists('Description', $FormPostValues))
         $this->Validation->ApplyRule('Description', 'Required');

      if (array_key_exists('Version', $FormPostValues))
         $this->Validation->ApplyRule('Version', 'Required');
/*
      if (array_key_exists('TestedWith', $FormPostValues))
         $this->Validation->ApplyRule('TestedWith', 'Required');
*/      
      // Get the ID from the form so we know if we are inserting or updating.
      $DownloadID = ArrayValue('DownloadID', $FormPostValues, '');
      $Insert = $DownloadID == '' ? TRUE : FALSE;
      
      if ($Insert) {
         if(!array_key_exists('Vanilla2', $FormPostValues))
            $FormPostValues['Vanilla2'] = '0';
         
         unset($FormPostValues['DownloadID']);
         $this->AddInsertFields($FormPostValues);
      } else if (!array_key_exists('Vanilla2', $FormPostValues)) {
         $Tmp = $this->GetID($DownloadID);
         $FormPostValues['Vanilla2'] = $Tmp->Vanilla2 ? '1' : '0';
      }
      $this->AddUpdateFields($FormPostValues);
      // Validate the form posted values
      if ($this->Validate($FormPostValues, $Insert)) {
         $Fields = $this->Validation->SchemaValidationFields(); // All fields on the form that relate to the schema
         $DownloadID = intval(ArrayValue('DownloadID', $Fields, 0));
         $Fields = RemoveKeyFromArray($Fields, 'DownloadID'); // Remove the primary key from the fields for saving
         $Download = FALSE;
         $Activity = 'EditDownload';
         if ($DownloadID > 0) {
            $this->SQL->Put($this->Name, $Fields, array($this->PrimaryKey => $DownloadID));
         } else {
            $DownloadID = $this->SQL->Insert($this->Name, $Fields);
            $Activity = 'AddDownload';
         }
         // Save the version
         if ($DownloadID > 0 && $FileName != '') {
            // Save the download file & version
            $DownloadVersionModel = new Gdn_Model('DownloadVersion');
            $DownloadVersionID = $DownloadVersionModel->Save(array(
               'DownloadID' => $DownloadID,
               'File' => $FileName,
               'Version' => ArrayValue('Version', $FormPostValues, ''),
               'TestedWith' => ArrayValue('TestedWith', $FormPostValues, 'Empty'),
               'DateInserted' => Gdn_Format::ToDateTime()
            ));
            // Mark the new download file & version as the current version
            $this->SQL->Put($this->Name, array('CurrentDownloadVersionID' => $DownloadVersionID), array($this->PrimaryKey => $DownloadID));
         }
         
         if ($DownloadID > 0) {
            $Download = $this->GetID($DownloadID);

            // Record Activity
            AddActivity(
               $Session->UserID,
               $Activity,
               '',
               '',
               '/download/'.$DownloadID.'/'.Gdn_Format::Url($Download['Name'])
            );
         }
      }
      if (!is_numeric($DownloadID))
         $DownloadID = FALSE;
         
      return count($this->ValidationResults()) > 0 ? FALSE : $DownloadID;
   }
   
   public function SetProperty($DownloadID, $Property, $ForceValue = FALSE) {
      if ($ForceValue !== FALSE) {
         $Value = $ForceValue;
      } else {
         $Value = '1';
         $Download = $this->GetID($DownloadID);
         $Value = ($Download[$Property] == '1' ? '0' : '1');
      }
      $this->SQL
         ->Update('Download')
         ->Set($Property, $Value)
         ->Where('DownloadID', $DownloadID)
         ->Put();
      return $Value;
   }

   public function Validate($Post, $Insert) {
      $this->Validation->AddRule('DownloadKey', 'function:ValidateDownloadKey');

      if (GetValue('Checked', $Post) && ($Insert || isset($Post['DownloadKey']))) {
         $this->Validation->ApplyRule('DownloadKey', 'Required');
         $this->Validation->ApplyRule('DownloadKey', 'DownloadKey');
      }

      if ($Insert || isset($Post['Description']))
         $this->Validation->ApplyRule('Description', 'Required');

      if ($Insert || isset($Post['Version'])) {
         $this->Validation->ApplyRule('Version', 'Required');
         $this->Validation->ApplyRule('Version', 'Version');
      }

      parent::Validate($Post, $Insert);

      // Validate against an existing download.
      if ($DownloadID = GetValue('DownloadID', $Post)) {
         $CurrentDownload = $this->GetID($DownloadID, TRUE);
         if ($CurrentDownload) {
            if (GetValue('DownloadKey', $CurrentDownload) && isset($Post['DownloadKey']) && GetValue('DownloadKey', $Post) != GetValue('DownloadKey', $CurrentDownload))
               $this->Validation->AddValidationResult('DownloadKey', '@'.sprintf(T('The download\'s key cannot be changed. The uploaded file has a key of <b>%s</b>, but it must be <b>%s</b>.'), GetValue('DownloadKey', $Post), GetValue('DownloadKey', $CurrentDownload)));
            else {
               // Make sure this version doesn't match.
               foreach ($CurrentDownload['Versions'] as $Version) {
                  if ($Version['Deleted'])
                     continue;

                  if (version_compare(GetValue('Version', $Version), GetValue('Version', $Post)) == 0) {
                     // This version matches a previous version.
                     if (GetValue('Checked', $Version) && GetValue('MD5', $Version) != GetValue('MD5', $Post))
                        $this->Validation->AddValidationResult('Version', '@'.sprintf(T('Version %s of this download already exists.'), GetValue('Version', $Version)));
                  }
               }
            }
         }
      }

      // Make sure there isn't another download with the same key as this one.
      if (ValidateRequired(GetValue('DownloadKey', $Post))) {
         $CountSame = $this->GetCount(array('DownloadKey' => $Post['DownloadKey'], 'DownloadID <>' => GetValue('DownloadID', $Post), 'a.DownloadTypeID' => GetValue('DownloadTypeID', $Post)));
         if ($CountSame > 0) {
            $this->Validation->AddValidationResult('DownloadKey', '@'.sprintf(T('The download key %s is already taken.'), $Post['DownloadKey']));
         }
      }

      return count($this->Validation->Results()) == 0;
   }
      
   public function Delete($DownloadID) {
      $this->SetProperty($DownloadID, 'Visible', '0');
   }

   public function UpdateCurrentVersion($DownloadID) {
      $Download = $this->GetID($DownloadID, TRUE);

      $MaxVersion = FALSE;
      foreach ($Download['Versions'] as $Version) {
         if (!$Version['Checked'] || $Version['Deleted'])
            continue;
         if (!$MaxVersion || version_compare($Version['Version'], $MaxVersion['Version'], '>')) {
            $MaxVersion = $Version;
         }
      }
      if ($MaxVersion) {
         $this->SQL->History()->Put('Download', array('CurrentDownloadVersionID' => $MaxVersion->Version), array('DownloadID' => $DownloadID));
      }
   }
}

function ValidateDownloadKey($Value) {
	if (is_numeric($Value))
      return FALSE;
   elseif (preg_match('`[-,;:/]`', $Value) || strpos($Value, '\\') !== FALSE)
      return FALSE;
   return TRUE;
}