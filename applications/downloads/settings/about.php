<?php

$ApplicationInfo['Downloads'] = array(
   'Name' => 'Downloads for Vanilla Forum',
   'Description' => "A download application for Vanilla Forum.",
   'Version' => '0.0.1',
   'RegisterPermissions' => FALSE,
   'SetupController' => 'setup',
   'SettingsUrl' => 'dashboard/settings/downloads',
   'SettingsPermission' => 'Garden.Settings.Manage',
   'Author' => "Livid Tech",
   'AuthorEmail' => '',
   'AuthorUrl' => 'http://lividtech.com',
   'Url' => 'http://vanillaforums.org/addon/downloads-application',
   'License' => 'GPLv2'
);