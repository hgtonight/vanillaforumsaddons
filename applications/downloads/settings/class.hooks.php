<?php if (!defined('APPLICATION')) exit();
/*
Copyright 2008, 2009 Vanilla Forums Inc.
This file is part of Garden.
Garden is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Garden is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Garden.  If not, see <http://www.gnu.org/licenses/>.
Contact Vanilla Forums Inc. at support [at] vanillaforums [dot] com
*/


class DownloadsHooks implements Gdn_IPlugin {
   /**
    * DiscussionModel Get() additions.
    *
    * Note: DownloadID is manually added to DiscussionModel before Get() in DownloadController.
    */
//   public function DiscussionModel_BeforeGet_Handler($Sender) {
//      $DownloadID = GetValue('DownloadID', $Sender);
//      if (is_numeric($DownloadID) && $DownloadID > 0) {
//         // Filter discussion list to a particular DownloadID if present in model
//         $Sender->SQL->Where('DownloadID', $DownloadID);
//      }
//      else { 
//         // Make Download name available on discussions list
//         $Sender->SQL->Select('ad.Name', '', 'DownloadName')
//            ->Join('Download ad', 'd.DownloadID = ad.DownloadID', 'left');
//      }
//   }
   
   /**
    * Hook for discussion prefixes in /discussions.
    */
   public function Base_BeforeDiscussionMeta_Handler($Sender, $Args) {
      if (Gdn::Controller()->ControllerName == 'downloadcontroller')
         return;
      $this->DownloadDiscussionPrefix($Args['Discussion']);
   }
   
   /**
    * Add prefix to the passed controller's discussion names when they are re: an download.
    *
    * Ex: [DownloadName] Discussion original name
    */
   public function DownloadDiscussionPrefix($Discussion) {
      $Download = GetValue('Download', $Discussion);
      if ($Download) {
         $Slug = DownloadModel::Slug($Download, FALSE);
         $Url = "/download/$Slug";
         $DownloadName = GetValue('Name', $Download);
         echo ' '.Wrap(Anchor(Gdn_Format::Html($DownloadName), $Url), 'span', array('class' => 'Tag Tag-Download')).' ';
      }
   }
   
   // Write information about downloads to the discussion if it is related to an download
   public function DiscussionController_BeforeCommentBody_Handler($Sender) {
      $Discussion = GetValue('Object', $Sender->EventArguments);
      $DownloadID = GetValue('DownloadID', $Discussion);
      if (GetValue('Type', $Sender->EventArguments) == 'Discussion' && is_numeric($DownloadID) && $DownloadID > 0) {
         $Data = Gdn::Database()->SQL()->Select('Name')->From('Download')->Where('DownloadID', $DownloadID)->Get()->FirstRow();
         if ($Data) {
            echo '<div class="Warning">'.sprintf(T('This discussion is related to the %s download.'), Anchor($Data->Name, 'download/'.$DownloadID.'/'.Gdn_Format::Url($Data->Name))).'</div>';
         }
      }
   }
   
   /**
    *
    * @param DiscussionsController $Sender
    */
   public function DiscussionModel_AfterAddColumns_Handler($Sender, $Args) {
      DownloadModel::JoinDownloads($Args['Data'], 'DownloadID', array('Name', 'Icon', 'DownloadKey', 'DownloadTypeID', 'Checked'));
   }
   
   public function DiscussionsController_BeforeDiscussionContent_Handler($Sender, $Args) {
      static $DownloadModel = NULL;
      if (!$DownloadModel) $DownloadModel = new DownloadModel();
      
      $Discussion = $Args['Discussion'];
      $Download = GetValue('Download', $Discussion);
      if ($Download) {
         $Slug = DownloadModel::Slug($Download, FALSE);
         $Url = "/download/$Slug";
//         if ($Download['Icon']) {
//            echo Anchor(Img(Gdn_Upload::Url($Download['Icon'])), $Url, array('class' => 'Download-Icon Author'));
//         } else {
//            echo Wrap(Anchor('Download', $Url), 'span', array('class' => 'Tag Tag-Download'));
//         }
      }
   }
   
   // Pass the downloadid to the form
   public function PostController_Render_Before($Sender) {
      $DownloadID = GetIncomingValue('DownloadID');
      if ($DownloadID > 0 && is_object($Sender->Form))
         $Sender->Form->AddHidden('DownloadID', $DownloadID);
   }
   
   // Make sure to use the DownloadID when saving discussions if present in the url
   public function DiscussionModel_BeforeSaveDiscussion_Handler($Sender) {
      $DownloadID = GetIncomingValue('DownloadID');
      if (is_numeric($DownloadID) && $DownloadID > 0) {
         $FormPostValues = GetValue('FormPostValues', $Sender->EventArguments);
         $FormPostValues['DownloadID'] = $DownloadID;
         $Sender->EventArguments['FormPostValues'] = $FormPostValues;
      }
   }
   
   public function DiscussionModel_BeforeNotification_Handler($Sender, $Args) {
      $Discussion = $Args['Discussion'];
      $Activity = $Args['Activity'];
      
      if (!GetValue('DownloadID', $Discussion))
         return;
      
      $DownloadModel = new DownloadModel();
      $Download = $DownloadModel->GetID($Discussion['DownloadID'], DATASET_TYPE_ARRAY);
      
      if (GetValue('InsertUserID', $Download) == Gdn::Session()->UserID)
         return;
      
      $ActivityModel = $Args['ActivityModel'];
      $Activity['NotifyUserID'] = $Download['InsertUserID'];
      $Activity['HeadlineFormat'] = '{ActivityUserID,user} asked a <a href="{Url,html}">question</a> about the <a href="{Data.DownloadUrl,exurl}">{Data.DownloadName,html}</a> download.';
      $Activity['Data']['DownloadName'] = $Download['Name'];
      $Activity['Data']['DownloadUrl'] = '/download/'.urlencode(DownloadModel::Slug($Download, FALSE));
      
      $ActivityModel->Queue($Activity, 'DownloadComment');
   }
   
   // Make sure that all translations are in the GDN_Translation table for the "source" language
   public function Gdn_Locale_BeforeTranslate_Handler($Sender) {
      $Code = ArrayValue('Code', $Sender->EventArguments, '');
      if ($Code != '' && !in_array($Code, $this->GetTranslations())) {
         $Session = Gdn::Session();
         // If the code wasn't in the source list, insert it
         $Database = Gdn::Database();
         $Database->SQL()->Replace('Translation', array(
            'Value' => $Code,
            'UserLanguageID' => 1,
            'Application' => $this->_EnabledApplication(),
            'InsertUserID' => $Session->UserID,
            'DateInserted' => Gdn_Format::ToDateTime(),
            'UpdateUserID' => $Session->UserID,
            'DateUpdated' => Gdn_Format::ToDateTime()
            ), array('Value' => $Code));
      }
   }

   private $_EnabledApplication = 'Vanilla';   
   public function Gdn_Dispatcher_AfterEnabledApplication_Handler($Sender) {
      $this->_EnabledApplication = ArrayValue('EnabledApplication', $Sender->EventArguments, 'Vanilla'); // Defaults to "Vanilla"
   }
   private function _EnabledApplication() {
      return $this->_EnabledApplication;
   }
   
   private $_Translations = FALSE;
   private function GetTranslations() {
      if (!is_array($this->_Translations)) {
         $TranslationModel = new Gdn_Model('Translation');
         $Translations = $TranslationModel->GetWhere(array('UserLanguageID' => 1));
         $this->_Translations = array();
         foreach ($Translations as $Translation) {
            $this->_Translations[] = $Translation->Value;
         }
      }
      return $this->_Translations;
   }
   
   public function ProfileController_AfterPreferencesDefined_Handler($Sender) {
      $Sender->Preferences['Notifications']['Popup.DownloadComment'] = T('Notify me when people comment on my downloads.');
      $Sender->Preferences['Notifications']['Email.DownloadComment'] = T('Notify me when people comment on my downloads.');
   }
   
   /**
    * Adds 'Downloads' tab to profiles and adds CSS & JS files to their head.
    * 
    * @since 2.0.0
    * @package Vanilla
    * 
    * @param object $Sender ProfileController.
    */ 
   public function ProfileController_AddProfileTabs_Handler($Sender) {
      if (is_object($Sender->User) && $Sender->User->UserID > 0) {
         $Sender->AddProfileTab(T('Downloads'), 'profile/downloads/'.$Sender->User->UserID.'/'.urlencode($Sender->User->Name));
         // Add the discussion tab's CSS and Javascript
         $Sender->AddCssFile('profile.css', 'downloads');
         $Sender->AddJsFile('downloads.js');
      }
   }
   /**
	 * Creates downloads tab ProfileController.
	 * 
    * @since 2.0.0
    * @package Vanilla
	 *
	 * @param object $Sender ProfileController.
	 */
   public function ProfileController_Downloads_Create($Sender) {
      $UserReference = ArrayValue(0, $Sender->RequestArgs, '');
		$Username = ArrayValue(1, $Sender->RequestArgs, '');
      // $Offset = ArrayValue(2, $Sender->RequestArgs, 0);
      // Tell the ProfileController what tab to load
		$Sender->GetUserInfo($UserReference, $Username);
      $Sender->SetTabView('Downloads', 'Profile', 'Download', 'Downloads');
      
      // Load the data for the requested tab.
      // if (!is_numeric($Offset) || $Offset < 0)
      //   $Offset = 0;
      
      $Offset = 0;
      $Limit = 100;
      $DownloadModel = new DownloadModel();
		$ResultSet = $DownloadModel->GetWhere(array('UserID' => $Sender->User->UserID), 'DateUpdated', 'desc', $Limit, $Offset);
		$Sender->SetData('Downloads', $ResultSet);
		$NumResults = $DownloadModel->GetCount(array('InsertUserID' => $Sender->User->UserID));
      
      // Set the HandlerType back to normal on the profilecontroller so that it fetches it's own views
      $Sender->HandlerType = HANDLER_TYPE_NORMAL;
      
      // Render the ProfileController
      $Sender->Render();
   }
   
   public function Setup() {
      $Database = Gdn::Database();
      $Config = Gdn::Factory(Gdn::AliasConfig);
      $Drop = C('Downloads.Version') === FALSE ? TRUE : FALSE;
      $Explicit = TRUE;
      $Validation = new Gdn_Validation(); // This is going to be needed by structure.php to validate permission names
      include(PATH_APPLICATIONS . DS . 'downloads' . DS . 'settings' . DS . 'structure.php');

      $ApplicationInfo = array();
      include(CombinePaths(array(PATH_APPLICATIONS . DS . 'downloads' . DS . 'settings' . DS . 'about.php')));
      $Version = ArrayValue('Version', ArrayValue('Downloads', $ApplicationInfo, array()), 'Undefined');
      SaveToConfig('Downloads.Version', $Version);
   }

   /**
    * Settings page.
    */
   public function Base_GetAppSettingsMenuItems_Handler($Sender) {
      $Menu = $Sender->EventArguments['SideMenu'];
      $Menu->AddLink('Add-ons', T('Downloads'), 'dashboard/settings/downloads', 'Garden.Settings.Manage');
   }

   //public function SettingsController_BeforeAddDownloadsCategoryForm_Handler($Sender) {
   //   
   //}

   public function SettingsController_Downloads_Create($Sender, $Args = array()) {
      $Sender->Permission('Garden.Plugins.Manage');
      $Sender->AddSideMenu();
      $Sender->Title('Downloads Application Settings');
      $Sender->Form = new Gdn_Form();
      $ConfigurationModule = new ConfigurationModule($Sender);
      $ConfigurationModule->RenderAll = True;

      $DownloadCategoryResult = Gdn::SQL()->Select('DownloadTypeID, Label, Visible')
                                   ->From('DownloadType')
                                   ->Get()
                                   ->Result(DATASET_TYPE_ARRAY);
      $Sender->SetData('DownloadsCategory', $DownloadCategoryResult);

      $Page = GetValue(0, $Args);
      switch(strtolower($Page)) {
         case 'add':
            if ($Sender->Form->AuthenticatedPostBack()) {
                 $SaveResult = $Sender->Form->FormValues();

                 Gdn::SQL()->Insert('DownloadType', array('Visible' => $SaveResult['Visible'], 'Label' => $SaveResult['Label']));
            }

            $Sender->View = realpath(dirname(__FILE__) . '/..') . '/views/settings/addcategory.php';
            break;
         case 'edit':
            if (isset($_GET['id'])) {
               $CategoryID = $_GET['id'];

               $CheckResult = Gdn::SQL()->Select('DownloadTypeID')
                                  ->From('DownloadType')
                                  ->Where('DownloadTypeID', $CategoryID)
                                  ->Get()
                                  ->FirstRow(DATASET_TYPE_ARRAY);

               if ($CheckResult === FALSE) {
                  break;
               }
            }

            if (!isset($_GET['id'])) {
               break;
            }

            if ($Sender->Form->AuthenticatedPostBack()) {
                 $SaveResult = $Sender->Form->FormValues();

                 Gdn::SQL()->Update('DownloadType')
                     ->Set('Visible', $SaveResult['Visible'])
                     ->Set('Label', $SaveResult['Label'])
                     ->Where('DownloadTypeID', $CategoryID)
                     ->Put();
            }

            $Sender->View = realpath(dirname(__FILE__) . '/..') . '/views/settings/addcategory.php';
            break;
         case 'delete':
            if (isset($_GET['id'])) {
               $CategoryID = $_GET['id'];

               $CheckResult = Gdn::SQL()->Select('DownloadTypeID')
                                  ->From('DownloadType')
                                  ->Where('DownloadTypeID', $CategoryID)
                                  ->Get()
                                  ->FirstRow(DATASET_TYPE_ARRAY);

               if ($CheckResult === FALSE) {
                  break;
               }
            }

            if (!isset($_GET['id'])) {
               break;
            }

            Gdn::SQL()->Delete('DownloadType', array('DownloadTypeID' => $CategoryID));

            $Sender->View = realpath(dirname(__FILE__) . '/..') . '/views/settings/deletecategory.php';
            break;
         default:
            $Sender->View = realpath(dirname(__FILE__) . '/..') . '/views/settings/settings.php';
      }

      $Sender->ConfigurationModule = $ConfigurationModule;
      $Sender->Render();
   }
}
