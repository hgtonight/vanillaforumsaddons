<?php if (!defined('APPLICATION')) exit();
/*
Copyright 2008, 2009 Vanilla Forums Inc.
This file is part of Garden.
Garden is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Garden is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Garden.  If not, see <http://www.gnu.org/licenses/>.
Contact Vanilla Forums Inc. at support [at] vanillaforums [dot] com
*/

if (!function_exists('Trace')) {
   function Trace($A, $B) {
   }
}

/**
 * MessagesController handles displaying lists of conversations and conversation messages.
 */
class DownloadController extends DownloadsController {   
   public $Uses = array('Form', 'DownloadModel', 'DownloadCommentModel');
	public $Filter = 'all';
	public $Sort = 'recent';
	public $Version = '0'; // The version of Vanilla to filter to (0 is no filter)
   /**
    * @var Gdn_Form 
    */
   public $Form;
   /**
    * @var DownloadModel
    */
   public $DownloadModel;
   
   public function Initialize() {
      parent::Initialize();
      if ($this->Head) {
         $this->AddJsFile('jquery.js');
         $this->AddJsFile('jquery.livequery.js');
         $this->AddJsFile('jquery.form.js');
         $this->AddJsFile('jquery.popup.js');
         $this->AddJsFile('jquery.gardenhandleajaxform.js');
         $this->AddJsFile('global.js');
      }
      $this->CountCommentsPerPage = 30;
   }

   /**
    * Home Page
    */
   public function Index($ID = '') {
      if ($ID != '') {
         $Download = $this->DownloadModel->GetSlug($ID, TRUE);
         if (!is_array($Download)) {
            throw NotFoundException('Download');
         } else {
            $DownloadID = $Download['DownloadID'];
            $this->SetData($Download);
            
            $Description = GetValue('Description', $Download);
            if ($Description) {
               $this->Head->AddTag('meta', array('name' => 'description', 'content' => Gdn_Format::PlainText($Description, FALSE)));
            }

//            if ($MaxVersion) {
//               $this->SetData('CurrentVersion', GetValue('Version', $MaxVersion));
//            }

            $this->AddCssFile('popup.css');
            $this->AddCssFile('fancyzoom.css');
            $this->AddJsFile('fancyzoom.js');
            $this->AddJsFile('download.js');
            $PictureModel = new Gdn_Model('DownloadPicture');
            $this->PictureData = $PictureModel->GetWhere(array('DownloadID' => $DownloadID));
				$DiscussionModel = new DiscussionModel();
            $this->DiscussionData = $DiscussionModel->Get(0, 50, array('DownloadID' => $DownloadID));
            
            $this->View = 'download';
				$this->Title($this->Data('Name').' '.$this->Data('Version').' by '.$this->Data('InsertName'));

            // Set the canonical url.
            $this->CanonicalUrl(Url('/download/'.DownloadModel::Slug($Download, FALSE), TRUE));
         }
      } else {
			$this->View = 'browse';
			$this->Browse();
			return;
		/*
         $this->ApprovedData = $this->DownloadModel->GetWhere(array('DateReviewed is not null' => ''), 'DateUpdated', 'desc', 5);
         $ApprovedIDs = ConsolidateArrayValuesByKey($this->ApprovedData->ResultArray(), 'DownloadID');
         if (count($ApprovedIDs) > 0)
            $this->DownloadModel->SQL->WhereNotIn('a.DownloadID', $ApprovedIDs);
            
         $this->NewData = $this->DownloadModel->GetWhere(FALSE, 'DateUpdated', 'desc', 5);
		*/
      }
  		$this->AddModule('DownloadHelpModule');
      $this->SetData('_Types', DownloadModel::$Types);
      $this->SetData('_TypesPlural', DownloadModel::$TypesPlural);
      
		$this->Render();
   }

   /**
    * Backup version of add for Vanilla 1 downloads.
    */
   public function Add() {
		$this->Permission('Downloads.Download.Add');
		$this->AddJsFile('/js/library/jquery.autogrow.js');
		$this->AddJsFile('forms.js');
      
      $this->Form->SetModel($this->DownloadModel);
      $DownloadTypeModel = new Gdn_Model('DownloadType');
      $this->TypeData = $DownloadTypeModel->GetWhere(array('Visible' => '1', 'Label <>' => 'Core'));
      
      if ($this->Form->AuthenticatedPostBack()) {
         $Upload = new Gdn_Upload();
         $Upload->AllowFileExtension(NULL);
         $Upload->AllowFileExtension('zip');
         try {
            // Validate the upload
            $TmpFile = $Upload->ValidateUpload('File');
            $Extension = pathinfo($Upload->GetUploadedFileName(), PATHINFO_EXTENSION);
            
            // Generate the target file name
            $TargetFile = $Upload->GenerateTargetName('downloads', $Extension);
            $FileBaseName = pathinfo($TargetFile, PATHINFO_BASENAME);
            
            // Save the uploaded file
            $Upload->SaveAs(
               $TmpFile,
               $TargetFile
            );
            $Path = $Upload->CopyLocal($TargetFile);
         } catch (Exception $ex) {
            $this->Form->AddError($ex->getMessage());
         }
         // If there were no errors, save the download
         if ($this->Form->ErrorCount() == 0) {
            // Save the download
            $DownloadID = $this->Form->Save();

            if ($DownloadID !== FALSE) {
               $Download = $this->DownloadModel->GetID($DownloadID);
               $this->SetData('Download', $Download);
               
               if ($this->DeliveryType == DELIVERY_TYPE_ALL) {
                  // Redirect to the new download.
                  Redirect("download/".DownloadModel::Slug($Download, FALSE));
               }
            }
         } else {
            if (isset($TargetFile) && file_exists($TargetFile))
               unlink($TargetFile);
         }
      }
      $this->Render();      
   }

   public function Check($DownloadID, $SaveVersionID = FALSE) {
      $this->Permission('Downloads.Download.Manage');

      if ($SaveVersionID !== FALSE) {
         // Get the version data.
         $Version = $this->DownloadModel->SQL->GetWhere('DownloadVersion', array('DownloadVersionID' => $SaveVersionID))->FirstRow(DATASET_TYPE_ARRAY);

         $this->DownloadModel->Save($Version);
         $this->Form->SetValidationResults($this->DownloadModel->ValidationResults());
      }

      $Download = $this->DownloadModel->GetID($DownloadID, TRUE);
      $DownloadTypes = Gdn::SQL()->Get('DownloadType')->ResultArray();
      $DownloadTypes = Gdn_DataSet::Index($DownloadTypes, 'DownloadTypeID');

      if (!$Download)
         throw NotFoundException('Download');

      // Get the data for the most recent version of the download.
      $Path = PATH_UPLOADS.'/'.$Download['File'];
      
      $DownloadData = ArrayTranslate((array)$Download, array('DownloadID', 'DownloadKey', 'Name', 'Type', 'Description', 'Requirements', 'Checked'));
      try {
         $FileDownloadData = UpdateModel::AnalyzeDownload($Path);
         if ($FileDownloadData) {
            $DownloadData = array_merge($DownloadData, ArrayTranslate($FileDownloadData, array('DownloadKey' => 'File_DownloadKey', 'Name' => 'File_Name', 'File_Type', 'Description' => 'File_Description', 'Requirements' => 'File_Requirements', 'Checked' => 'File_Checked')));
            $DownloadData['File_Type'] = GetValueR($FileDownloadData['DownloadTypeID'].'.Label', $DownloadTypes, 'Unknown');
         }
      } catch (Exception $Ex) {
         $DownloadData['File_Error'] = $Ex->getMessage();
      }
      $this->SetData('Download', $DownloadData);

      // Go through the versions and make sure we get the versions to check out.
      $Versions = array();
      foreach ($Download['Versions'] as $Version) {
         $Version = $Version;
         $Path = PATH_UPLOADS."/{$Version['File']}";

         try {
            $VersionData = ArrayTranslate((array)$Version, array('DownloadVersionID', 'Version', 'DownloadKey', 'Name', 'MD5', 'FileSize', 'Checked'));
            
            $FileVersionData = UpdateModel::AnalyzeDownload($Path);
            $FileVersionData = ArrayTranslate($FileVersionData, array('Version' => 'File_Version', 'DownloadKey' => 'File_DownloadKey', 'Name' => 'File_Name', 'MD5' => 'File_MD5', 'FileSize' => 'File_FileSize', 'Checked' => 'File_Checked'));
         } catch (Exception $Ex) {
            $FileVersionData = array('File_Error' => $Ex->getMessage());
         }
         $Versions[] = array_merge($VersionData, $FileVersionData);
      }
      $this->SetData('Versions', $Versions);

      $this->AddModule('DownloadHelpModule');
      $this->Render();
   }

   public function DeleteVersion($VersionID) {
      $this->Permission('Downloads.Download.Manage');
      $Version = $this->DownloadModel->GetVersion($VersionID);
      $this->Data = $Version;

      if ($this->Form->AuthenticatedPostBack() && $this->Form->GetFormValue('Yes')) {
         $this->DownloadModel->DeleteVersion($VersionID);

         // Update the current version of the download.
         $DownloadID = GetValue('DownloadID', $Version);
         $this->DownloadModel->UpdateCurrentVersion($DownloadID);
         $this->RedirectUrl = Url('/download/check/'.$DownloadID);
      }
      $this->Render();
   }

   public function Edit($DownloadID = '') {
		$this->Permission('Downloads.Download.Add');

		$this->AddJsFile('/js/library/jquery.autogrow.js');
		$this->AddJsFile('forms.js');

		$Session = Gdn::Session();
      $Download = $this->DownloadModel->GetID($DownloadID);
      if (!$Download)
         throw NotFoundException('Download');

      if ($Download['InsertUserID'] != $Session->UserID)
         $this->Permission('Downloads.Download.Manage');

      $this->Form->SetModel($this->DownloadModel);
      $this->Form->AddHidden('DownloadID', $DownloadID);
      $DownloadTypeModel = new Gdn_Model('DownloadType');
      $this->TypeData = $DownloadTypeModel->GetWhere(array('Visible' => '1'));

      if ($this->Form->AuthenticatedPostBack() === FALSE) {
         $this->Form->SetData($Download);
      } else {
         if ($this->Form->Save() !== FALSE) {
            $Download = $this->DownloadModel->GetID($DownloadID);
            $this->StatusMessage = T("Your changes have been saved successfully.");
            $this->RedirectUrl = Url('/download/'.DownloadModel::Slug($Download));
         }
      }

      $this->Render();
   }
   
   public function EditV1($DownloadID = '') {
		// $this->Permission('Downloads.Download.Manage');
		
		$this->AddJsFile('/js/library/jquery.autogrow.js');
		$this->AddJsFile('forms.js');
      
		$Session = Gdn::Session();
      $Download = $this->DownloadModel->GetID($DownloadID);
      if (!$Download)
         throw NotFoundException('Download');
         
      if ($Download['InsertUserID'] != $Session->UserID)
         $this->Permission('Garden.Settings.Manage');
         
      $this->Form->SetModel($this->DownloadModel);
      $this->Form->AddHidden('DownloadID', $DownloadID);
      $DownloadTypeModel = new Gdn_Model('DownloadType');
      $this->TypeData = $DownloadTypeModel->GetWhere(array('Visible' => '1'));
      
      if ($this->Form->AuthenticatedPostBack() === FALSE) {
         $this->Form->SetData($Download);
      } else {
         if ($this->Form->Save(TRUE) !== FALSE) {
            $Download = $this->DownloadModel->GetID($DownloadID);
            $this->StatusMessage = T("Your changes have been saved successfully.");
            $this->RedirectUrl = Url('/download/'.DownloadModel::Slug($Download));
         }
      }
      
      $this->Render();
   }

   public function NewVersion($DownloadID = '') {
      $this->_NewVersion($DownloadID);
   }
   
   protected function _NewVersion($DownloadID = '', $V1 = FALSE) {
		$Session = Gdn::Session();
      $Download = $this->DownloadModel->GetID($DownloadID);
      if (!$Download)
         Redirect('dashboard/home/filenotfound');
         
      if ($Download['InsertUserID'] != $Session->UserID)
         $this->Permission('Downloads.Download.Manage');

      $this->AddModule('DownloadHelpModule');
      
      $this->Form->SetModel($this->DownloadModel);
      $this->Form->AddHidden('DownloadID', $DownloadID);
      
      if ($this->Form->IsPostBack()) {
         $Upload = new Gdn_Upload();
         $Upload->AllowFileExtension(NULL);
         $Upload->AllowFileExtension('zip');
         try {
            // Validate the upload
            $TmpFile = $Upload->ValidateUpload('File');
            $Extension = pathinfo($Upload->GetUploadedFileName(), PATHINFO_EXTENSION);
            
            // Generate the target name
            $TargetFile = $Upload->GenerateTargetName('downloads', $Extension);
            $FileBaseName = pathinfo($TargetFile, PATHINFO_BASENAME);
            $TargetPath = PATH_UPLOADS.'/'.$TargetFile;
            
            if (!file_exists(dirname($TargetPath))) {
               mkdir(dirname($TargetPath), 0777, TRUE);
            }
            
            // Save the file to a temporary location for parsing...
            if (!move_uploaded_file($TmpFile, $TargetPath)) {
               throw new Exception("We couldn't save the file you uploaded. Please try again later.", 400);
            }
            
            $AnalyzedDownload = UpdateModel::AnalyzeDownload($TargetPath, TRUE);
//            decho($AnalyzedDownload);
//            
//            decho();
//            die();
            
            // Set the filename for the CDN...
            $Upload->EventArguments['OriginalFilename'] = DownloadModel::Slug($AnalyzedDownload, TRUE).'.zip';
            
            // Save the uploaded file
            $Parsed = $Upload->SaveAs(
               $TargetPath,
               $TargetFile
            );
            $AnalyzedDownload['DownloadID'] = $DownloadID;
            $AnalyzedDownload['File'] = $Parsed['SaveName'];
            unset($AnalyzedDownload['Path']);
//            $AnalyzedDownload['Description2'] = $this->Form->GetFormValue('Description2');
            Trace($AnalyzedDownload, 'Analyzed Download');
            
            
            $this->Form->FormValues($AnalyzedDownload);
//            $this->Form->SetFormValue('Path', $TargetPath);
//				$this->Form->SetFormValue('TestedWith', 'Blank');
         } catch (Exception $ex) {
            $this->Form->AddError($ex);
            
            // Delete the erroneous file.
            try {
               $Upload->Delete($AnalyzedDownload['File']);
            } catch(Exception $Ex2) {
            }
         }
         
         if (isset($TargetPath) && file_exists($TargetPath)) {
            unlink($TargetPath);
         }
         
         // If there were no errors, save the downloadversion
         if ($this->Form->ErrorCount() == 0) {
            $NewVersionID = $this->Form->Save($V1);
            if ($NewVersionID) {
               $this->SetData('Download', $AnalyzedDownload);
               $this->SetData('Url', Url('/download/'.DownloadModel::Slug($Download, TRUE), TRUE));
               $this->RedirectUrl = $this->Data('Url');
               if ($this->DeliveryType() == DELIVERY_TYPE_ALL)
                  $this->RedirectUrl = $this->Data('Url');
            } else {
               if (file_exists($Path))
                  unlink($Path);
            }
         }
      }
      $this->Render();      
   }

   public function NewVersionV1($DownloadID = '') {
      $this->_NewVersion($DownloadID, TRUE);
   }

   public function NotFound() {
      $this->Render();
   }
   
   public function Approve($DownloadID = '') {
      $this->Permission('Downloads.Download.Manage');
      $Session = Gdn::Session();
      $Download = $this->Download = $this->DownloadModel->GetID($DownloadID);
      $VersionModel = new Gdn_Model('DownloadVersion');
      if (!$Download['DateReviewed']) {
         $VersionModel->Save(array('DownloadVersionID' => $Download['DownloadVersionID'], 'DateReviewed' => Gdn_Format::ToDateTime()));
      } else {
         $VersionModel->Update(array('DateReviewed' => null), array('DownloadVersionID' => $Download['DownloadVersionID']));
      }
      
      Redirect('/download/'.DownloadModel::Slug($Download));
  }
  
   protected static function NotFoundString($Code, $Item) {
      return sprintf(T('%1$s "%2$s" not found.'), T($Code), $Item);
   }
  
   public function ChangeOwner($DownloadID) {
      $this->Permission('Garden.Settings.Manage');
      $Download = $this->DownloadModel->GetSlug($DownloadID);
      
      if (!$Download)
         throw NotFoundException('Download');
         
      if ($this->Form->IsPostBack()) {
         $this->Form->ValidateRule('User', 'ValidateRequired');
         
         if ($this->Form->ErrorCount() == 0) {
            $NewUser = $this->Form->GetFormValue('User');
            if (is_numeric($NewUser))
               $User = Gdn::UserModel()->GetID($NewUser, DATASET_TYPE_ARRAY);
            else
               $User = Gdn::UserModel()->GetByUsername($NewUser);
         
            if (!$User)
               $this->Form->AddError('@'.self::NotFoundString('User', $NewUser));
         }
         
         if ($this->Form->ErrorCount() == 0) {
            $this->DownloadModel->SetField($Download['DownloadID'], 'InsertUserID', GetValue('UserID', $User));
         }
      } else {
         $this->Form->AddError('You must POST to this page.');
      }
      
      $this->Render();
   }
  
   public function Delete($DownloadID = '') {
      $this->Permission('Downloads.Download.Manage');
      $Session = Gdn::Session();
      if (!$Session->IsValid())
         $this->Form->AddError('You must be authenticated in order to use this form.');

      $Download = $this->DownloadModel->GetID($DownloadID);
      if (!$Download)
         Redirect('dashboard/home/filenotfound');

      if ($Session->UserID != $Download['InsertUserID'])
			$this->Permission('Downloads.Download.Manage');

      $Session = Gdn::Session();
      if (is_numeric($DownloadID)) 
         $this->DownloadModel->Delete($DownloadID);

      if ($this->_DeliveryType === DELIVERY_TYPE_ALL)
         Redirect(GetIncomingValue('Target', Gdn_Url::WebRoot()));

      $this->View = 'index';
      $this->Render();
   }

   /**
    * Add a comment to an download
    */
   public function AddComment($DownloadID = '') {
      $Render = TRUE;
      $this->Form->SetModel($this->DownloadCommentModel);
      $DownloadID = $this->Form->GetFormValue('DownloadID', $DownloadID);

      if (is_numeric($DownloadID) && $DownloadID > 0)
         $this->Form->AddHidden('DownloadID', $DownloadID);
      
      if ($this->Form->AuthenticatedPostBack()) {
         $NewCommentID = $this->Form->Save();
         // Comment not saving for some reason - no errors reported
         if ($NewCommentID > 0) {
            // Update the Comment count
            $this->DownloadModel->SetProperty($DownloadID, 'CountComments', $this->DownloadCommentModel->GetCount(array('DownloadID' => $DownloadID)));
            if ($this->DeliveryType() == DELIVERY_TYPE_ALL)
               Redirect('download/'.$DownloadID.'/#Comment_'.$NewCommentID);
               
            $this->SetJson('CommentID', $NewCommentID);
            // If this was not a full-page delivery type, return the partial response
            // Load all new messages that the user hasn't seen yet (including theirs)
            $LastCommentID = $this->Form->GetFormValue('LastCommentID');
            if (!is_numeric($LastCommentID))
               $LastCommentID = $NewCommentID - 1;
            
            $Session = Gdn::Session();
            $this->Download = $this->DownloadModel->GetID($DownloadID);   
            $this->CommentData = $this->DownloadCommentModel->GetNew($DownloadID, $LastCommentID);
            $this->View = 'comments';
         } else {
            // Handle ajax based errors...
            if ($this->DeliveryType() != DELIVERY_TYPE_ALL) {
               $this->StatusMessage = $this->Form->Errors();
            } else {
               $Render = FALSE;
               $this->Index($DownloadID);
            }
         }
      }

      if ($Render)
         $this->Render();      
   }
   
   public function DeleteComment($CommentID = '') {
      $this->Permission('Downloads.Comments.Manage');
      $Session = Gdn::Session();
      if (is_numeric($CommentID))
         $this->DownloadCommentModel->Delete($CommentID);

      if ($this->_DeliveryType === DELIVERY_TYPE_ALL) {
         Redirect(Url(GetIncomingValue('Return', ''), TRUE));
      }
         
      $this->View = 'notfound';
      $this->Render();
   }
   
	public function Browse($FilterToType = '', $Sort = '', $Page = '') {
      $Checked = GetIncomingValue('checked', FALSE);

		// Implement user prefs
		$Session = Gdn::Session();
		if ($Session->IsValid()) {
			if ($FilterToType != '') {
				$Session->SetPreference('Downloads.FilterType', $FilterToType);
			}
			if ($Sort != '')
				$Session->SetPreference('Downloads.Sort', $Sort);
         if ($Checked !== FALSE)
            $Session->SetPreference('Downloads.FilterChecked', $Checked);
			
			$FilterToType = $Session->GetPreference('Downloads.FilterType', 'all');
			$Sort = $Session->GetPreference('Downloads.Sort', 'recent');
         $Checked = $Session->GetPreference('Downloads.FilterChecked');
		}
		
		if (!array_key_exists($FilterToType, DownloadModel::$TypesPlural))
			$FilterToType = 'all';
		
		if ($Sort != 'popular')
			$Sort = 'recent';
			
		$this->Sort = $Sort;

      $this->FilterChecked = $Checked;

		$this->AddJsFile('/js/library/jquery.gardenmorepager.js');
		$this->AddJsFile('browse.js');

      list($Offset, $Limit) = OffsetLimit($Page, Gdn::Config('Garden.Search.PerPage', 20));
		
      $this->Filter = $FilterToType;
      
      if ($this->Filter == 'themes')
         $Title = 'Browse Themes';
      elseif ($this->Filter == 'plugins')
         $Title = 'Browse Plugins';
      elseif ($this->Filter == 'applications')
         $Title = 'Browse Applications';
      else
         $Title = 'Browse Downloads';
      $this->SetData('Title', $Title);
      
		$Search = GetIncomingValue('Keywords', '');
		$this->_BuildBrowseWheres($Search);
				
		$SortField = $Sort == 'recent' ? 'DateUpdated' : 'CountDownloads';
		$ResultSet = $this->DownloadModel->GetWhere(FALSE, $SortField, 'desc', $Limit, $Offset);
		$this->SetData('Downloads', $ResultSet);
		$this->_BuildBrowseWheres($Search);
		$NumResults = $this->DownloadModel->GetCount(FALSE);
      $this->SetData('TotalDownloads', $NumResults);
		
		// Build a pager
		$PagerFactory = new Gdn_PagerFactory();
		$Pager = $PagerFactory->GetPager('Pager', $this);
		$Pager->MoreCode = '›';
		$Pager->LessCode = '‹';
		$Pager->ClientID = 'Pager';
		$Pager->Configure(
			$Offset,
			$Limit,
			$NumResults,
			'download/browse/'.$FilterToType.'/'.$Sort.'/%1$s/?Form/Keywords='.urlencode($Search)
		);
		$this->SetData('_Pager', $Pager);
      
      if ($this->_DeliveryType != DELIVERY_TYPE_ALL)
         $this->SetJson('MoreRow', $Pager->ToString('more'));
      
		$this->AddModule('DownloadHelpModule');
		
		$this->Render();
	}
	
	private function _BuildBrowseWheres($Search = '') {
      if ($Search != '') {
         $this->DownloadModel
            ->SQL
            ->BeginWhereGroup()
            ->Like('a.Name', $Search)
            ->OrLike('a.Description', $Search)
            ->EndWhereGroup();
		}

      $Ch = array('unchecked' => 0, 'checked' => 1);
      if (isset($Ch[$this->FilterChecked])) {
         $this->DownloadModel->SQL->Where('a.Checked', $Ch[$this->FilterChecked]);
      }

      if ($Types = $this->Request->Get('Types')) {
         $Types = explode(',', $Types);
         foreach ($Types as $Index => $Type) {
            if (isset(DownloadModel::$Types[trim($Type)]))
               $Types[$Index] = DownloadModel::$Types[trim($Type)];
            else
               unset($Types[$Index]);
         }
         $this->DownloadModel->SQL->WhereIn('a.DownloadTypeID', $Types);
      }

      $DownloadTypeID = GetValue($this->Filter, DownloadModel::$TypesPlural);
      if ($DownloadTypeID)
			$this->DownloadModel
				->SQL
				->Where('a.DownloadTypeID', $DownloadTypeID);
	}
   
   public function AddPicture($DownloadID = '') {
      $Session = Gdn::Session();
      if (!$Session->IsValid())
         $this->Form->AddError('You must be authenticated in order to use this form.');

      $Download = $this->DownloadModel->GetID($DownloadID);
      if (!$Download)
         throw NotFoundException('Download');

      if ($Session->UserID != $Download['InsertUserID'])
			$this->Permission('Downloads.Download.Manage');
      
      $this->AddModule('DownloadHelpModule', 'Panel');
      $DownloadPictureModel = new Gdn_Model('DownloadPicture');
      $this->Form->SetModel($DownloadPictureModel);
      $this->Form->AddHidden('DownloadID', $DownloadID);
      if ($this->Form->AuthenticatedPostBack() === TRUE) {
         $UploadImage = new Gdn_UploadImage();
         try {
            // Validate the upload
            $TmpImage = $UploadImage->ValidateUpload('Picture');
            
            // Generate the target image name
            $TargetImage = $UploadImage->GenerateTargetName(PATH_UPLOADS, '');
            $ImageBaseName = 'downloads/screens/'.pathinfo($TargetImage, PATHINFO_BASENAME);
            
            // Save the uploaded image in large size
            $ImgParsed = $UploadImage->SaveImageAs(
               $TmpImage,
               ChangeBaseName($ImageBaseName, 'ao%s'),
               700,
               1000
            );

            // Save the uploaded image in thumbnail size
            $ThumbSize = 150;
            $ThumbParsed = $UploadImage->SaveImageAs(
               $TmpImage,
               ChangeBasename($ImageBaseName, 'at%s'),
               $ThumbSize,
               $ThumbSize
            );
            $ImageBaseName = sprintf($ImgParsed['SaveFormat'], $ImageBaseName);
         } catch (Exception $ex) {
            $this->Form->AddError($ex->getMessage());
         }
         // If there were no errors, insert the picture
         if ($this->Form->ErrorCount() == 0) {
            $DownloadPictureModel = new Gdn_Model('DownloadPicture');
            $DownloadPictureID = $DownloadPictureModel->Insert(array('DownloadID' => $DownloadID, 'File' => $ImageBaseName));
         }

         // If there were no problems, redirect back to the download
         if ($this->Form->ErrorCount() == 0)
            $this->RedirectUrl = Url('/download/'.DownloadModel::Slug($Download));
      }
      $this->Render();
   }
   
   public function DeletePicture($DownloadPictureID = '') {
      $this->Permission('Downloads.Download.Manage');

      if ($this->Form->AuthenticatedPostBack() && $this->Form->GetFormValue('Yes')) {
         $DownloadPictureModel = new Gdn_Model('DownloadPicture');
         $Picture = $DownloadPictureModel->GetWhere(array('DownloadPictureID' => $DownloadPictureID))->FirstRow();
         $Upload = new Gdn_Upload();
         
         if ($Picture) {
            $Upload->Delete(ChangeBasename($Picture->File, 'ao%s'));
            $Upload->Delete(ChangeBasename($Picture->File, 'at%s'));
            $DownloadPictureModel->Delete(array('DownloadPictureID' => $DownloadPictureID));
         }
         $this->RedirectUrl = Url('/download/'.$Picture->DownloadID);
      }
      $this->Render('deleteversion');
   }

   public function GetList($IDs) {
      $IDs = explode(',', $IDs);
      array_map('trim', $IDs);

      $Downloads = $this->DownloadModel->GetIDs($IDs);
      $this->SetData('Downloads', $Downloads);

      $this->Render('browse');
   }
   
   public function Icon($DownloadID = '') {
      $Session = Gdn::Session();
      if (!$Session->IsValid())
         $this->Form->AddError('You must be authenticated in order to use this form.');

      $Download = $this->DownloadModel->GetID($DownloadID);
      if (!$Download)
         throw NotFoundException('Download');

      if ($Session->UserID != $Download['InsertUserID'])
			$this->Permission('Downloads.Download.Manage');

      $this->AddModule('DownloadHelpModule', 'Panel');
      $this->Form->SetModel($this->DownloadModel);
      $this->Form->AddHidden('DownloadID', $DownloadID);
      if ($this->Form->AuthenticatedPostBack() === TRUE) {
         $UploadImage = new Gdn_UploadImage();
         try {
            // Validate the upload
            $TmpImage = $UploadImage->ValidateUpload('Icon');
            
            // Generate the target image name
            $TargetImage = $UploadImage->GenerateTargetName('downloads/icons', '');
            $ImageBaseName = pathinfo($TargetImage, PATHINFO_BASENAME);
            
            // Save the uploaded icon
            $Parsed = $UploadImage->SaveImageAs(
               $TmpImage,
               $TargetImage,
               128,
               128,
               FALSE, FALSE
            );
            $TargetImage = $Parsed['SaveName'];
         } catch (Exception $ex) {
            $this->Form->AddError($ex);
         }
         // If there were no errors, remove the old picture and insert the picture
         if ($this->Form->ErrorCount() == 0) {
//            $Download = $this->DownloadModel->GetID($DownloadID);
            if ($Download['Icon']) {
               $UploadImage->Delete($Download['Icon']);
            }
               
            $this->DownloadModel->Save(array('DownloadID' => $DownloadID, 'Icon' => $TargetImage));
         }

         // If there were no problems, redirect back to the download
         if ($this->Form->ErrorCount() == 0)
            $this->RedirectUrl = Url('/download/'.DownloadModel::Slug($Download));
      }
      $this->Render();
   }
}