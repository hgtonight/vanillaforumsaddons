<?php if (!defined('APPLICATION')) exit(); ?>

<div class="Box">
	<h4>Download Navigation</h4>
	<ul>
	<?php
		$Session = Gdn::Session();
		echo '<li>'.Anchor('Quick-Start Guide', '/page/DownloadQuickStart').'</li>';
		if ($Session->IsValid()) {
			echo '<li>'.Anchor('Upload a New Download', '/download/add').'</li>';
		} else {
			echo '<li>'.Anchor('Sign In', Gdn::Authenticator()->SignInUrl('/downloads'), SignInPopup() ? 'SignInPopup' : '').'</li>';
		}
	?>
	</ul>
</div>

<div class="Box What">
	<h4>What is this stuff?</h4>
	<p>Downloads are created by our community and by people like you!</p>
</div>
	
<div class="Box Approved">
	<h4>Are the downloads approved?</h4>
	<p>We review downloads to make sure they are safe and don't cause any problems. A download is considered to be approved once our review process is complete.</p>
</div>
