<?php if (!defined('APPLICATION')) exit();

function WriteDownload($Download, $Alt) {
   $Url = '/download/'.DownloadModel::Slug($Download, FALSE);
	?>
	<li class="Item DownloadRow<?php echo $Alt; ?>">
		<div class="ItemContent">
			<?php
			echo '<div>', Anchor($Download->Name, $Url, 'Title'), '</div>';
			
			if ($Download->Icon != '') {
				echo '<div><a class="Icon" href="'.Url($Url).'"><div class="IconWrap"><img src="'.Gdn_Upload::Url($Download->Icon).'" /></div></a></div>';
         }
	
			echo '<div>', Anchor(SliceString(Gdn_Format::Text($Download->Description), 300), $Url), '</div>';
			?>
			<div class="Meta">
				<span class="<?php echo $Download->Vanilla2 == '1' ? 'Vanilla2' : 'Vanilla1'; ?>"><?php
					echo $Download->Vanilla2 == '1' ? 'Vanilla 2' : 'Vanilla 1'; ?></span>
				<?php
            if (Gdn::Session()->CheckPermission('Downloads.Download.Manage')) {
               if ($Download->Checked) {
                  echo '<span class="Approved">Checked</span>';
               } elseif ($Download->Vanilla2) {
                  echo Anchor('<span class="Closed">Check</span>', Url('/download/check/'.$Download->DownloadID));
               }
            }
				if ($Download->DateReviewed != '')
					echo '<span class="Approved">Approved</span>';
				?>
				<span class="Type">
					Type
					<span><?php echo $Download->Type; ?></span>
				</span>
            <span class="Version">
               Version
               <span><?php echo $Download->Version; ?></span>
            </span>
				<span class="Author">
					Author
					<span><?php echo $Download->InsertName; ?></span>
				</span>
				<span class="Downloads">
					Downloads
					<span><?php echo number_format($Download->CountDownloads); ?></span>
				</span>
				<span class="Updated">
					Updated
					<span><?php echo Gdn_Format::Date($Download->DateUpdated); ?></span>
				</span>
			</div>
		</div>
	</li>
<?php
}

function fGetDownloadCategoriesList() {
    $DownloadCategoryResult = Gdn::SQL()->Select('DownloadTypeID, Label, Visible')
                                  ->From('DownloadType')
                                  ->Get()
                                  ->Result(DATASET_TYPE_ARRAY);
                                  
    return $DownloadCategoryResult;
}
                                   