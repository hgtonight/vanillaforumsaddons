<?php if (!defined('APPLICATION')) exit();
$Session = Gdn::Session();
include($this->FetchViewLocation('helper_functions'));

if ($this->DeliveryType() == DELIVERY_TYPE_ALL) {
   echo $this->FetchView('head');
?>
   <h1 class="H"><?php echo $this->Data('Title'); ?></h1>
   <div class="DataList">
   <ul class="Downloads">
      <?php
      if ($this->Data('Downloads')->NumRows() == 0)
         echo '<li class="Empty">There were no downloads matching your search criteria.</li>';
}            
$Alt = '';
foreach ($this->Data('Downloads')->Result() as $Download) {
   $Alt = $Alt == ' Alt' ? '' : ' Alt';
   WriteDownload($Download, $Alt);
}
if ($this->DeliveryType() == DELIVERY_TYPE_ALL && $this->Data('_Pager')) {
?>
   </ul>
   </div>
   <?php
   echo $this->Data('_Pager')->ToString('more');
} else {
?></ul><?php
}