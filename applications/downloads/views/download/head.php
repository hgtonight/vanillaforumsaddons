<?php if (!defined('APPLICATION')) exit();
$Session = Gdn::Session();
if (!property_exists($this, 'HideSearch')) {
?>
<div class="SearchForm">
	<?php
	$Url = '/download/browse/'.$this->Filter.'/';
	$Query = GetIncomingValue('Keywords', '');
	echo $this->Form->Open(array('action' => Url($Url.$this->Sort.'/')));
	echo $this->Form->Errors();
	echo $this->Form->TextBox('Keywords', array('value' => $Query));
	echo $this->Form->Button('Browse Downloads');
   
   $Query = urlencode($Query);
	if ($Query != '')
		$Query = '?Keywords='.$Query;
	?>
</div>
<div class="Box FilterMenu">
	<div class="DownloadOptions">
		<strong>&#9658;</strong>
		<?php
		$Suffix = $this->Sort.'/'.$Query;
		echo Anchor('Show All Downloads', 'download/browse/all/'.$Suffix, 'ShowAll' . ($this->Filter == 'all' ? ' Active' : ''));
		?>
		or filter to
		<?php
        foreach (fGetDownloadCategoriesList() as $Category) {
            $strCategoryNameLowercase = strtolower($Category['Label']);
            
            echo Anchor($Category['Label'], 'download/browse/' . $strCategoryNameLowercase . '/' . $Suffix, $this->Filter == $strCategoryNameLowercase ? 'Active' : 'MItem');
        }
		?>
	</div>
	<div class="DownloadOptions OrderOptions">
		<strong>&#9658;</strong> Order by
		<?php
		$Suffix = $Query;
		echo Anchor('Recent', $Url.'recent/'.$Suffix, $this->Sort == 'recent' ? 'Active' : 'MItem');
		echo Anchor('Popular', $Url.'popular/'.$Suffix, $this->Sort == 'popular' ? 'Active' : 'MItem');
		?>
	</div>
	<?php
	echo $this->Form->Close();
	?>
</div>
<?php
}