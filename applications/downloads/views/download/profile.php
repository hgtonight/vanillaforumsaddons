<?php if (!defined('APPLICATION')) exit();
// Create some variables so that they aren't defined in every loop.

include($this->FetchViewLocation('helper_functions', 'download', 'downloads'));
?>
<ul class="DataList Downloads">
<?php
if ($this->Data('Downloads')->NumRows() == 0) {
   echo '<li class="Empty">This user has not contributed any downloads.</li>';
}

$Alt = '';
foreach ($this->Data('Downloads')->Result() as $Download) {
   $Alt = $Alt == ' Alt' ? '' : ' Alt';
   WriteDownload($Download, $Alt);
}
?></ul>
