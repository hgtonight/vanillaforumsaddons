<?php if (!defined('APPLICATION')) exit(); ?>
<h1><?php
if ($this->Download['File'] == '') {
	echo 'The requested download could not be found';
} else {
	echo 'Downloading: ' . htmlspecialchars($this->Download['Name']) . ' version ' . $this->Download['Version'];
?></h1>
<div class="Box DownloadInfo">
	<strong>Your download should begin shortly</strong>
	<p>If your download does not begin right away, <a href="<?php echo Gdn_Upload::Url($this->Download['File']); ?>">click here to download now</a>.</p>
	
	<strong>Need help installing this download?</strong>
	<p>There should be a readme file in the download with more specific instructions on how to install it. If you are still having problems, <a href="http://vanillaforums.org/discussions">ask for help on the community forums</a>.</p>

	<strong>Note</strong>
	<p>Vanilla Forums Inc cannot be held liable for issues that arise from the download or use of these downloads.</p>
	
	<strong>Now what?</strong>
	<p>Head on back to the <a href="<?php echo Url('/download/'.$this->Download['DownloadID']); ?>"><?php echo $this->Download['Name']; ?> page</a>, search for <a href="http://vanillaforums.org/downloads">more add-ons</a>, or you can <a href="http://vanillaforums.org/docs">learn how to make your own</a>.</p>
</div>
<?php
}