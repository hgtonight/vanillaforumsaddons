<?php if (!defined('APPLICATION')) exit(); ?>
<h1><?php echo $this->Data('Title'); ?></h1>
<div class="Info"><?php echo T('You can add download categories using this tool.'); ?></div>
<?php
echo '<noscript><div class="Errors"><ul><li>', T('This page requires Javascript.'), '</li></ul></div></noscript>';

echo <<<EOT
<script type="text/javascript">
// This file contains javascript that is specific to the dashboard/routes controller.
jQuery(document).ready(function($) {

   // Pop add/edit route clicks and reload the page contents when finished.
   $('a.Add, a.Edit').popup({
      onUnload: function(settings) {
         $('#Content').load(window.location.href, {'DeliveryType': 'VIEW'});
      }
   });

   // Confirm deletes before performing them
   $('a.Delete').popup({
      confirm: true,
      followConfirm: false,
      afterConfirm: function(json, sender) {
         $(sender).parents('tr').remove();
      }
   });

});
</script>
EOT;

echo $this->Form->Open();
?>
<div class="Info">
   <?php
   echo Anchor(T('Add Downloads Category'), '/dashboard/settings/downloads/add', array('class' => 'SmallButton Add'));
   ?>
</div>
<?php

echo '<div id="CategoryTable">';
include dirname(__FILE__).'/categorytable.php';
echo '</div id="CategoryTable">';

echo $this->Form->Close();