<?php if (!defined('APPLICATION')) exit(); ?>
<h1><?php echo $this->Data('Title'); ?></h1>
<?php
echo $this->Form->Open();
echo $this->Form->Errors();

if (isset($_GET['id'])) {
   $CategoryID = $_GET['id'];

   $CheckResult = Gdn::SQL()->Select('Label, Visible')
                      ->From('DownloadType')
                      ->Where('DownloadTypeID', $CategoryID)
                      ->Get()
                      ->FirstRow(DATASET_TYPE_ARRAY);
}
?>
<ul>
   <?php $this->FireEvent("BeforeAddDownloadsCategoryForm"); ?>
   <li>
      <?php
         echo $this->Form->Label('Visible', 'CategoryVisible');
         echo '<select id="Form_Visible" name="Visible"><option value="0">No</option><option value="1"';
         if (($CheckResult !== FALSE && $CheckResult['Visible'] == '1') || !isset($_GET['id'])) { echo ' selected="1"'; }
         echo '>Yes</option></select>'
      ?>
   </li>
   <li>
      <?php
         echo $this->Form->Label('Category Label', 'CategoryLabel');
         echo '<input type="text" id="Form_Label" name="Label" value="';
         if ($CheckResult !== FALSE) { echo $CheckResult['Label']; }
         echo '" class="InputBox">';
      ?>
   </li>
   <?php $this->FireEvent("AfterAddDownloadsCategoryForm"); ?>
</ul>
<?php echo $this->Form->Close('Save'); ?>