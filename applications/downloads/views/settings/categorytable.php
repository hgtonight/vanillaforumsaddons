<?php if (!defined('APPLICATION')) exit();
PagerModule::Write(array('Sender' => $this, 'Limit' => 20, 'CurrentRecords' => count($this->Data('DownloadsCategory'))));
?>
<table id="Log" class="AltColumns">
   <thead>
      <tr>
         <th><?php echo T('Category ID', 'ID'); ?></th>
         <th><?php echo T('Category Label', 'Downloads Category Label'); ?></th>
         <th><?php echo T('Category Visible', 'Visible'); ?></th>
         <th><?php echo T('Options'); ?></th>
      </tr>
   </thead>
   <tbody>
      <?php
      foreach ($this->Data('DownloadsCategory') as $Row):
      ?>
      <tr id="<?php echo "DownloadsCategoryID_{$Row['DownloadTypeID']}"; ?>">
         <td><?php echo htmlspecialchars($Row['DownloadTypeID']); ?></td>
         <td><?php echo T($Row['Label']); ?></td>
         <td>
            <?php
            if ($Row['Visible'] == 1) {
               echo T('Yes');
            } else {
               echo T('No');
            }
            ?>
         </td>
         <td>
            <?php
            echo Anchor(T('Edit'), '/dashboard/settings/downloads/edit?id='.$Row['DownloadTypeID'], array('class' => 'SmallButton Edit'));
            echo ' ';
            echo Anchor(T('Delete'), '/dashboard/settings/downloads/delete?id='.$Row['DownloadTypeID'], array('class' => 'SmallButton Delete'));
            ?>
         </td>
      </tr>
      <?php
      endforeach;
      ?>
   </tbody>
</table>
<?php
PagerModule::Write();