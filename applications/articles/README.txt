**** WARNING ****
DO NOT USE ON PRODUCTION WEBSITE. DO NOT USE FOR DEVELOPMENT UNTIL ALPHA, BETA, OR SUCH RELEASE IS OUT.

DEVELOPMENT ENVIRONMENT:
Vanilla: 2.2.3.3 (master-branch; development build)
PHP 5.4.17

Author: Livid Tech (Shadowdare)
Author Website: http://lividtech.com/

** BRIEF SUMMARY **
Eliminate the need for specific uses of WordPress integrations with a Vanilla forum! No longer the need for separate user databases, themes, and hacky embeds! This will be an application under Garden to provide more than just a blog: an application built towards websites with professional featured content (articles), including workflows and authoring features, as well as taxonomies.

** USEFUL STUFF **
- http://vanillawiki.homebrewforums.net/index.php/Main_Page
- http://www.vanilla-wiki.info/
- http://lincolnwebs.com/vanilla2doc/namespaces.html

** ABSOLUTELY IMPORTANT FEATURES **
- Permissions for authoring.
- Permissions for workflow.
- Setting Array Stuff and Configuration Variables
- If conditionals when needed - to be done during code freeze.
- Official submission to addons page.
- Finish documentation and check styling of code and readmes during code freeze.
- . . .

** PLANNED FEATURES **
- Workflow: Draft -> Pending Review -> Published
- Save as draft button.
- Auto save draft every few seconds without refresh.
- Categories (create and select for articles)
- Tags (for SEO and header meta tags only)
- WYSIWYG (CKEditor?) editor and BodyBox usage through Garden for default editor.
----- Check for HTML filtering.
- Upload images to be inserted into article body. Needs to work with WYSIWYG editor.
- Upload a "photo" thumbnail for the article. Look at code from categories. Have the thumbnail scaled to specific width and height, and cropped to fit aspect ratio instead of stretching.
- Be able to change the (1) author, (2) date, (3) category, (4) everything else of an article.
- SEO header meta tags such as: Facebook OG stuff, Twitter Cards, etc.
- . . . MORE TO BE ADDED TO THIS LIST.
- "allow direct reply comments and have a simplified editor. It would even make sense to allow comments without an account (pending moderation and/or captcha)."
- RSS feeds
- Social media buttons: Facebook / Twitter / Reddit.
- Way to embed youtube videos with just link like you can in comments (that works with WYSIWYG).
- Threaded comments SQL column: take similar depth names from categories.
- "Someone has mentioned you in article." Look at code from conversations app. Also, notification from threaded replies to parent or child users in thread.
- Cache checks.
- If no summary and no body, then limit body (and HTML except images?) to character limit (config setting?) in /controllers/articlescontroller.php



