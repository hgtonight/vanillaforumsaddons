<?php if (!defined('APPLICATION')) exit();
/**
 * Skeleton Controller for new applications.
 * 
 * Repace 'Skeleton' with your app's short name wherever you see it.
 *
 * @package Skeleton
 */
 
/**
 * A brief description of the controller.
 *
 * Your app will automatically be able to find any models from your app when you instantiate them.
 * You can also access the UserModel and RoleModel (in Dashboard) from anywhere in the framework.
 *
 * @since 1.0
 * @package Skeleton
 */
class ArticlesController extends Gdn_Controller {
   /** @var array List of objects to prep. They will be available as $this->$Name. */
   public $Uses = array('Database', 'Form', 'ArticleCategoryModel');

   /**
    * @var ArticleCategoryModel 
    */
   public $ArticleCategoryModel;
   
   /**
    * Should the articles have their options available.
    * 
    * @since 2.0.0
    * @access public
    * @var bool
    */
   public $ShowOptions = TRUE;
   
   /**
    * Unique identifier.
    * 
    * @since 2.0.0
    * @access public
    * @var int
    */
   public $CategoryID;
   
   /**
    * Category object.
    * 
    * @since 2.0.0
    * @access public
    * @var object
    */
   public $Category;
   
   /**
    * If you use a constructor, always call parent.
    * Delete this if you don't need it.
    *
    * @access public
    */
   public function __construct() {
      parent::__construct();
   }
   
   public function Index() {
      // We want to limit the number of pages on large databases because requesting a super-high page can kill the db.
      $MaxPages = C('Articles.Articles.MaxPages');
      if ($MaxPages && $Page > $MaxPages) {
         throw NotFoundException();
      }
      
      // Set criteria & get articles data
      $this->SetData('ArticleCategory', FALSE, TRUE);
      $ArticleModel = new ArticleModel();
      $ArticleModel->Watching = TRUE;
      
      // Get Article Count
      $CountArticles = $ArticleModel->GetCount();
      
      if($MaxPages)
         $CountArticles = $MaxPages * $Limit;
   
      $this->SetData('CountArticles', $CountArticles);
      
      // Get Announcements
      $this->AnnounceData = $Offset == 0 ? $ArticleModel->GetAnnouncements() : FALSE;
		$this->SetData('Announcements', $this->AnnounceData !== FALSE ? $this->AnnounceData : array(), TRUE);
      
      // Get Articles
      $this->ArticleData = $ArticleModel->GetWhere(FALSE, $Offset, $Limit);
      
      $this->SetData('Articles', $this->ArticleData, TRUE);
      $this->SetJson('Loading', $Offset . ' to ' . $Limit);

      // Build a pager
      $PagerFactory = new Gdn_PagerFactory();
		$this->EventArguments['PagerType'] = 'Pager';
		$this->FireEvent('BeforeBuildPager');
      $this->Pager = $PagerFactory->GetPager($this->EventArguments['PagerType'], $this);
      $this->Pager->ClientID = 'Pager';
      $this->Pager->Configure(
         $Offset,
         $Limit,
         $CountArticles,
         'articles/%1$s'
      );
      if (!$this->Data('_PagerUrl'))
         $this->SetData('_PagerUrl', 'articles/{Page}');
      $this->SetData('_Page', $Page);
      $this->SetData('_Limit', $Limit);
		$this->FireEvent('AfterBuildPager');
      
      // Deliver JSON data if necessary
      if ($this->_DeliveryType != DELIVERY_TYPE_ALL) {
         $this->SetJson('LessRow', $this->Pager->ToString('less'));
         $this->SetJson('MoreRow', $this->Pager->ToString('more'));
         $this->View = 'index';
      }
      
      // We don't want search engines to index these pages because they can go in through the individual categories MUCH faster.
      if ($this->Head)
         $this->Head->AddTag('meta', array('name' => 'robots', 'content' => 'noindex,noarchive'));
      
      $this->SetData('Breadcrumbs', array(array('Name' => T('Articles'), 'Url' => '/articles')));
      
      // Setup head.
      if (!$this->Data('Title')) {
         $Title = C('Garden.HomepageTitle');
         if ($Title)
            $this->Title($Title, '');
         else
            $this->Title(T('Articles'));
      }
      
      $this->Render();
   }
   
   /**
    * Show all (nested) categories.
    *
    * @since 2.0.17
    * @access public
    */
   public function Categories($CategoryIdentifier = '', $Page = '0') {
      if($CategoryIdentifier == '') {
         // Setup head.
         $this->Menu->HighlightRoute('/articles/categories');
         if (!$this->Title()) {
            $Title = C('Garden.HomepageTitle');
            if ($Title)
               $this->Title($Title, '');
            else
               $this->Title(T('All Article Categories'));
         }
         Gdn_Theme::Section('ArticleCategoryList');
               
         $this->Description(C('Garden.Description', NULL));
         
         $this->SetData('Breadcrumbs', ArticleCategoryModel::GetAncestors(GetValue('CategoryID', $this->Data('Category'))));
        
         // Set the category follow toggle before we load category data so that it affects the category query appropriately.
         //$CategoryFollowToggleModule = new CategoryFollowToggleModule($this);
         //$CategoryFollowToggleModule->SetToggle();
         
         // Get category data
         $this->ArticleCategoryModel->Watching = !Gdn::Session()->GetPreference('ShowAllCategories');
         
         $Categories = $this->ArticleCategoryModel->GetFull()->ResultArray();
         $this->SetData('Categories', $Categories);
         
         // Add modules
         //$this->AddModule('NewArticleModule');
         //$this->AddModule('ArticleFilterModule');
         //$this->AddModule('BookmarkedModule');
         //$this->AddModule($CategoryFollowToggleModule);

         $this->CanonicalUrl(Url('/articles/categories', TRUE));
         
         $Location = $this->FetchViewLocation('helper_functions', 'categories', FALSE, FALSE);
         if ($Location)
            include_once $Location;
         $this->Render();
      } else {
         $Category = ArticleCategoryModel::Categories($CategoryIdentifier);
         
         if (empty($Category)) {
            if ($CategoryIdentifier)
               throw NotFoundException();
         }
         $Category = (object)$Category;
         Gdn_Theme::Section($Category->CssClass);
         
         // Load the breadcrumbs.
         $this->SetData('Breadcrumbs', ArticleCategoryModel::GetAncestors(GetValue('CategoryID', $Category)));
         
         $this->SetData('Category', $Category, TRUE);
         
         $this->Title(htmlspecialchars(GetValue('Name', $Category, '')));
         $this->Description(GetValue('Description', $Category), TRUE);
         
         if ($Category->DisplayAs == 'Categories') {
            if (GetValue('Depth', $Category) > 0) {
               // Heading don't make sense if we've cascaded down one level.
               SaveToConfig('Articles.Categories.DoHeadings', FALSE, FALSE);
            }
            
            Trace($this->DeliveryMethod(), 'delivery method');
            Trace($this->DeliveryType(), 'delivery type');
            Trace($this->SyndicationMethod, 'syndication');
            
            if ($this->SyndicationMethod != SYNDICATION_NONE) {
               // RSS can't show a category list so just tell it to expand all categories.
               SaveToConfig('Articles.ExpandCategories', TRUE, FALSE);
            } else {
               // This category is an overview style category and displays as a category list.
               $this->View = 'category';
               $this->CategoryArticles();
               return;
            }
         }
         
         Gdn_Theme::Section('ArticleList');
         
         // Load the subtree.
         if (C('Articles.ExpandCategories'))
            $Categories = ArticleCategoryModel::GetSubtree($CategoryIdentifier);
         else
            $Categories = array($Category);
         
         $this->SetData('Categories', $Categories);

         // Setup head
         $this->AddCssFile('articles.css');
         $this->Menu->HighlightRoute('/articles/categories');
         if ($this->Head) {
            $this->AddJsFile('articles.js');
            $this->Head->AddRss($this->SelfUrl.'/feed.rss', $this->Head->Title());
         }
         
         // Set CategoryID
         $CategoryID = GetValue('CategoryID', $Category);
         $this->SetData('CategoryID', $CategoryID, TRUE);
         
         // Add modules
         //$this->AddModule('NewArticleModule');
         //$this->AddModule('ArticleFilterModule');
         //$this->AddModule('CategoriesModule');
         //$this->AddModule('BookmarkedModule');
         
         // Get a ArticleModel
         $ArticleModel = new ArticleModel();
         $CategoryIDs = ConsolidateArrayValuesByKey($this->Data('Categories'), 'CategoryID');
         $Wheres = array('a.CategoryID' => $CategoryIDs);
         $this->SetData('_ShowCategoryLink', count($CategoryIDs) > 1);
         
         // Check permission
         $this->Permission('Articles.Articles.View', TRUE, 'Category', GetValue('PermissionCategoryID', $Category));
         
         // Set article meta data.
         $this->EventArguments['PerPage'] = C('Articles.Articles.PerPage', 30);
         $this->FireEvent('BeforeGetArticles');
         list($Offset, $Limit) = OffsetLimit($Page, $this->EventArguments['PerPage']);
         
         if (!is_numeric($Offset) || $Offset < 0)
            $Offset = 0;
            
         $CountArticles = $ArticleModel->GetCount($Wheres);
         $this->SetData('CountArticles', $CountArticles);
         $this->SetData('_Limit', $Limit);
         
         // We don't wan't child categories in announcements.
         $Wheres['a.CategoryID'] = $CategoryID;
         $AnnounceData = $Offset == 0 ? $ArticleModel->GetAnnouncements($Wheres) : new Gdn_DataSet();
         $this->SetData('AnnounceData', $AnnounceData, TRUE);
         $Wheres['a.CategoryID'] = $CategoryIDs;
         
         $this->ArticleData = $this->SetData('Articles', $ArticleModel->GetWhere($Wheres, $Offset, $Limit));

         // Build a pager
         $PagerFactory = new Gdn_PagerFactory();
         $this->Pager = $PagerFactory->GetPager('Pager', $this);
         $this->Pager->ClientID = 'Pager';
         $this->Pager->Configure(
            $Offset,
            $Limit,
            $CountArticles,
            array('CategoryUrl')
         );
         $this->Pager->Record = $Category;
         PagerModule::Current($this->Pager);
         $this->SetData('_Page', $Page);

         // Set the canonical Url.
         $this->CanonicalUrl(ArticleCategoryModel::ArticleCategoryUrl($Category, PageNumber($Offset, $Limit)));
         
         // Change the controller name so that it knows to grab the article views
         $this->View = 'index';
         // Pick up the articles class
         $this->CssClass = 'Articles Category-'.GetValue('UrlCode', $Category);
         
         // Deliver JSON data if necessary
         if ($this->_DeliveryType != DELIVERY_TYPE_ALL) {
            $this->SetJson('LessRow', $this->Pager->ToString('less'));
            $this->SetJson('MoreRow', $this->Pager->ToString('more'));
            $this->View = 'index';
         }
         // Render default view.
         $this->FireEvent('BeforeArticleCategoriesRender');
         $this->Render();
      }
   }
   
   /**
    * This is a good place to include JS, CSS, and modules used by all methods of this controller.
    *
    * Always called by dispatcher before controller's requested method.
    * 
    * @since 1.0
    * @access public
    */
   public function Initialize() {
      $this->Head = new HeadModule($this);
      $this->AddJsFile('jquery.js');
      $this->AddJsFile('jquery.livequery.js');
      $this->AddJsFile('jquery.form.js');
      $this->AddJsFile('jquery.popup.js');
      $this->AddJsFile('jquery.gardenhandleajaxform.js');
      $this->AddJsFile('global.js');
      $this->AddJsFile('articles.js');

      $this->AddModule('GuestModule');
      $this->AddModule('SignedInModule');
      // Below modules - do not need to set application dependency to Articles
      // because if the following modules don't exist, there is no error.
      $this->AddModule('NewArticleModule');
      $this->AddModule('ArticleFilterModule');
      $this->AddModule('BookmarkedModule');
      $this->AddModule('ArticlesModule');
      $this->AddModule('RecentActivityModule');
      
      $this->AddCssFile('style.css');

      parent::Initialize();
   }
}
