<?php if (!defined('APPLICATION')) exit();

class PostController extends ArticlesController {
   /**
    * @var Gdn_Form
    */
   public $Form;
	
	/**
	 * @var array An associative array of form types and their locations.
	 */
	public $FormCollection;

   /**
    * Models to include.
    * 
    * @since 2.0.0
    * @access public
    * @var array
    */
   public $Uses = array('Form', 'Database', 'ArticleCommentModel', 'ArticleModel', 'ArticleDraftModel');
   
   /**
    * @var bool Whether or not to show the category dropdown.
    */
   public $ShowCategorySelector = TRUE;
   
   public function Index() {
      throw new Exception(sprintf(T('%s Not Found'), T('Page')), 404);
   }
   
   /**
    * Create or update a comment.
    *
    * @since 2.0.0
    * @access public
    * 
    * @param int $ArticleID Unique ID to add the comment to. If blank, this method will throw an error.
    */
   public function Comment($ArticleID = '') {
      // Get $ArticleID from RequestArgs if valid
      if ($ArticleID == '' && count($this->RequestArgs))
         if (is_numeric($this->RequestArgs[0]))
            $ArticleID = $this->RequestArgs[0];
            
      // If invalid $ArticleID, get from form.
      $this->Form->SetModel($this->ArticleCommentModel);
      $ArticleID = is_numeric($ArticleID) ? $ArticleID : $this->Form->GetFormValue('ArticleID', 0);
      
      // Set article data
      $this->ArticleID = $ArticleID;
      $this->Article = $Article = $this->ArticleModel->GetID($ArticleID);

      $articles_type = $this->Form->GetFormValue('articles_type', '');
      $articles_url = $this->Form->GetFormValue('articles_url', '');
      $articles_category_id = $this->Form->GetFormValue('articles_category_id', '');
      $Attributes = array();
      $articles_identifier = $this->Form->GetFormValue('articles_identifier', '');
      
      // Only allow articles identifiers of 32 chars or less - md5 if larger
      if (strlen($articles_identifier) > 32) {
         $Attributes['articles_identifier'] = $articles_identifier;
         $articles_identifier = md5($articles_identifier);
      }
         
         if ($Article) {
            $this->ArticleID = $ArticleID = $Article->ArticleID;
            $this->Form->SetValue('ArticleID', $ArticleID);
         }
      }
      
      // If no article was found, error out
      if (!$Article)
         $this->Form->AddError(T('Failed to find article for commenting.'));
      
      $PermissionCategoryID = GetValue('PermissionCategoryID', $Article);
      
      // Setup head
      $this->AddJsFile('jquery.autogrow.js');
      $this->AddJsFile('post.js');
      $this->AddJsFile('autosave.js');
      
      // Setup comment model, $CommentID, $DraftID
      $Session = Gdn::Session();
      $CommentID = isset($this->Comment) && property_exists($this->Comment, 'CommentID') ? $this->Comment->CommentID : '';
      $DraftID = isset($this->Comment) && property_exists($this->Comment, 'DraftID') ? $this->Comment->DraftID : '';
      $this->EventArguments['CommentID'] = $CommentID;
      $this->EventArguments['DraftID'] = $DraftID;
      
      // Determine whether we are editing
      $Editing = $CommentID > 0 || $DraftID > 0;
      $this->EventArguments['Editing'] = $Editing;
      
      // If closed, cancel & go to article
      if ($Article && $Article->Closed == 1 && !$Editing && !$Session->CheckPermission('Articles.Articles.Close', TRUE, 'Category', $PermissionCategoryID))
         Redirect(ArticleModel::ArticleUrl($Article));
      
      // Add hidden IDs to form
      $this->Form->AddHidden('ArticleID', $ArticleID);
      $this->Form->AddHidden('CommentID', $CommentID);
      $this->Form->AddHidden('DraftID', $DraftID, TRUE);
      
      // Check permissions
      if ($Article && $Editing) {
         // Permisssion to edit
         if ($this->Comment->InsertUserID != $Session->UserID)
            $this->Permission('Articles.Comments.Edit', TRUE, 'Category', $Article->PermissionCategoryID);
            
         // Make sure that content can (still) be edited.
         $EditContentTimeout = C('Garden.EditContentTimeout', -1);
         $CanEdit = $EditContentTimeout == -1 || strtotime($this->Comment->DateInserted) + $EditContentTimeout > time();
         if (!$CanEdit)
            $this->Permission('Articles.Comments.Edit', TRUE, 'Category', $Article->PermissionCategoryID);

         // Make sure only moderators can edit closed things
         if ($Article->Closed)
            $this->Permission('Articles.Comments.Edit', TRUE, 'Category', $Article->PermissionCategoryID);
         
      } else if ($Article) {
         // Permission to add
         $this->Permission('Articles.Comments.Add', TRUE, 'Category', $Article->PermissionCategoryID);
      }

      if (!$this->Form->IsPostBack()) {
         // Form was validly submitted
         if (isset($this->Comment)) {
            $this->Form->SetData((array)$this->Comment);
         }
            
      } else {
         // Save as a draft?
         $FormValues = $this->Form->FormValues();
         $FormValues = $this->ArticleCommentModel->FilterForm($FormValues);
         
         if ($DraftID == 0)
            $DraftID = $this->Form->GetFormValue('DraftID', 0);
         
         $Type = GetIncomingValue('Type');
         $Draft = $Type == 'Draft';
         $this->EventArguments['Draft'] = $Draft;
         $Preview = $Type == 'Preview';
         if ($Draft) {
            $DraftID = $this->ArticleDraftModel->Save($FormValues);
            $this->Form->AddHidden('DraftID', $DraftID, TRUE);
            $this->Form->SetValidationResults($this->ArticleDraftModel->ValidationResults());
         } else if (!$Preview) {
            // Fix an undefined title if we can.
            if ($this->Form->GetFormValue('Name') && GetValue('Name', $Article) == T('Undefined article subject.')) {
               $Set = array('Name' => $this->Form->GetFormValue('Name'));
               
               $this->ArticleModel->SetField(GetValue('ArticleID', $Article), $Set);
            }
            
            $Inserted = !$CommentID;
            $CommentID = $this->ArticleCommentModel->Save($FormValues);

            // The comment is now half-saved.
            if (is_numeric($CommentID) && $CommentID > 0) {
               if ($this->_DeliveryType == DELIVERY_TYPE_ALL) {
                  $this->ArticleCommentModel->Save2($CommentID, $Inserted, TRUE, TRUE);
               } else {
                  $this->JsonTarget('', Url("/articles/post/comment2.json?commentid=$CommentID&inserted=$Inserted"), 'Ajax');
               }

               $Comment = $this->ArticleCommentModel->GetID($CommentID);

               $this->EventArguments['Article'] = $Article;
               $this->EventArguments['Comment'] = $Comment;
               $this->FireEvent('AfterArticleCommentSave');
            } elseif ($CommentID === SPAM || $CommentID === UNAPPROVED) {
               $this->StatusMessage = T('CommentRequiresApprovalStatus', 'Your comment will appear after it is approved.');
            }
            
            $this->Form->SetValidationResults($this->ArticleCommentModel->ValidationResults());
            if ($CommentID > 0 && $DraftID > 0)
               $this->ArticleDraftModel->Delete($DraftID);
         }
         
         // Handle non-ajax requests first:
         if ($this->_DeliveryType == DELIVERY_TYPE_ALL) {
            if ($this->Form->ErrorCount() == 0) {
               // Make sure that this form knows what comment we are editing.
               if ($CommentID > 0)
                  $this->Form->AddHidden('CommentID', $CommentID);
               
               // If the comment was not a draft
               if (!$Draft) {
                  // Redirect to the new comment.
                  if ($CommentID > 0)
                     Redirect("article/comment/$CommentID/#Comment_$CommentID");
                  elseif ($CommentID == SPAM) {
                     $this->SetData('ArticleUrl', ArticleModel::ArticleUrl($Article));
                     $this->View = 'Spam';
                  }
               } elseif ($Preview) {
                  // If this was a preview click, create a comment shell with the values for this comment
                  $this->Comment = new stdClass();
                  $this->Comment->InsertUserID = $Session->User->UserID;
                  $this->Comment->InsertName = $Session->User->Name;
                  $this->Comment->InsertPhoto = $Session->User->Photo;
                  $this->Comment->DateInserted = Gdn_Format::Date();
                  $this->Comment->Body = ArrayValue('Body', $FormValues, '');
                  $this->Comment->Format = GetValue('Format', $FormValues, C('Garden.InputFormatter'));
                  $this->AddAsset('Content', $this->FetchView('preview'));
               } else {
                  // If this was a draft save, notify the user about the save
                  $this->InformMessage(sprintf(T('Draft saved at %s'), Gdn_Format::Date()));
               }
            }
         } else {
            // Handle ajax-based requests
            if ($this->Form->ErrorCount() > 0) {
               // Return the form errors
               $this->ErrorMessage($this->Form->Errors());
            } else {
               // Make sure that the ajax request form knows about the newly created comment or draft id
               $this->SetJson('CommentID', $CommentID);
               $this->SetJson('DraftID', $DraftID);
               
               if ($Preview) {
                  // If this was a preview click, create a comment shell with the values for this comment
                  $this->Comment = new stdClass();
                  $this->Comment->InsertUserID = $Session->User->UserID;
                  $this->Comment->InsertName = $Session->User->Name;
                  $this->Comment->InsertPhoto = $Session->User->Photo;
                  $this->Comment->DateInserted = Gdn_Format::Date();
                  $this->Comment->Body = ArrayValue('Body', $FormValues, '');
                  $this->View = 'preview';
               } elseif (!$Draft) { // If the comment was not a draft
                  // If Editing a comment 
                  if ($Editing) {
                     // Just reload the comment in question
                     $this->Offset = 1;
                     $Comments = $this->ArticleCommentModel->GetIDData($CommentID);
                     $this->SetData('Comments', $Comments);
                     $this->SetData('Article', $Article);
                     // Load the article
                     $this->ControllerName = 'article';
                     $this->View = 'comments';
                     
                     // Also define the article url in case this request came from the post screen and needs to be redirected to the article
                     $this->SetJson('ArticleUrl', ArticleModel::ArticleUrl($this->Article).'#Comment_'.$CommentID);
                  } else {
                     // If the comment model isn't sorted by DateInserted or CommentID then we can't do any fancy loading of comments.
                     $OrderBy = GetValueR('0.0', $this->ArticleCommentModel->OrderBy());
                        $this->Offset = $this->ArticleCommentModel->GetOffset($CommentID);
                        $Comments = $this->ArticleCommentModel->GetIDData($CommentID);
                        $this->SetData('Comments', $Comments);

                        $this->SetData('NewComments', TRUE);
                        
                        $this->ClassName = 'ArticleController';
                        $this->ControllerName = 'article';
                        $this->View = 'comments';
                     
                     // Make sure to set the user's article watch records
                     $CountComments = $this->ArticleCommentModel->GetCount($ArticleID);
                     $Limit = is_object($this->Data('Comments')) ? $this->Data('Comments')->NumRows() : $Article->CountComments;
                     $Offset = $CountComments - $Limit;
                     $this->ArticleCommentModel->SetWatch($this->Article, $Limit, $Offset, $CountComments);
                  }
               } else {
                  // If this was a draft save, notify the user about the save
                  $this->InformMessage(sprintf(T('Draft saved at %s'), Gdn_Format::Date()));
               }
               // And update the draft count
               $UserModel = Gdn::UserModel();
               $CountDrafts = $UserModel->GetAttribute($Session->UserID, 'CountDrafts', 0);
               $this->SetJson('My Article Drafts', T('My Article Drafts'));
               $this->SetJson('CountArticleDrafts', $CountDrafts);
            }
         }
      }
      
      // Include data for FireEvent
      if (property_exists($this,'Article'))
         $this->EventArguments['Article'] = $this->Article;
      if (property_exists($this,'Comment'))
         $this->EventArguments['Comment'] = $this->Comment;
         
      $this->FireEvent('BeforeArticleCommentRender');
      
      if ($this->DeliveryType() == DELIVERY_TYPE_DATA) {
         $Comment = $this->Data('Comments')->FirstRow(DATASET_TYPE_ARRAY);
         if ($Comment) {
            $Photo = $Comment['InsertPhoto'];
            
            if (strpos($Photo, '//') === FALSE) {
               $Photo = Gdn_Upload::Url(ChangeBasename($Photo, 'n%s'));
            }
            
            $Comment['InsertPhoto'] = $Photo;
         }
         $this->Data = array('Comment' => $Comment);
         $this->RenderData($this->Data);
      } else {
         require_once $this->FetchViewLocation('helper_functions', 'Article');
         // Render default view.
         $this->Render();
      }
   }
}
