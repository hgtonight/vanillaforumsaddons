<?php if(!defined('APPLICATION')) exit();

class SettingsController extends Gdn_Controller {
   /**
    * @var Gdn_Form
    */
   public $Form;
	
	/**
	 * @var array An associative array of form types and their locations.
	 */
	public $FormCollection;

   /**
    * Models to include.
    * 
    * @since 2.0.0
    * @access public
    * @var array
    */
   public $Uses = array('Database', 'Form', 'ArticleCategoryModel', 'ArticleModel', 'ArticleDraftModel', 'ArticleCommentModel');
   
   /**
    * @var bool Whether or not to show the category dropdown.
    */
   public $ShowCategorySelector = TRUE;
   
   /**
    * Configures navigation sidebar in Dashboard.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param $CurrentUrl Path to current location in dashboard.
    */
   public function AddSideMenu($CurrentUrl) {
      // Only add to the assets if this is not a view-only request
      if($this->_DeliveryType == DELIVERY_TYPE_ALL) {
         $SideMenu = new SideMenuModule($this);
         $SideMenu->HtmlId = '';
         $SideMenu->HighlightRoute($CurrentUrl);
         $SideMenu->Sort = C('Garden.DashboardMenu.Sort');
         $this->EventArguments['SideMenu'] = &$SideMenu;
         $this->FireEvent('GetAppSettingsMenuItems');
         $this->AddModule($SideMenu, 'Panel');
      }
   }
   
   public function Index() {
      $this->Permission('Garden.Settings.Manage');
      $this->View = 'all';
      $this->All();
   }
   
   /**
    * Pre-populate the form with values from the query string.
    * 
    * @param Gdn_Form $Form
    */
   protected function PopulateForm($Form) {
      $Get = $this->Request->Get();
      $Get = array_change_key_case($Get);
      $Values = ArrayTranslate($Get, array('name' => 'Name', 'urlcode' => 'UrlCode', 'tags' => 'Tags', 'body' => 'Body', 'summary' => 'Summary'));
      foreach($Values as $Key => $Value) {
         $Form->SetValue($Key, $Value);
      }
      
      if(isset($Get['category'])) {
         $Category = ArticleCategoryModel::Categories($Get['category']);
         if($Category && $Category['PermsArticlesAdd'])
            $Form->SetValue('CategoryID', $Category['CategoryID']);
      }
   }
   
   /**
    * Returns XHTML for a select list containing categories that the user has
    * permission to use.
    *
    * @param array $FieldName An array of category data to render.
    * @param array $Options An associative array of options for the select. Here
    * is a list of "special" options and their default values:
    *
    *   Attribute     Options                        Default
    *   ------------------------------------------------------------------------
    *   Value         The ID of the category that    FALSE
    *                 is selected.
    *   IncludeNull   Include a blank row?           TRUE
    *   CategoryData  Custom set of categories to    ArticleCategoryModel::Categories()
    *                 display.
    *
    * @return string
    */
   public function ArticleCategoryDropDown($FieldName = 'CategoryID', $Options = FALSE) {
      $Value = ArrayValueI('Value', $Options); // The selected category id
      $CategoryData = GetValue('CategoryData', $Options, ArticleCategoryModel::Categories());
      
      // Sanity check
      if(is_object($CategoryData))
         $CategoryData = (array)$CategoryData;
      else if(!is_array($CategoryData))
         $CategoryData = array();
      
      $Permission = $this->Form->GetValue('Permission', $Options, 'add');

      // Respect category permissions (remove categories that the user shouldn't see).
      $SafeCategoryData = array();
      foreach($CategoryData as $CategoryID => $Category) {
         if ($Permission == 'add' && !$Category['PermsArticlesAdd'])
            continue;
         
         if ($Value != $CategoryID) {
            if ($Category['CategoryID'] <= 0 || !$Category['PermsArticlesView'])
               continue;
         }

         $SafeCategoryData[$CategoryID] = $Category;
      }
      
      // Opening select tag
      $Return = '<select id="Form_CategoryID" name="CategoryID">\n';
      
      // Get value from attributes
      if ($Value === FALSE)
         $Value = $this->Form->GetValue($FieldName);
      if (!is_array($Value)) 
         $Value = array($Value);
         
      // Prevent default $Value from matching key of zero
      $HasValue = ($Value !== array(FALSE) && $Value !== array('')) ? TRUE : FALSE;
      
      // Start with null option?
      $IncludeNull = $this->Form->GetValue('IncludeNull', $Options);
      if ($IncludeNull === TRUE)
         $Return .= '<option value="">'.T('Select a category...').'</option>';
      elseif (is_array($IncludeNull))
         $Return .= "<option value=\"{$IncludeNull[0]}\">{$IncludeNull[1]}</option>\n";
      elseif ($IncludeNull)
         $Return .= "<option value=\"\">$IncludeNull</option>\n";
      elseif (!$HasValue)
         $Return .= '<option value=""></option>';
         
      // Show root categories as headings (ie. you can't post in them)?
      $DoHeadings = GetValue('Headings', $Options, C('Articles.Categories.DoHeadings'));
      
      // If making headings disabled and there was no default value for
      // selection, make sure to select the first non-disabled value, or the
      // browser will auto-select the first disabled option.
      $ForceCleanSelection = ($DoHeadings && !$HasValue && !$IncludeNull);
      
      // Write out the category options
      if (is_array($SafeCategoryData)) {
         foreach($SafeCategoryData as $CategoryID => $Category) {
            $Depth = GetValue('Depth', $Category, 0);
            $Disabled = $Depth == 1 && $DoHeadings;
            $Selected = in_array($CategoryID, $Value) && $HasValue;
            if ($ForceCleanSelection && $Depth > 1) {
               $Selected = TRUE;
               $ForceCleanSelection = FALSE;
            }

            $Return .= '<option value="' . $CategoryID . '"';
            if ($Disabled)
               $Return .= ' disabled="disabled"';
            else if ($Selected)
               $Return .= ' selected="selected"'; // only allow selection if NOT disabled
            
            $Name = htmlspecialchars(GetValue('Name', $Category, 'Blank Category Name'));
            if ($Depth > 1) {
               $Name = str_repeat('&#160;', 4 * ($Depth - 1)).$Name;
            }
               
            $Return .= '>' . $Name . "</option>\n";
         }
      }
      return $Return . '</select>'; 
   }
   
   public function CheckOrRadio($FieldName, $LabelCode, $ListOptions, $Attributes = array()) {
      $Form = new Gdn_Form();
      
      if (count($ListOptions) == 2 && array_key_exists(0, $ListOptions)) {
         unset($ListOptions[0]);
         $Value = array_pop(array_keys($ListOptions));
         
         // This can be represented by a checkbox.
         return $Form->CheckBox($FieldName, $LabelCode);
      } else {
         $CssClass = GetValue('ListClass', $Attributes, 'List Inline');
         
         $Result = ' <b>'.T($LabelCode)."</b> <ul class=\"$CssClass\">";
         foreach ($ListOptions as $Value => $Code) {
            $Result .= ' <li>'.$Form->Radio($FieldName, $Code, array('Value' => $Value)).'</li> ';
         }
         $Result .= '</ul>';
         return $Result;
      }
   }
   
   public function Write($CategoryUrlCode = '') {
      $this->Permission('Garden.Settings.Manage');
      $this->AddSideMenu('articles/settings/write');
      
      $this->AddJsFile('jquery.autogrow.js');
      $this->AddJsFile('settings.js');
      
      // Set article, draft, and category data
      $ArticleID = isset($this->Article) ? $this->Article->ArticleID : '';
      $DraftID = isset($this->ArticleDraft) ? $this->ArticleDraft->DraftID : 0;
      $Category = FALSE;
      if(isset($this->Article)) {
         $this->CategoryID = $this->Article->CategoryID;
         $Category = ArticleCategoryModel::Categories($this->CategoryID);
      } else if($CategoryUrlCode != '') {
         $ArticleCategoryModel = new ArticleCategoryModel();
         $Category = $ArticleCategoryModel->GetByCode($CategoryUrlCode);
         $this->CategoryID = $Category->CategoryID;
      }
      if($Category)
         $this->Category = (object)$Category;
      else {
         $this->CategoryID = 0;
         $this->Category = NULL;
      }
      
      // Override CategoryID if categories are disabled
      $UseCategories = $this->ShowCategorySelector = (bool)C('Articles.Categories.Use');
      if (!$UseCategories) 
         $CategoryUrlCode = '';

      $CategoryData = $UseCategories ? ArticleCategoryModel::Categories() : FALSE;

      // Check permission 
      if(isset($this->Article)) {
         
         // Permission to edit
         if($this->Article->InsertUserID != $Session->UserID)
            $this->Permission('Articles.Articles.Edit', TRUE, 'ArticleCategory', $this->Category->PermissionCategoryID);

         // Make sure that content can (still) be edited.
         $EditContentTimeout = C('Garden.EditContentTimeout', -1);
         $CanEdit = $EditContentTimeout == -1 || strtotime($this->Article->DateInserted) + $EditContentTimeout > time();
         if(!$CanEdit)
            $this->Permission('Articles.Articles.Edit', TRUE, 'ArticleCategory', $this->Category->PermissionCategoryID);
         
         // Make sure only moderators can edit closed things
         if($this->Article->Closed)
            $this->Permission('Articles.Articles.Edit', TRUE, 'ArticleCategory', $this->Category->PermissionCategoryID);

         $this->Title(T('Edit Article'));
      } else {
         // Permission to add
         $this->Permission('Articles.Articles.Add');
         $this->Title(T('New Article'));
      }
      
      // Set the model on the form
      $this->Form->SetModel($this->ArticleModel);
      if($this->Form->IsPostBack() == FALSE) {
         $this->Form->AddHidden('CodeIsDefined', '0');
         // Prep form with current data for editing
         if(isset($this->Article)) {
            $this->Form->SetData($this->Article);
         } elseif(isset($this->ArticleDraft)) {
            $this->Form->SetData($this->ArticleDraft);
         } else {
            if($this->Category !== NULL)
               $this->Form->SetData(array('CategoryID' => $this->Category->CategoryID));
            $this->PopulateForm($this->Form);
         }
            
      } else { // Form was submitted
         // Save as a draft?
         $FormValues = $this->Form->FormValues();
         $FormValues = $this->ArticleModel->FilterForm($FormValues);
         $this->DeliveryType(GetIncomingValue('DeliveryType', $this->_DeliveryType));
         if($DraftID == 0)
            $DraftID = $this->Form->GetFormValue('DraftID', 0);
            
         $Draft = $this->Form->ButtonExists('Save Draft') ? TRUE : FALSE;
         $Preview = $this->Form->ButtonExists('Preview') ? TRUE : FALSE;
         if(!$Preview) {
            if(!is_object($this->Category) && is_array($CategoryData) && isset($FormValues['CategoryID']))
               $this->Category = $this->Form->GetValue($FormValues['CategoryID'], $CategoryData);

            if(is_object($this->Category)) {
               // Check category permissions.
               if($this->Form->GetFormValue('Announce', '') && !$Session->CheckPermission('Articles.Articles.Announce', TRUE, 'ArticleCategory', $this->Category->PermissionCategoryID))
                  $this->Form->AddError('You do not have permission to announce in this category', 'Announce');

               if($this->Form->GetFormValue('Close', '') && !$Session->CheckPermission('Articles.Articles.Close', TRUE, 'ArticleCategory', $this->Category->PermissionCategoryID))
                  $this->Form->AddError('You do not have permission to close in this category', 'Close');

               if(!isset($this->Article) && !$Session->CheckPermission('Articles.Articles.Add', TRUE, 'ArticleCategory', $this->Category->PermissionCategoryID))
                  $this->Form->AddError('You do not have permission to start articles in this category', 'CategoryID');
            }

            // Make sure that the title will not be invisible after rendering
            $Name = trim($this->Form->GetFormValue('Name', ''));
            if($Name != '' && Gdn_Format::Text($Name) == '')
               $this->Form->AddError(T('You have entered an invalid article title'), 'Name');
            else {
               // Trim the name.
               $FormValues['Name'] = $Name;
               $this->Form->SetFormValue('Name', $Name);
            }
            
                  
            $UrlCode = $FormValues['UrlCode'];
            if($UrlCode == '') {
               $UrlCode = htmlspecialchars($Name);
               $UrlCode = Gdn_Format::Url($UrlCode);
               $this->Form->SetFormValue('UrlCode', $UrlCode);
            }
            
            if($this->Form->ErrorCount() == 0) {
               if($Draft) {
                  $DraftID = $this->ArticleDraftModel->Save($FormValues);
                  $ValidationResults = $this->ArticleDraftModel->ValidationResults();
               } else {
                  $ArticleID = $this->ArticleModel->Save($FormValues, $this->ArticleCommentsModel);
                  $ValidationResults = $this->ArticleModel->ValidationResults();
                  
                  if($ArticleID > 0) {
                     if($DraftID > 0)
                        $this->ArticleDraftModel->Delete($DraftID);
                  }
               }

               $ValidationResults = $this->ArticleModel->CheckValidationResults($ValidationResults);
               
               $this->Form->SetValidationResults($ValidationResults);
            }
         } else {
            // If this was a preview click, create a article/comment shell with the values for this comment
            $this->Article = new stdClass();
            $this->Article->Name = $this->Form->$this->Form->GetValue('Name', '');
            $this->ArticleComment = new stdClass();
            $this->ArticleComment->InsertUserID = $Session->User->UserID;
            $this->ArticleComment->InsertName = $Session->User->Name;
            $this->ArticleComment->InsertPhoto = $Session->User->Photo;
            $this->ArticleComment->DateInserted = Gdn_Format::Date();
            $this->ArticleComment->UrlCode = ArrayValue('UrlCode', $FormValues, '');
            $this->ArticleComment->Body = ArrayValue('Body', $FormValues, '');
            $this->ArticleComment->Summary = ArrayValue('Summary', $FormValues, '');
            $this->ArticleComment->Format = $this->Form->GetValue('Format', $FormValues, C('Garden.InputFormatter'));
            
            $this->EventArguments['Article'] = &$this->Article;
            $this->EventArguments['Comment'] = &$this->ArticleComment;
            $this->FireEvent('BeforeArticlePreview');

            if($this->_DeliveryType == DELIVERY_TYPE_ALL) {
               $this->AddAsset('Content', $this->FetchView('preview'));
            } else {
               $this->View = 'preview';
            }
         }
         if($this->Form->ErrorCount() > 0) {
            // Return the form errors
            $this->ErrorMessage($this->Form->Errors());
         } else if($ArticleID > 0 || $DraftID > 0) {
            // Make sure that the ajax request form knows about the newly created article or draft id
            $this->SetJson('ArticleID', $ArticleID);
            $this->SetJson('DraftID', $DraftID);
            
            if(!$Preview) {
               // If the article was not a draft
               if(!$Draft) {
                  // Redirect to the new article
                  $Article = $this->ArticleModel->GetID($ArticleID);
                  $this->SetData('Article', $Article);
                  $this->EventArguments['Article'] = $Article;
                  $this->FireEvent('AfterArticleSave');
                  
                  
                  $ArticleModel = new ArticleModel();
                  if($this->_DeliveryType == DELIVERY_TYPE_ALL) {
                     Redirect($ArticleModel->ArticleUrl($Article));
                  } else {
                     $this->RedirectUrl = $ArticleModel->ArticleUrl($Article, '', TRUE);
                  }
               } else {
                  // If this was a draft save, notify the user about the save
                  $this->InformMessage(sprintf(T('Draft saved at %s'), Gdn_Format::Date()));
               }
            }
         }
      }
      
      // Add hidden fields for editing
      $this->Form->AddHidden('ArticleID', $ArticleID);
      $this->Form->AddHidden('DraftID', $ArticleDraftID, TRUE);
      
      $this->FireEvent('BeforeArticleRender');
      
      if ($this->CategoryID)
         $Breadcrumbs = ArticleCategoryModel::GetAncestors($this->CategoryID);
      else
         $Breadcrumbs = array();
      $Breadcrumbs[] = array('Name' => $this->Data('Title'), 'Url' => '/articles/settings/write');
      
		$this->SetData('Breadcrumbs', $Breadcrumbs);
      
      $this->SetData('_AnnounceOptions', $this->AnnounceOptions());
      
      $this->Render();
   }
   
   /**
    * Edit an article (wrapper for SettingsController::Write). 
    *
    * Will throw an error if both params are blank.
    *
    * @since 2.0.0
    * @access public
    * 
    * @param int $ArticleID Unique ID of the article to edit.
    * @param int $DraftID Unique ID of draft article to edit.
    */
   public function EditArticle($ArticleID = '', $DraftID = '') {
      if ($DraftID != '') {
         $this->Draft = $this->ArticleDraftModel->GetID($DraftID);
         $this->CategoryID = $this->Draft->CategoryID;
      } else {
         $this->SetData('Article', $this->ArticleModel->GetID($ArticleID), TRUE);
         $this->CategoryID = $this->Article->CategoryID;
      }
      
      $this->Form->RemoveFormValue('Format');
      // Set view and render
      $this->View = 'Write';
      $this->Write($this->CategoryID);
   }
   
   public function AnnounceOptions() {
      $Result = array(
         0  => '@'.T("Don't announce.")
      );
      
      if (C('Articles.Categories.Use')) {
         $Result = array_merge($Result, array(
            1 => '@'.sprintf(sprintf(T('In <b>%s</b> and recent articles.'), T('the category'))),
            2 => '@'.sprintf(T('In <b>%s.</b>'), T('the category'))
         ));
      } else {
         $Result = array_merge($Result, array(
            1 => '@'.T('In recent articles.'),
         ));
      }
      
      return $Result;
   }
   
   public function All() {
      $this->Permission('Garden.Settings.Manage');
      $this->AddSideMenu('articles/settings/all');
      $this->Title(T('All Articles'));
      $this->Render();
   }
   
   /**
    * Enabling and disabling categories from list.
    * 
    * @since 2.0.0
    * @access public
    */
   public function ManageCategories() {
      // Check permission
      $this->Permission('Garden.Settings.Manage');
      
      $this->AddSideMenu('articles/settings/managecategories');
      
      // Nested sortable always breaks when we update jQuery so we give it it's own old version of jquery.
      $this->RemoveJsFile('jquery.js');
      $this->AddJsFile('js/library/nestedSortable.1.3.4/jquery-1.7.2.min.js', '', array('Sort' => 0));
      
      $this->AddJsFile('categories.js');
      $this->AddJsFile('jquery.alphanumeric.js');
      $this->AddJsFile('js/library/nestedSortable.1.3.4/jquery-ui-1.8.11.custom.min.js');
      $this->AddJsFile('js/library/nestedSortable.1.3.4/jquery.ui.nestedSortable.js');
      $this->Title(T('Article Categories'));
      
      // Get category data
      $this->SetData('CategoryData', $this->ArticleCategoryModel->GetAll('TreeLeft'), TRUE);
      
      // Enable/Disable Categories
      if (Gdn::Session()->ValidateTransientKey(GetValue(1, $this->RequestArgs))) {
         $Toggle = GetValue(0, $this->RequestArgs, '');
         if ($Toggle == 'enable') {
            SaveToConfig('Articles.Categories.Use', TRUE);
         } else if ($Toggle == 'disable') {
            SaveToConfig('Articles.Categories.Use', FALSE);
         }
         Redirect('articles/settings/managecategories');
      }
      
      // Setup & save forms
      $Validation = new Gdn_Validation();
      $ConfigurationModel = new Gdn_ConfigurationModel($Validation);
      $ConfigurationModel->SetField(array(
         'Articles.Categories.MaxDisplayDepth',
         'Articles.Categories.DoHeadings',
         'Articles.Categories.HideModule'
      ));
      
      // Set the model on the form.
      $this->Form->SetModel($ConfigurationModel);
      
      // Define MaxDepthOptions
      $DepthData = array();
      $DepthData['2'] = sprintf(T('more than %s deep'), Plural(1, '%s level', '%s levels'));
      $DepthData['3'] = sprintf(T('more than %s deep'), Plural(2, '%s level', '%s levels'));
      $DepthData['4'] = sprintf(T('more than %s deep'), Plural(3, '%s level', '%s levels')) ;
      $DepthData['0'] = T('never');
      $this->SetData('MaxDepthData', $DepthData);
      
      // If seeing the form for the first time...
      if ($this->Form->AuthenticatedPostBack() === FALSE) {
         // Apply the config settings to the form.
         $this->Form->SetData($ConfigurationModel->Data);
      } else {
         if ($this->Form->Save() !== FALSE)
            $this->InformMessage(T("Your settings have been saved."));
      }
      
      // Render default view
      $this->Render();
   }
   
   /**
    * Adding a new category.
    * 
    * @since 2.0.0
    * @access public
    */
   public function AddCategory() {
      // Check permission
      $this->Permission('Garden.Settings.Manage');
      
      // Set up head
      $this->AddJsFile('jquery.alphanumeric.js');
      $this->AddJsFile('categories.js');
      $this->AddJsFile('jquery.gardencheckboxgrid.js');
      $this->Title(T('Add Article Category'));
      $this->AddSideMenu('articles/settings/managecategories');
      
      // Prep models
      $RoleModel = new RoleModel();
      $PermissionModel = Gdn::PermissionModel();
      $this->Form->SetModel($this->ArticleCategoryModel);
      
      // Load all roles with editable permissions.
      $this->RoleArray = $RoleModel->GetArray();
      
      $this->FireEvent('AddEditCategory');

      if ($this->Form->IsPostBack() == FALSE) {
         $this->Form->AddHidden('CodeIsDefined', '0');
      } else {
         // Form was validly submitted
         $IsParent = $this->Form->GetFormValue('IsParent', '0');
         $this->Form->SetFormValue('AllowArticles', $IsParent == '1' ? '0' : '1');
         
         $CategoryID = $this->Form->Save();
         if ($CategoryID) {
            $Category = ArticleCategoryModel::Categories($CategoryID);
            $this->SetData('Category', $Category);
            
            if ($this->DeliveryType() == DELIVERY_TYPE_ALL)
               Redirect('articles/settings/managecategories');
         } else {
            unset($CategoryID);
         }
      }
      // Get all of the currently selected role/permission combinations for this junction.
      $Permissions = $PermissionModel->GetJunctionPermissions(array('JunctionID' => isset($CategoryID) ? $CategoryID : 0), 'Category');
      $Permissions = $PermissionModel->UnpivotPermissions($Permissions, TRUE);
   
      if ($this->DeliveryType() == DELIVERY_TYPE_ALL) {
         $this->SetData('PermissionData', $Permissions, TRUE);
      }
      
      // Render default view
      $this->Render();      
   }
   
   /**
    * Editing a category.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param int $CategoryID Unique ID of the category to be updated.
    */
   public function EditCategory($CategoryID = '') {
      // Check permission
      $this->Permission('Garden.Settings.Manage');
      
      // Set up models
      $RoleModel = new RoleModel();
      $PermissionModel = Gdn::PermissionModel();
      $this->Form->SetModel($this->ArticleCategoryModel);
      
      if (!$CategoryID && $this->Form->IsPostBack()) {
         if ($ID = $this->Form->GetFormValue('CategoryID'))
            $CategoryID = $ID;
      }
      
      // Get category data
      $this->Category = $this->ArticleCategoryModel->GetID($CategoryID);
      $this->Category->CustomPermissions = $this->Category->CategoryID == $this->Category->PermissionCategoryID;
      
      // Set up head
      $this->AddJsFile('jquery.alphanumeric.js');
      $this->AddJsFile('categories.js');
      $this->AddJsFile('jquery.gardencheckboxgrid.js');
      $this->Title(T('Edit Article Category'));
      
      $this->AddSideMenu('articles/settings/managecategories');
      
      // Make sure the form knows which item we are editing.
      $this->Form->AddHidden('CategoryID', $CategoryID);
      $this->SetData('CategoryID', $CategoryID);
      
      // Load all roles with editable permissions
      $this->RoleArray = $RoleModel->GetArray();
      
      $this->FireEvent('AddEditCategory');
      
      if ($this->Form->IsPostBack() == FALSE) {
         $this->Form->SetData($this->Category);
         $this->Form->SetValue('CustomPoints', $this->Category->PointsCategoryID == $this->Category->CategoryID);
      } else {
         $Upload = new Gdn_Upload();
         $TmpImage = $Upload->ValidateUpload('PhotoUpload', FALSE);
         if ($TmpImage) {
            
            // Generate the target image name
            $TargetImage = $Upload->GenerateTargetName(PATH_UPLOADS);
            $ImageBaseName = pathinfo($TargetImage, PATHINFO_BASENAME);

            // Save the uploaded image
            $Parts = $Upload->SaveAs(
               $TmpImage,
               $ImageBaseName
            );
            $this->Form->SetFormValue('Photo', $Parts['SaveName']);
         }
         $this->Form->SetFormValue('CustomPoints', (bool)$this->Form->GetFormValue('CustomPoints'));
         
         if ($this->Form->Save()) {
            $Category = ArticleCategoryModel::Categories($CategoryID);
            $this->SetData('Category', $Category);
            
            if ($this->DeliveryType() == DELIVERY_TYPE_ALL)
               Redirect('articles/settings/managecategories');
         }
      }
       
      // Get all of the currently selected role/permission combinations for this junction.
      $Permissions = $PermissionModel->GetJunctionPermissions(array('JunctionID' => $CategoryID), 'Category', '', array('AddDefaults' => !$this->Category->CustomPermissions));
      $Permissions = $PermissionModel->UnpivotPermissions($Permissions, TRUE);
      
      if ($this->DeliveryType() == DELIVERY_TYPE_ALL)
         $this->SetData('PermissionData', $Permissions, TRUE);
      
      // Render default view
      $this->Render();
   }
   
   /**
    * Deleting a category.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param int $CategoryID Unique ID of the category to be deleted.
    */
   public function DeleteCategory($CategoryID = FALSE) {
      // Check permission
      $this->Permission('Garden.Settings.Manage');
      
      // Set up head
      $this->AddJsFile('categories.js');
      $this->Title(T('Delete Article Category'));
      $this->AddSideMenu('articles/settings/managecategories');

      // Get category data
      $this->Category = $this->ArticleCategoryModel->GetID($CategoryID);
      
      
      if (!$this->Category) {
         $this->Form->AddError('The specified category could not be found.');
      } else {
         // Make sure the form knows which item we are deleting.
         $this->Form->AddHidden('CategoryID', $CategoryID);
         
         // Get a list of categories other than this one that can act as a replacement
         $this->OtherCategories = $this->ArticleCategoryModel->GetWhere(
            array(
               'CategoryID <>' => $CategoryID,
               'AllowArticles' => $this->Category->AllowArticles, // Don't allow a category with article to be the replacement for one without articles (or vice versa)
               'CategoryID >' => 0
            ),
            'Sort'
         );
         
         if (!$this->Form->AuthenticatedPostBack()) {
            $this->Form->SetFormValue('DeleteArticles', '1'); // Checked by default
         } else {
            $ReplacementCategoryID = $this->Form->GetValue('ReplacementCategoryID');
            $ReplacementCategory = $this->ArticleCategoryModel->GetID($ReplacementCategoryID);
            // Error if:
            // 1. The category being deleted is the last remaining category that
            // allows articles.
            if ($this->Category->AllowArticles == '1'
               && $this->OtherCategories->NumRows() == 0)
               $this->Form->AddError('You cannot remove the only remaining category that allows articles');
            
            if ($this->Form->ErrorCount() == 0) {
               // Go ahead and delete the category
               try {
                  $this->ArticleCategoryModel->Delete($this->Category, $this->Form->GetValue('ReplacementCategoryID'));
               } catch (Exception $ex) {
                  $this->Form->AddError($ex);
               }
               if ($this->Form->ErrorCount() == 0) {
                  $this->RedirectUrl = Url('articles/settings/managecategories');
                  $this->InformMessage(T('Deleting category...'));
               }
            }
         }
      }
      
      // Render default view
      $this->Render();
   }
   
   /**
    * Deleting a category photo.
    * 
    * @since 2.1
    * @access public
    *
    * @param int $CategoryID Unique ID of the category to have its photo deleted.
    */
   public function DeleteCategoryPhoto($CategoryID = FALSE, $TransientKey = '') {
      // Check permission
      $this->Permission('Garden.Settings.Manage');
      
      $RedirectUrl = 'articles/settings/editcategory/'.$CategoryID;
      
      if (Gdn::Session()->ValidateTransientKey($TransientKey)) {
         // Do removal, set message, redirect
         $CategoryModel = new ArticleCategoryModel();
         $Category = $CategoryModel->GetID($CategoryID);
         $Upload = new Gdn_Upload();
         $Upload->Delete($Category->Photo);
         $CategoryModel->SetField($CategoryID, 'Photo', NULL);
         $this->InformMessage(T('Category photo has been deleted.'));
      }
      if ($this->_DeliveryType == DELIVERY_TYPE_ALL) {
          Redirect($RedirectUrl);
      } else {
         $this->ControllerName = 'Home';
         $this->View = 'FileNotFound';
         $this->RedirectUrl = Url($RedirectUrl);
         $this->Render();
      }
   }
   
   /**
    * Sorting display order of categories.
    *
    * Accessed by ajax so its default is to only output true/false.
    * 
    * @since 2.0.0
    * @access public
    */
   public function SortCategories() {
      // Check permission
      $this->Permission('Garden.Settings.Manage');
      
      // Set delivery type to true/false
      $TransientKey = GetIncomingValue('TransientKey');
      if (Gdn::Request()->IsPostBack()) {
         $TreeArray = GetValue('TreeArray', $_POST);
         $Saves = $this->ArticleCategoryModel->SaveTree($TreeArray);
         $this->SetData('Result', TRUE);
         $this->SetData('Saves', $Saves);
      }
      
      // Renders true/false rather than template  
      $this->Render();
   }   
   
   public function Advanced() {
      $this->Permission('Garden.Settings.Manage');
      
      // Load up config options we'll be setting
      $Validation = new Gdn_Validation();
      $ConfigurationModel = new Gdn_ConfigurationModel($Validation);
      $ConfigurationModel->SetField(array(
         'Articles.Articles.PerPage',
         'Articles.Comments.PerPage'
      ));
      
      // Set the model on the form.
      $this->Form->SetModel($ConfigurationModel);
      
      // If seeing the form for the first time...
      if($this->Form->AuthenticatedPostBack() === FALSE) {
         // Apply the config settings to the form.
         $this->Form->SetData($ConfigurationModel->Data);
      } else {
         // Form was validly submitted
         // Define some validation rules for the fields being saved
         $ConfigurationModel->Validation->ApplyRule('Articles.Articles.PerPage', 'Required');
         $ConfigurationModel->Validation->ApplyRule('Articles.Articles.PerPage', 'Integer');
         $ConfigurationModel->Validation->ApplyRule('Articles.Comments.PerPage', 'Required');
         $ConfigurationModel->Validation->ApplyRule('Articles.Comments.PerPage', 'Integer');
         
         // Save new settings
         $Saved = $this->Form->Save();
         if($Saved) {
            $this->InformMessage(T("Your changes have been saved."));
         }
      }
      
      //$this->AddJsFile('write.js');
      
      $this->AddSideMenu('articles/settings/advanced');
      $this->Title(T('Articles Settings'));
      $this->Render();
   }
   
   public function Initialize() {
      $this->Head = new HeadModule($this);
      $this->AddJsFile('jquery.js');
      $this->AddJsFile('jquery.livequery.js');
      $this->AddJsFile('jquery.form.js');
      $this->AddJsFile('jquery.popup.js');
      $this->AddJsFile('jquery.gardenhandleajaxform.js');
      $this->AddJsFile('global.js');

      $this->AddCssFile('admin.css');
         
      // Call Gdn_Controller's Initialize() as well.
      $this->MasterView = 'admin';
      parent::Initialize();
      Gdn_Theme::Section('Dashboard');
   }
}
