<?php if (!defined('APPLICATION')) exit();

class ArticleController extends ArticlesController {
   /**
    * Models to include.
    * 
    * @since 2.0.0
    * @access public
    * @var array
    */
   public $Uses = array('ArticleModel', 'ArticleCommentModel', 'Form');
   
   /**
    * Unique identifier.
    * 
    * @since 2.0.0
    * @access public
    * @var array
    */
   public $CategoryID;
   
   /**
    * @var ArticleModel 
    */
   public $ArticleModel;
   
   
   public function __get($Name) {
      switch ($Name) {
         case 'CommentData':
            Deprecated('ArticleController->CommentData', "ArticleController->Data('Comments')");
            return $this->Data('Comments');
            break;
      }
      throw new Exception("ArticleController->$Name not found.", 400);
   }
   
   /**
    * Redirect to the url specified by the discussion.
    * @param array|object $Discussion
    */
   protected function RedirectArticle($Article) {
      $Body = Gdn_Format::To(GetValue('Body', $Article), GetValue('Format', $Article));
      if (preg_match('`href="([^"]+)"`i', $Body, $Matches)) {
         $Url = $Matches[1];
         Redirect($Url, 301);
      }
   }
   
   /**
    * Default single article display.
    * 
    * @since 2.0.0
    * @access public
    * 
    * @param int $ArticleID Unique article ID
    * @param string $ArticleStub URL-safe title slug
    * @param int $Page The current page of comments
    */
   public function Index($ArticleYear = '', $ArticleSlub = '', $Page = '') {
      // Setup head
      $Session = Gdn::Session();
      $this->AddJsFile('jquery.autogrow.js');
      $this->AddJsFile('article.js');
      $this->AddJsFile('autosave.js');
      Gdn_Theme::Section('Article');
      
      // Load the article record
      if($ArticleYear != '' && is_numeric($ArticleYear) && $ArticleSlub != '') {
         if (!array_key_exists('Article', $this->Data))
            $this->SetData('Article', $this->ArticleModel->GetUrlCode($ArticleSlub), TRUE);
      }
      
      $ValidArticleYear = $this->Article->DateInserted;
      $ValidArticleYear = date('Y', strtotime($ValidArticleYear));

      if(!is_object($this->Article) || ($ArticleYear != $ValidArticleYear)) {
         throw new Exception(sprintf(T('%s Not Found'), T('Article')), 404);
      }
      
      $ArticleID = $this->Article->ArticleID;
      
      // Define the query offset & limit.
      $Limit = C('Articles.Comments.PerPage', 30);

      $OffsetProvided = $Page != '';
      list($Offset, $Limit) = OffsetLimit($Page, $Limit);
      
      // Check permissions
      $this->Permission('Articles.Articles.View', TRUE, 'Category', $this->Article->PermissionCategoryID);
      $this->SetData('CategoryID', $this->CategoryID = $this->Article->CategoryID, TRUE);
      
      if (strcasecmp(GetValue('Type', $this->Article), 'redirect') === 0) {
         $this->RedirectArticle($this->Article);
      }
      
      $Category = ArticleCategoryModel::Categories($this->Article->CategoryID);
      $this->SetData('Category', $Category);
      
      if ($CategoryCssClass = GetValue('CssClass', $Category))
         Gdn_Theme::Section($CategoryCssClass);
      
      $this->SetData('Breadcrumbs', ArticleCategoryModel::GetAncestors($this->CategoryID));
      
      // Setup
      $this->Title($this->Article->Name);

      // Actual number of comments, excluding the article itself.
      $ActualResponses = $this->Article->CountComments;

      $this->Offset = $Offset;
      if (C('Articles.Comments.AutoOffset')) {
         if (!is_numeric($this->Offset) || $this->Offset < 0 || !$OffsetProvided) {
            // Round down to the appropriate offset based on the user's read comments & comments per page
            $CountCommentWatch = $this->Article->CountCommentWatch > 0 ? $this->Article->CountCommentWatch : 0;
            if ($CountCommentWatch > $ActualResponses)
               $CountCommentWatch = $ActualResponses;

            // (((67 comments / 10 perpage) = 6.7) rounded down = 6) * 10 perpage = offset 60;
            $this->Offset = floor($CountCommentWatch / $Limit) * $Limit;
         }
         if ($ActualResponses <= $Limit)
            $this->Offset = 0;

         if ($this->Offset == $ActualResponses)
            $this->Offset -= $Limit;
      } else {
         if ($this->Offset == '')
            $this->Offset = 0;
      }

      if ($this->Offset < 0)
         $this->Offset = 0;
      
      
      $LatestItem = $this->Article->CountCommentWatch;
      if ($LatestItem === NULL) {
         $LatestItem = 0;
      } elseif ($LatestItem < $this->Article->CountComments) {
         $LatestItem += 1;
      }
      
      $this->SetData('_LatestItem', $LatestItem);
      
      // Set the canonical url to have the proper page title.
      $this->CanonicalUrl($this->ArticleModel->ArticleUrl($this->Article, PageNumber($this->Offset, $Limit, 0, FALSE)));
      
      // Load the comments
      $this->SetData('Comments', $this->ArticleCommentModel->Get($ArticleID, $Limit, $this->Offset));
      
      $PageNumber = PageNumber($this->Offset, $Limit);
      $this->SetData('Page', $PageNumber);
      $this->_SetOpenGraph();
      
      include_once(PATH_LIBRARY.'/vendors/simplehtmldom/simple_html_dom.php');
      if ($PageNumber == 1) {
         $this->Description(SliceParagraph(Gdn_Format::PlainText($this->Article->Body, $this->Article->Format), 160));
         // Add images to head for open graph
         $Dom = str_get_html(Gdn_Format::To($this->Article->Body, $this->Article->Format));
      } else {
         $this->Data['Title'] .= sprintf(T(' - Page %s'), PageNumber($this->Offset, $Limit));
         
         $FirstComment = $this->Data('Comments')->FirstRow();
         $FirstBody = GetValue('Body', $FirstComment);
         $FirstFormat = GetValue('Format', $FirstComment);
         $this->Description(SliceParagraph(Gdn_Format::PlainText($FirstBody, $FirstFormat), 160));
         // Add images to head for open graph
         $Dom = str_get_html(Gdn_Format::To($FirstBody, $FirstFormat));
      }

      if ($Dom) {
         foreach($Dom->find('img') as $img) {
            if (isset($img->src))
               $this->Image($img->src);
         }
      }
         
      // Make sure to set the user's article watch records
      $this->ArticleCommentModel->SetWatch($this->Article, $Limit, $this->Offset, $this->Article->CountComments);

      // Build a pager
      $PagerFactory = new Gdn_PagerFactory();
		$this->EventArguments['PagerType'] = 'Pager';
		$this->FireEvent('BeforeBuildPager');
      $this->Pager = $PagerFactory->GetPager($this->EventArguments['PagerType'], $this);
      $this->Pager->ClientID = 'Pager';
         
      $this->Pager->Configure(
         $this->Offset,
         $Limit,
         $ActualResponses,
         array('ArticleUrl')
      );
      $this->Pager->Record = $this->Article;
      PagerModule::Current($this->Pager);
      $this->FireEvent('AfterBuildPager');
      
      // Define the form for the comment input
      $this->Form = Gdn::Factory('Form', 'Comment');
      $this->Form->Action = Url('/articles/post/comment/');
      $this->ArticleID = $this->Article->ArticleID;
      $this->Form->AddHidden('ArticleID', $this->ArticleID);
      $this->Form->AddHidden('CommentID', '');

      // Look in the session stash for a comment
      $StashComment = $Session->Stash('CommentForArticleID_'.$this->Article->ArticleID, '', FALSE);
      if ($StashComment)
         $this->Form->SetFormValue('Body', $StashComment);
         
      // Retrieve & apply the draft if there is one:
      if (Gdn::Session()->UserID) {
         $DraftModel = new DraftModel();
         $Draft = $DraftModel->Get($Session->UserID, 0, 1, $this->Article->ArticleID)->FirstRow();
         $this->Form->AddHidden('DraftID', $Draft ? $Draft->DraftID : '');
         if ($Draft && !$this->Form->IsPostBack()) {
            $this->Form->SetValue('Body', $Draft->Body);
            $this->Form->SetValue('Format', $Draft->Format);
         }
      }
      
      // Deliver JSON data if necessary
      if ($this->_DeliveryType != DELIVERY_TYPE_ALL) {
         $this->SetJson('LessRow', $this->Pager->ToString('less'));
         $this->SetJson('MoreRow', $this->Pager->ToString('more'));
         $this->View = 'comments';
      }
      
		// Inform moderator of checked comments in this article
		$CheckedComments = $Session->GetAttribute('CheckedComments', array());
		if (count($CheckedComments) > 0)
			ModerationController::InformCheckedComments($this);

      // Add modules
      //$this->AddModule('ArticleFilterModule');
      //$this->AddModule('NewArticleModule');
      //$this->AddModule('CategoriesModule');
      //$this->AddModule('BookmarkedModule');
      
		$this->CanEditComments = Gdn::Session()->CheckPermission('Articles.Comments.Edit', TRUE, 'Category', 'any') && C('Articles.AdminCheckboxes.Use');

      // Report the article id so js can use it.      
      $this->AddDefinition('ArticleID', $ArticleID);
      
      $this->FireEvent('BeforeArticleRender');
      $this->Render();
   }
   
   /**
    * Allows user to bookmark or unbookmark a article.
    *
    * If the article isn't bookmarked by the user, this bookmarks it.
    * If it is already bookmarked, this unbookmarks it.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param int $ArticleID Unique article ID.
    */
   public function Bookmark($ArticleID) {
      // Make sure we are posting back.
      if (!$this->Request->IsPostBack())
         throw PermissionException('Javascript');
      
      $Session = Gdn::Session();
      
      if ($Session->UserID == 0)
         throw PermissionException('SignedIn');
      
      $Article = $this->ArticleModel->GetID($ArticleID);
      
      if (!$Article)
         throw NotFoundException('Article');
      
      $State = $this->ArticleModel->BookmarkArticle($ArticleID, $Session->UserID, $Article);

      // Update the user's bookmark count
      $CountBookmarks = $this->ArticleModel->SetUserBookmarkCount($Session->UserID);
      $this->JsonTarget('.User-CountBookmarks', (string)$CountBookmarks);
      
      // Return the appropriate bookmark.
      require_once $this->FetchViewLocation('helper_functions', 'Articles');
      $Html = BookmarkButton($Article);
//      $this->JsonTarget(".Section-ArticleList #Article_$ArticleID .Bookmark,.Section-Article .PageTitle .Bookmark", $Html, 'ReplaceWith');
      $this->JsonTarget("!element", $Html, 'ReplaceWith');

      // Add the bookmark to the bookmarks module.
      if($State) {
         // Grab the individual bookmark and send it to the client.
         $Bookmarks = new BookmarkedModule($this);
         if($CountBookmarks == 1) {
            // When there is only one bookmark we have to get the whole module.
            $Target = '#Panel';
            $Type = 'Append';
            $Bookmarks->GetData();
            $Data = $Bookmarks->ToString();
         } else {
            $Target = '#Bookmark_List';
            $Type = 'Prepend';
            $Loc = $Bookmarks->FetchViewLocation('article');
            
            ob_start();
            include($Loc);
            $Data = ob_get_clean();
         }
         
         $this->JsonTarget($Target, $Data, $Type);
      } else {
         // Send command to remove bookmark html.
         if($CountBookmarks == 0) {
            $this->JsonTarget('#Bookmarks', NULL, 'Remove');
         } else {
            $this->JsonTarget('#Bookmark_'.$ArticleID, NULL, 'Remove');
         }
      }
      
      $this->Render('Blank', 'Utility', 'Dashboard');
   }
   
   /**
    * Allows user to close or re-open a article.
    *
    * If the article isn't closed, this closes it. If it is already 
    * closed, this re-opens it. Closed articles may not have new 
    * comments added to them.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param int $ArticleID Unique article ID.
    * @param bool $Close Whether or not to close the article.
    */
   public function Close($ArticleID = '', $Close = TRUE, $From = 'list') {
      // Make sure we are posting back.
      if (!$this->Request->IsPostBack())
         throw PermissionException('Javascript');
      
      $Article = $this->ArticleModel->GetID($ArticleID);
      
      if (!$Article)
         throw NotFoundException('Article');
      
      $this->Permission('Articles.Articles.Close', TRUE, 'Category', $Article->PermissionCategoryID);
      
      // Close the article.
      $this->ArticleModel->SetField($ArticleID, 'Closed', $Close);
      $Article->Closed = $Close;
      
      // Redirect to the front page
      if ($this->_DeliveryType === DELIVERY_TYPE_ALL) {
         $Target = GetIncomingValue('Target', 'articles');
         Redirect($Target);
      }
      
      $this->SendOptions($Article);
      
      if ($Close) {
         require_once $this->FetchViewLocation('helper_functions', 'Articles');
         $this->JsonTarget(".Section-ArticleList #Article_$ArticleID .Meta-Article", Tag($Article, 'Closed', 'Closed'), 'Prepend');
         $this->JsonTarget(".Section-ArticleList #Article_$ArticleID", 'Closed', 'AddClass');
      } else {
         $this->JsonTarget(".Section-ArticleList #Article_$ArticleID .Tag-Closed", NULL, 'Remove');
         $this->JsonTarget(".Section-ArticleList #Article_$ArticleID", 'Closed', 'RemoveClass');
      }
      
      $this->JsonTarget("#Article_$ArticleID", NULL, 'Highlight');
      $this->JsonTarget(".Article #Item_0", NULL, 'Highlight');
      
      $this->Render('Blank', 'Utility', 'Dashboard');
   }
   
   public function SendOptions($Article) {
      require_once $this->FetchViewLocation('helper_functions', 'Article');
      ob_start();
      WriteArticleOptions($Article);
      $Options = ob_get_clean();
      
      $this->JsonTarget("#Article_{$Article->ArticleID} .OptionsMenu,.Section-Article .Article .OptionsMenu", $Options, 'ReplaceWith');
   }
   
   protected function _SetOpenGraph() {
      if (!$this->Head)
         return;
      $this->Head->AddTag('meta', array('property' => 'og:type', 'content' => 'article'));
   }
}
