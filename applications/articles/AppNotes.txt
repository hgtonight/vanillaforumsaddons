Application
a) it could be made standalone and therefore a true alternative to WP bloat
b) blog comments are a completely different beast
c) you would need/want multiple controllers for the back/frontend
d) the extra backend work is very well suited to an application.

Comments
allow direct reply comments and have a simplified editor.
It would even make sense to allow comments without an account (pending moderation and/or captcha).