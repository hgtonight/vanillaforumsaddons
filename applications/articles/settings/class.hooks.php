<?php if (!defined('APPLICATION')) exit(); // Make sure this file can't get accessed directly
/**
 * A special function that is automatically run upon enabling your application.
 *
 * Remember to rename this to FooHooks, where 'Foo' is you app's short name.
 */
class ArticlesHooks implements Gdn_IPlugin {
   /**
    * Example hook. You should delete this.
    *
    * @param object $Sender The object that fired the event. All hooks must accept this single parameter.
    */
   //public function ControllerName_EventName_Handler($Sender) {
      // You can find existing hooks by searching for 'FireEvent'
      // Request new hooks on the VanillaForums.org community forum!
   //}

   /**
    * Adds "Mark All Viewed" to main menu.
    *
    * @since 1.0
    * @access public
    */
   public function Base_Render_Before($Sender) {
      // Add "Mark All Viewed" to main menu
      //if ($Sender->Menu && Gdn::Session()->IsValid()) {
      if ($Sender->Menu) {
         //if (C('Plugins.AllViewed.ShowInMenu', TRUE))
            $Sender->Menu->AddLink('Articles', T('Articles'), '/articles');
      }
   }
   
   public function Base_BeforeDiscussionFilters_Handler($Sender) {
      echo '<li class="Articles' . (strtolower($Sender->ControllerName) == 'articlescontroller' && strtolower($Sender->RequestMethod) == 'index' ? ' Active' : '') . '">' . Gdn_Theme::Link('articles', Sprite('SpArticles') . ' ' . T('Articles')) . '</li>';
   }
   
   public function Base_GetAppSettingsMenuItems_Handler($Sender) {
      $Menu = &$Sender->EventArguments['SideMenu'];
      $Menu->AddLink('Articles', T('Write Article'), 'articles/settings/write', 'Garden.Settings.Manage');
      $Menu->AddLink('Articles', T('All Articles'), 'articles/settings/all', 'Garden.Settings.Manage');
      $Menu->AddLink('Articles', T('Manage Categories'), 'articles/settings/managecategories', 'Garden.Settings.Manage');
      $Menu->AddLink('Articles', T('Advanced'), 'articles/settings/advanced', 'Garden.Settings.Manage');
   }
   
   /**
    * Adds 'Articles' and 'Article Comments' tabs to profiles and adds CSS & JS files to their head.
    * 
    * @since 2.0.0
    * @package Vanilla
    * 
    * @param object $Sender ProfileController.
    */ 
   public function ProfileController_AddProfileTabs_Handler($Sender) {
      if (is_object($Sender->User) && $Sender->User->UserID > 0) {
         $UserID = $Sender->User->UserID;
         // Add the 'Articles' and 'Article Comments' tabs
         $ArticlesLabel = Sprite('SpArticles').' '.T('Articles');
         $ArticleCommentsLabel = Sprite('SpArticleComments').' '.T('Article Comments');
         if (C('Articles.Profile.ShowCounts', TRUE)) {
            $ArticlesLabel .= '<span class="Aside">'.CountString(GetValueR('User.CountArticles', $Sender, NULL), "/profile/count/articles/articles?userid=$UserID").'</span>';
            $CommentsLabel .= '<span class="Aside">'.CountString(GetValueR('User.CountArticleComments', $Sender, NULL), "/profile/count/articles/comments?userid=$UserID").'</span>';
         }
         $Sender->AddProfileTab(T('Articles'), 'profile/articles/articles/'.$Sender->User->UserID.'/'.rawurlencode($Sender->User->Name), 'Articles', $ArticlesLabel);
         $Sender->AddProfileTab(T('Article Comments'), 'profile/articles/comments/'.$Sender->User->UserID.'/'.rawurlencode($Sender->User->Name), 'ArticleComments', $ArticleCommentsLabel);
         // Add the article tab's CSS and Javascript.
         $Sender->AddJsFile('jquery.gardenmorepager.js');
         //$Sender->AddJsFile('Articles.js');
      }
   }
   
   /**
	 * Creates virtual 'Articles' method in ProfileController.
	 * 
    * @since 2.0.0
    * @package Vanilla
    * @param mixed $UserReference Unique identifier, possible ID or username.
    * @param string $Username Username.
    * @param int $UserID Unique ID.
	 *
	 * @param ProfileController $Sender ProfileController.
	 */
   public function ProfileController_Articles_Create($Sender, $Stub = '', $UserReference = '', $Username = '', $UserID = '', $Page = '') {
		$Sender->EditMode(FALSE);

      // Tell the ProfileController what tab to load
		$Sender->GetUserInfo($UserReference, $Username, $UserID);
      
      if($Stub == 'articles') {
         $Sender->_SetBreadcrumbs(T('Articles'), UserUrl($Sender->User, '', 'articles/articles'));
         $Sender->SetTabView('Articles', 'Articles', 'Profile', 'Articles');
         // $Sender->CountCommentsPerPage = C('Articles.ArticleComments.PerPage', 30);
         
         // list($Offset, $Limit) = OffsetLimit($Page, Gdn::Config('Articles.Articles.PerPage', 30));
         
         // $ArticleModel = new ArticleModel();
         // $Articles = $ArticleModel->GetByUser($Sender->User->UserID, $Limit, $Offset);
         // $CountArticles = $Offset + $ArticleModel->LastArticleCount + 1;
         // $Sender->ArticleData = $Sender->SetData('Articles', $Articles);
         
         // Build a pager
         $PagerFactory = new Gdn_PagerFactory();
         $Sender->Pager = $PagerFactory->GetPager('MorePager', $Sender);
         $Sender->Pager->MoreCode = 'More Articles';
         $Sender->Pager->LessCode = 'Newer Articles';
         $Sender->Pager->ClientID = 'Pager';
         $Sender->Pager->Configure(
            $Offset,
            $Limit,
            $CountArticles,
            UserUrl($Sender->User, '', 'Articles').'?page={Page}'
         );
         
         // Deliver JSON data if necessary
         if ($Sender->DeliveryType() != DELIVERY_TYPE_ALL && $Offset > 0) {
            $Sender->SetJson('LessRow', $Sender->Pager->ToString('less'));
            $Sender->SetJson('MoreRow', $Sender->Pager->ToString('more'));
            $Sender->View = 'articles';
         }
      } elseif($Stub == 'comments') {
         $Sender->_SetBreadcrumbs(T('Article Comments'), UserUrl($Sender->User, '', 'articles/comments'));
         $Sender->SetTabView('Article Comments', 'ArticleComments', 'Profile', 'Articles');
         
         $PageSize = Gdn::Config('Articles.Comments.PerPage', 30);
         list($Offset, $Limit) = OffsetLimit($Page, $PageSize);
         
         // $CommentModel = new CommentModel();
         // $Comments = $CommentModel->GetByUser2($Sender->User->UserID, $Limit, $Offset, $Sender->Request->Get('lid'));
         // $TotalRecords = $Offset + $CommentModel->LastCommentCount + 1;
         $Comments = array(); // Placeholder statement since we have no ArticleComments model yet.
         
         // Build a pager
         $PagerFactory = new Gdn_PagerFactory();
         $Sender->Pager = $PagerFactory->GetPager('MorePager', $Sender);
         $Sender->Pager->MoreCode = 'More Article Comments';
         $Sender->Pager->LessCode = 'Newer Article Comments';
         $Sender->Pager->ClientID = 'Pager';
         $Sender->Pager->Configure(
            $Offset,
            $Limit,
            $TotalRecords,
            UserUrl($Sender->User, '', 'articlecomments').'?page={Page}' //?lid='.$CommentModel->LastCommentID
         );
         
         // Deliver JSON data if necessary
         if ($Sender->DeliveryType() != DELIVERY_TYPE_ALL && $Offset > 0) {
            $Sender->SetJson('LessRow', $Sender->Pager->ToString('less'));
            $Sender->SetJson('MoreRow', $Sender->Pager->ToString('more'));
            $Sender->View = 'articlecomments';
         }
         $Sender->SetData('ArticleComments', $Comments);
      }
      
      // Set the HandlerType back to normal on the profilecontroller so that it fetches it's own views
      $Sender->HandlerType = HANDLER_TYPE_NORMAL;
      
      // Do not show Article options
      $Sender->ShowOptions = FALSE;
      
      if ($Sender->Head) {
         // These pages offer only duplicate content to search engines and are a bit slow.
         $Sender->Head->AddTag('meta', array('name' => 'robots', 'content' => 'noindex,noarchive'));
      }
      
      if($Stub == 'articles' || $Stub == 'comments') {
         // Render the ProfileController
         $Sender->Render();
      } else {
         $Sender->Activity($UserReference, $Username, $UserID);
      }
   }
   
   /**
    * Special function automatically run upon clicking 'Enable' on your application.
    * Change the word 'skeleton' anywhere you see it.
    */
   public function Setup() {
      // You need to manually include structure.php here for it to get run at install.
      include(PATH_APPLICATIONS . DS . 'articles' . DS . 'settings' . DS . 'structure.php');

      // Stores a value in the config to indicate it has previously been installed.
      // You can use if(C('Skeleton.Setup', FALSE)) to test whether to repeat part of your setup.
      $ApplicationInfo = array();
      include(CombinePaths(array(PATH_APPLICATIONS . DS . 'articles' . DS . 'settings' . DS . 'about.php')));
      $Version = ArrayValue('Version', ArrayValue('Articles', $ApplicationInfo, array()), 'Undefined');
      SaveToConfig('Articles.Version', $Version);
   }
   
   /**
    * Special function automatically run upon clicking 'Disable' on your application.
    */
   public function OnDisable() {
      // Optional. Delete this if you don't need it.
   }
   
   /**
    * Special function automatically run upon clicking 'Remove' on your application.
    */
   public function CleanUp() {
      RemoveFromConfig('Articles.Version');
   }
}