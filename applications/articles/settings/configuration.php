<?php if (!defined('APPLICATION')) exit();
// DO NOT EDIT THIS FILE
// All of the settings defined here can be overridden in the /conf/config.php file.

$Configuration['Articles']['Articles']['PerPage'] = '30';
$Configuration['Articles']['Comments']['PerPage'] = '30';

$Configuration['Articles']['Comment']['MaxLength'] = '8000';
$Configuration['Articles']['Articles']['PerCategory'] = '5';
$Configuration['Articles']['Categories']['Use'] = TRUE;

$Configuration['Articles']['Article']['SummaryRequired'] = FALSE;

// The max depth of nested categories to display on all category view.
$Configuration['Articles']['Categories']['MaxDisplayDepth'] = 3;
// The max depth of nested categories to display in the category module (in the panel).
$Configuration['Articles']['Categories']['MaxModuleDisplayDepth'] = 0;
// Should the first level of category above root be a heading and unclickable?
$Configuration['Articles']['Categories']['DoHeadings'] = 0;

// Should users be automatically pushed to the last comment they read in a discussion?
$Configuration['Articles']['Comments']['AutoOffset'] = TRUE;
$Configuration['Articles']['Comment']['ReplaceNewlines'] = TRUE;

// ^^ That's for users of your application, not you (the developer).
// This file is where to place any default config settings for your application.

// Usually you'll want to name your config settings in 3 parts: app short name, section of the app, and setting name:
//$Configuration['Articles']['Section']['Setting'] = 'Value';

// You can access the above anywhere in your application like this: $DefaultValue = C('Skeleton.Section.Setting');