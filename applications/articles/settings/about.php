<?php
/**
 * An associative array of information about this application.
 *
 * Be sure to change the 'Skeleton' in the array's key to your app's short name.
 */
$ApplicationInfo['Articles'] = array(
   'Description' => "Articles is an application that provides a way to write and publish articles in Garden.",
   'Version' => '0.0.1',
   'License' => 'GPL v2',
   'Author' => "Livid Tech",
   'AuthorUrl' => 'http://lividtech.com',
   'Url' => 'http://lividtech.com',
   'RegisterPermissions' => FALSE,
   //'RequiredApplications' => array('Vanilla' => '2.2.3.3'),
   'SetupController' => 'setup',
   'SettingsUrl' => 'articles/settings/advanced'
);
