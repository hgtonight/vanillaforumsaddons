<?php if(!defined('APPLICATION')) exit(); // Make sure this file can't get accessed directly
// Use this file to do any database changes for your application.

if(!isset($Drop))
   $Drop = FALSE; // Safe default - Set to TRUE to drop the table if it already exists.
   
if(!isset($Explicit))
   $Explicit = TRUE; // Safe default - Set to TRUE to remove all other columns from table.

$Database = Gdn::Database();
$SQL = $Database->SQL(); // To run queries.
$Construct = $Database->Structure(); // To modify and add database tables.
$Validation = new Gdn_Validation(); // To validate permissions (if necessary).

// Add your tables or new columns under here (see example below).
   
/**
 * Column() has the following arguments:
 *
 * @param string $Name Name of the column to create.
 * @param string $Type Data type of the column. Length may be specified in parenthesis.
 *    If an array is provided, the type will be set as "enum" and the array's values will be assigned as the column's enum values.
 * @param string $NullOrDefault Default is FALSE. Whether or not nulls are allowed, if not a default can be specified.
 *    TRUE: Nulls allowed. FALSE: Nulls not allowed. Any other value will be used as the default (with nulls disallowed).
 * @param string $KeyType Default is FALSE. Type of key to make this column. Options are: primary, key, or FALSE (not a key).
 *
 * @see /library/database/class.generic.structure.php
 */

// Construct the Category table.
$Construct->Table('ArticleCategory');
$ArticleCategoryExists = $Construct->TableExists();
$PermissionCategoryIDExists = $Construct->ColumnExists('PermissionCategoryID');

$LastArticleIDExists = $Construct->ColumnExists('LastArticleID');

$Construct->PrimaryKey('CategoryID')
   ->Column('ParentCategoryID', 'int', TRUE)
   ->Column('TreeLeft', 'int', TRUE)
   ->Column('TreeRight', 'int', TRUE)
   ->Column('Depth', 'int', TRUE)
   ->Column('CountArticles', 'int', '0')
   ->Column('CountArticleComments', 'int', '0')
   ->Column('DateMarkedRead', 'datetime', NULL)
   ->Column('AllowArticles', 'tinyint', '1')
   ->Column('Archived', 'tinyint(1)', '0')
   ->Column('Name', 'varchar(255)')
   ->Column('UrlCode', 'varchar(255)', TRUE)
   ->Column('Description', 'varchar(500)', TRUE)
   ->Column('Sort', 'int', TRUE)
   ->Column('CssClass', 'varchar(50)', TRUE)
   ->Column('Photo', 'varchar(255)', TRUE)
   ->Column('PermissionCategoryID', 'int', '-1') // default to root.
   ->Column('PointsCategoryID', 'int', '0') // default to global.
   ->Column('HideAllArticles', 'tinyint(1)', '0')
   ->Column('DisplayAs', array('Categories', 'Articles', 'Default'), 'Default')
   ->Column('InsertUserID', 'int', FALSE, 'key')
   ->Column('UpdateUserID', 'int', TRUE)
   ->Column('DateInserted', 'datetime')
   ->Column('DateUpdated', 'datetime')
   ->Column('LastCommentID', 'int', NULL)
   ->Column('LastArticleID', 'int', NULL)
   ->Column('LastDateInserted', 'datetime', NULL)
   ->Set($Explicit, $Drop);

$RootCategoryInserted = FALSE;
if($ArticleCategoryExists && $SQL->GetWhere('ArticleCategory', array('CategoryID' => -1))->NumRows() == 0) {
   $SQL->Insert('ArticleCategory', array('CategoryID' => -1, 'TreeLeft' => 1, 'TreeRight' => 4, 'InsertUserID' => 1, 'UpdateUserID' => 1, 'DateInserted' => Gdn_Format::ToDateTime(), 'DateUpdated' => Gdn_Format::ToDateTime(), 'Name' => 'Root', 'UrlCode' => '', 'Description' => 'Root of category tree. Users should never see this.', 'PermissionCategoryID' => -1));
   $RootCategoryInserted = TRUE;
}

if($Drop || !$ArticleCategoryExists) {
   $SQL->Insert('ArticleCategory', array('ParentCategoryID' => -1, 'TreeLeft' => 2, 'TreeRight' => 3, 'InsertUserID' => 1, 'UpdateUserID' => 1, 'DateInserted' => Gdn_Format::ToDateTime(), 'DateUpdated' => Gdn_Format::ToDateTime(), 'Name' => 'Uncategorized', 'UrlCode' => 'uncategorized', 'Description' => 'Uncategorized articles.', 'PermissionCategoryID' => -1));
}

if($ArticleCategoryExists) {
   $ArticleCategoryModel = new ArticleCategoryModel();
   $ArticleCategoryModel->RebuildTree();
   unset($ArticleCategoryModel);
}

$Construct->Table('UserArticleCategory')
   ->Column('UserID', 'int', FALSE, 'primary')
   ->Column('CategoryID', 'int', FALSE, 'primary')
   ->Column('DateMarkedRead', 'datetime', NULL)
   ->Set($Explicit, $Drop);
 
// Construct the Article table.
$Construct->Table('Article');

$Construct
   ->PrimaryKey('ArticleID')
   ->Column('CategoryID', 'int', FALSE, array('key', 'index.CategoryPages'))
   ->Column('InsertUserID', 'int', FALSE, 'key')
   ->Column('UpdateUserID', 'int', TRUE)
   ->Column('FirstCommentID', 'int', TRUE)
   ->Column('LastCommentID', 'int', TRUE)
   ->Column('Name', 'varchar(100)', FALSE, 'fulltext')
   ->Column('UrlCode', 'varchar(255)', TRUE)
   ->Column('Body', 'text', FALSE, 'fulltext')
   ->Column('Summary', 'text', FALSE, 'fulltext')
   ->Column('Format', 'varchar(20)', TRUE)
   ->Column('Tags', 'text', NULL)
   ->Column('Photo', 'varchar(255)', TRUE)
   ->Column('CountArticleComments', 'int', '0')
   ->Column('CountArticleBookmarks', 'int', NULL)
   ->Column('CountViews', 'int', '1')
   ->Column('State', 'tinyint(1)', '0')
   ->Column('Closed', 'tinyint(1)', '0')
   ->Column('Announce', 'tinyint(1)', '0')
   ->Column('DateInserted', 'datetime', FALSE, 'index')
   ->Column('DateUpdated', 'datetime', TRUE)
   ->Column('InsertIPAddress', 'varchar(15)', TRUE)
   ->Column('UpdateIPAddress', 'varchar(15)', TRUE)
   ->Column('DateLastComment', 'datetime', NULL, array('index', 'index.CategoryPages'))
	->Column('LastCommentUserID', 'int', TRUE)
   ->Column('Attributes', 'text', TRUE)
   ->Set($Explicit, $Drop);

// Construct the UserArticle table.
// Allows the tracking of relationships between articles and users (bookmarks, dismissed announcements, # of read comments in a article, etc)
// Column($Name, $Type, $Length = '', $Null = FALSE, $Default = NULL, $KeyType = FALSE, $AutoIncrement = FALSE)
$Construct->Table('UserArticle')
   ->Column('UserID', 'int', FALSE, 'primary')
   ->Column('ArticleID', 'int', FALSE, array('primary', 'key'))
   ->Column('CountArticleComments', 'int', '0')
   ->Column('DateLastViewed', 'datetime', NULL) // null signals never
   ->Column('Dismissed', 'tinyint(1)', '0') // relates to dismissed announcements
   ->Column('Bookmarked', 'tinyint(1)', '0')
   ->Set($Explicit, $Drop);

// Construct the ArticleComment table.
$Construct->Table('ArticleComment');

if ($Construct->TableExists())
   $CommentIndexes = $Construct->IndexSqlDb();
else
   $CommentIndexes = array();

$Construct->PrimaryKey('CommentID')
	->Column('ArticleID', 'int', FALSE, 'index.1')
	->Column('InsertUserID', 'int', TRUE, 'key')
	->Column('UpdateUserID', 'int', TRUE)
	->Column('DeleteUserID', 'int', TRUE)
	->Column('Body', 'text', FALSE, 'fulltext')
	->Column('Format', 'varchar(20)', TRUE)
	->Column('DateInserted', 'datetime', NULL, array('index.1', 'index'))
	->Column('DateDeleted', 'datetime', TRUE)
	->Column('DateUpdated', 'datetime', TRUE)
   ->Column('InsertIPAddress', 'varchar(15)', TRUE)
   ->Column('UpdateIPAddress', 'varchar(15)', TRUE)
	->Column('Flag', 'tinyint', 0)
	->Column('Attributes', 'text', TRUE)
	->Set($Explicit, $Drop);

// Allows the tracking of already-read comments & votes on a per-user basis.
$Construct->Table('UserArticleComment')
   ->Column('UserID', 'int', FALSE, 'primary')
   ->Column('CommentID', 'int', FALSE, 'primary')
   ->Column('DateLastViewed', 'datetime', NULL) // null signals never
   ->Set($Explicit, $Drop);
   
// Add extra columns to user table for tracking articles & article comments
$Construct->Table('User')
   ->Column('CountArticles', 'int', NULL)
   ->Column('CountUnreadArticles', 'int', NULL)
   ->Column('CountArticleComments', 'int', NULL)
   ->Column('CountArticleDrafts', 'int', NULL)
   ->Column('CountArticleBookmarks', 'int', NULL)
   ->Set();

// Construct the TagArticle table
$Construct->Table('TagArticle');
$DateInsertedExists = $Construct->ColumnExists('DateInserted');

$Construct
   ->Column('TagID', 'int', FALSE, 'primary')
   ->Column('ArticleID', 'int', FALSE, 'primary')
   ->Column('CategoryID', 'int', FALSE, 'index')
   ->Column('DateInserted', 'datetime', !$DateInsertedExists)
   ->Engine('InnoDB')
   ->Set($Explicit, $Drop);

if (!$DateInsertedExists) {
   $SQL->Update('TagArticle ta')
      ->Join('Article a', 'ta.ArticleID = a.ArticleID')
      ->Set('ta.DateInserted', 'a.DateInserted', FALSE, FALSE)
      ->Put();
}

$Construct->Table('Tag')
   ->Column('CountArticles', 'int', 0)
   ->Set();

// Set up permissions
$PermissionModel = Gdn::PermissionModel();
$PermissionModel->Database = $Database;
$PermissionModel->SQL = $SQL;

// Define some global articles permissions.
$PermissionModel->Define(array(
	'Articles.Approval.Require',
   'Articles.Comments.Me' => 1,
	));
$PermissionModel->Undefine(array('Articles.Settings.Manage', 'Articles.Categories.Manage'));

// Define some permissions for the Articles.
$PermissionModel->Define(array(
	'Articles.Articles.View' => 1,
	'Articles.Articles.Add' => 1,
	'Articles.Articles.Edit' => 0,
	'Articles.Articles.Announce' => 0,
	'Articles.Articles.Close' => 0,
	'Articles.Articles.Delete' => 0,
	'Articles.Comments.Add' => 1,
	'Articles.Comments.Edit' => 0,
	'Articles.Comments.Delete' => 0),
	'tinyint',
	'Category',
	'PermissionCategoryID'
);

$PermissionModel->Undefine('Articles.Spam.Manage');

if ($RootCategoryInserted) {
   // Get the root category so we can assign permissions to it.
   $GeneralCategoryID = -1;
   
   // Set the initial guest permissions.
   $PermissionModel->Save(array(
      'Role' => 'Guest',
      'JunctionTable' => 'ArticleCategory',
      'JunctionColumn' => 'PermissionCategoryID',
      'JunctionID' => $GeneralCategoryID,
      'Articles.Articles.View' => 1
      ), TRUE);

   $PermissionModel->Save(array(
      'Role' => 'Confirm Email',
      'JunctionTable' => 'ArticleCategory',
      'JunctionColumn' => 'PermissionCategoryID',
      'JunctionID' => $GeneralCategoryID,
      'Articles.Articles.View' => 1
      ), TRUE);

   $PermissionModel->Save(array(
      'Role' => 'Applicant',
      'JunctionTable' => 'ArticleCategory',
      'JunctionColumn' => 'PermissionCategoryID',
      'JunctionID' => $GeneralCategoryID,
      'Articles.Articles.View' => 1
      ), TRUE);
   
   // Set the intial member permissions.
   $PermissionModel->Save(array(
      'Role' => 'Member',
      'JunctionTable' => 'ArticleCategory',
      'JunctionColumn' => 'PermissionCategoryID',
      'JunctionID' => $GeneralCategoryID,
      'Articles.Articles.Add' => 1,
      'Articles.Articles.View' => 1,
      'Articles.Comments.Add' => 1
      ), TRUE);
      
   $PermissionModel->Save(array(
      'Role' => 'Moderator',
      'JunctionTable' => 'ArticleCategory',
      'JunctionColumn' => 'PermissionCategoryID',
      'JunctionID' => $GeneralCategoryID,
      'Articles.Articles.Add' => 1,
      'Articles.Articles.Edit' => 1,
      'Articles.Articles.Announce' => 1,
      'Articles.Articles.Close' => 1,
      'Articles.Articles.Delete' => 1,
      'Articles.Articles.View' => 1,
      'Articles.Comments.Add' => 1,
      'Articles.Comments.Edit' => 1,
      'Articles.Comments.Delete' => 1
      ), TRUE);
      
   $PermissionModel->Save(array(
      'Role' => 'Administrator',
      'JunctionTable' => 'ArticleCategory',
      'JunctionColumn' => 'PermissionCategoryID',
      'JunctionID' => $GeneralCategoryID,
      'Articles.Articles.Add' => 1,
      'Articles.Articles.Edit' => 1,
      'Articles.Articles.Announce' => 1,
      'Articles.Articles.Close' => 1,
      'Articles.Articles.Delete' => 1,
      'Articles.Articles.View' => 1,
      'Articles.Comments.Add' => 1,
      'Articles.Comments.Edit' => 1,
      'Articles.Comments.Delete' => 1
      ), TRUE);
}

// Example: New table construction.
/* 
$Construct->Table('ExampleTable')
	->PrimaryKey('ExampleTableID')
   ->Column('ExampleUserID', 'int', TRUE)
   ->Column('Field1', 'varchar(50)')
   ->Set($Explicit, $Drop); // If you omit $Explicit and $Drop they default to false.
*/ 

// Example: Add column to existing table.
/* 
$Construct->Table('User')
   ->Column('NewColumnNeeded', 'varchar(255)', TRUE) // Always allow for NULLs unless it's truly required.
   ->Set(); 
*/  

