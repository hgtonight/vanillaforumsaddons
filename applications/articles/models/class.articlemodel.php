<?php if (!defined('APPLICATION')) exit();
/*
Copyright 2008, 2009 Articles Forums Inc.
This file is part of Garden.
Garden is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Garden is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Garden.  If not, see <http://www.gnu.org/licenses/>.
Contact Articles Forums Inc. at support [at] articlesforums [dot] com
*/
/**
 * Article Model
 *
 * @package Articles
 */
 
/**
 * Manages features.
 *
 * @since 2.0.0
 * @package Articles
 */
class ArticleModel extends ArticlesModel {
   /** @var array */
   protected static $_CategoryPermissions = NULL;
   
   /** @var bool */
   public $Watching = FALSE;
   
   /**
    * Class constructor. Defines the related database table name.
    * 
    * @since 2.0.0
    * @access public
    */
   public function __construct() {
      parent::__construct('Article');
   }
   
   /**
    * Identify current user's category permissions and set as local array.
    * 
    * @since 2.0.0
    * @access public
    * 
	 * @param bool $Escape Prepends category IDs with @
	 * @return array Protected local _CategoryPermissions
	 */
   public static function CategoryPermissions($Escape = FALSE) {
      if(is_null(self::$_CategoryPermissions)) {
         $Session = Gdn::Session();
         
         if((is_object($Session->User) && $Session->User->Admin)) {
            self::$_CategoryPermissions = TRUE;
			} elseif(C('Garden.Permissions.Disabled.Category')) {
				if($Session->CheckPermission('Articles.Articles.View'))
					self::$_CategoryPermissions = TRUE;
				else
					self::$_CategoryPermissions = array(); // no permission
         } else {
            $SQL = Gdn::SQL();
            
            $Categories = ArticleCategoryModel::Categories();
            $IDs = array();
            
            foreach ($Categories as $ID => $Category) {
               if ($Category['PermsArticlesView']) {
                  $IDs[] = $ID;
               }
            }

            // Check to see if the user has permission to all categories. This is for speed.
            $CategoryCount = count($Categories);
            
            if (count($IDs) == $CategoryCount)
               self::$_CategoryPermissions = TRUE;
            else {
               self::$_CategoryPermissions = array();
               foreach($IDs as $ID) {
                  self::$_CategoryPermissions[] = ($Escape ? '@' : '').$ID;
               }
            }
         }
      }
      
      return self::$_CategoryPermissions;
   }
   
	/**
    * Modifies article data before it is returned.
    *
    * Takes fixes inaccurate comment counts.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param object $Data SQL result.
    */
	public function AddArticleColumns($Data) {
		$Result = &$Data->Result();
		foreach($Result as &$Article) {
         $this->Calculate($Article);
		}
	}
   
   public function Calculate(&$Article) {
      // Fix up output
      $Article->Name = Gdn_Format::Text($Article->Name);
      $Article->Attributes = @unserialize($Article->Attributes);
      $Article->Url = $this->ArticleUrl($Article);
      $Article->Tags = $this->FormatTags($Article->Tags);
      
      // Join in the category.
      $Category = ArticleCategoryModel::Categories($Article->CategoryID);
      if(!$Category) $Category = FALSE;
      $Article->Category = $Category['Name'];
      $Article->CategoryUrlCode = $Category['UrlCode'];
      $Article->PermissionCategoryID = $Category['PermissionCategoryID'];

      // Add some legacy calculated columns.
      if(!property_exists($Article, 'FirstUserID')) {
         $Article->FirstUserID = $Article->InsertUserID;
         $Article->FirstDate = $Article->DateInserted;
         $Article->LastUserID = $Article->LastCommentUserID;
         $Article->LastDate = $Article->DateLastComment;
      }

      // Add the columns from UserArticle if they don't exist.
      if(!property_exists($Article, 'CountArticleCommentWatch')) {
         $Article->WatchUserID = NULL;
         $Article->DateLastViewed = NULL;
         $Article->Dismissed = 0;
         $Article->CountArticleCommentWatch = NULL;
      }

      if(!property_exists($Article, 'Read')) {
         $Article->Read = !(bool)$Article->CountUnreadArticleComments;
         if($Category && !is_null($Category['DateMarkedRead'])) {
            // If the category was marked explicitly read at some point, see if that applies here
            if ($Category['DateMarkedRead'] > $Article->DateLastComment)
               $Article->Read = TRUE;
            
            if ($Article->Read)
               $Article->CountUnreadArticleComments = 0;
         }
      }

      // Logic for incomplete comment count.
      if($Article->CountArticleCommentWatch == 0 && $DateLastViewed = GetValue('DateLastViewed', $Article)) {
         $Article->CountUnreadArticleComments = TRUE;
         if (Gdn_Format::ToTimestamp($DateLastViewed) >= Gdn_Format::ToTimestamp($Article->LastDate)) {
            $Article->CountArticleCommentWatch = $Article->CountArticleComments;
            $Article->CountUnreadArticleComments = 0;
         }
      }
      
      if($Article->CountUnreadArticleComments === NULL)
         $Article->CountUnreadArticleComments = 0;
      elseif($Article->CountUnreadArticleComments < 0)
         $Article->CountUnreadArticleComments = 0;

      $Article->CountArticleCommentWatch = is_numeric($Article->CountArticleCommentWatch) ? $Article->CountArticleCommentWatch : NULL;

      if($Article->LastUserID == NULL) {
         $Article->LastUserID = $Article->InsertUserID;
         $Article->LastDate = $Article->DateInserted;
      }
      
      $this->EventArguments['Article'] = $Article;
      $this->FireEvent('SetCalculatedFields');
   }
   
   /**
    * Removes undismissed announcements from the data.
    *
    * @since 2.0.0
    * @access public
    *
    * @param object $Data SQL result.
    */
   public function RemoveAnnouncements($Data) {
      $Result = &$Data->Result();
      $Unset = FALSE;
      
      foreach($Result as $Key => &$Article) {
         if (isset($this->_AnnouncementIDs)) {
            if (in_array($Article->ArticleID, $this->_AnnouncementIDs)) {
               unset($Result[$Key]);
               $Unset = TRUE;
            }
         } elseif ($Article->Announce && $Article->Dismissed == 0) {
            // Unset articles that are announced and not dismissed
            unset($Result[$Key]);
            $Unset = TRUE;
         }
      }
      if ($Unset) {
         // Make sure the articles are still in order for json encoding.
         $Result = array_values($Result);
      }
   }
   
   /**
    * Count how many articles match the given criteria.
    * 
    * @since 2.0.0
    * @access public
    * 
	 * @param array $Wheres SQL conditions.
	 * @param bool $ForceNoAnnouncements Not used.
	 * @return int Number of articles.
	 */
   public function GetCount($Wheres = '', $ForceNoAnnouncements = FALSE) {
      if (is_array($Wheres) && count($Wheres) == 0)
         $Wheres = '';
      
      // Check permission and limit to categories as necessary
      if ($this->Watching)
         $Perms = ArticleCategoryModel::CategoryWatch();
      else
         $Perms = self::CategoryPermissions();
      
      if (!$Wheres || (count($Wheres) == 1 && isset($Wheres['a.CategoryID']))) {
         // Grab the counts from the faster category cache.
         if (isset($Wheres['a.CategoryID'])) {
            $CategoryIDs = (array)$Wheres['a.CategoryID'];
            if ($Perms === FALSE)
               $CategoryIDs = array();
            elseif (is_array($Perms))
               $CategoryIDs = array_intersect($CategoryIDs, $Perms);
            
            if (count($CategoryIDs) == 0) {
               return 0;
            } else {
               $Perms = $CategoryIDs;
            }
         }
         
         $Categories = ArticleCategoryModel::Categories();
         $Count = 0;
         
         foreach ($Categories as $Cat) {
            if (is_array($Perms) && !in_array($Cat['CategoryID'], $Perms))
               continue;
            $Count += (int)$Cat['CountArticles'];
         }
         return $Count;
      }
      
      if ($Perms !== TRUE) {
         $this->SQL->WhereIn('c.CategoryID', $Perms);
      }
      
      $this->EventArguments['Wheres'] = &$Wheres;
		$this->FireEvent('BeforeGetCount'); // @see 'BeforeGet' for consistency in count vs. results
         
      $this->SQL
         ->Select('a.ArticleID', 'count', 'CountArticles')
         ->From('Article a')
         ->Join('ArticleCategory c', 'a.CategoryID = c.CategoryID')
         ->Join('UserArticle w', 'a.ArticleID = w.ArticleID and w.UserID = '.Gdn::Session()->UserID, 'left')
         ->Where($Wheres);
      
      $Result = $this->SQL
         ->Get()
         ->FirstRow()
         ->CountArticles;
      
      return $Result;
   }
   
   /**
    * Builds base SQL query for article data.
    * 
    * Events: AfterArticleSummaryQuery.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param array $AdditionalFields Allows selection of additional fields as Alias=>Table.Fieldname.
    */
   public function ArticleSummaryQuery($AdditionalFields = array(), $Join = TRUE) {
      // Verify permissions (restricting by category if necessary)
      if ($this->Watching)
         $Perms = ArticleCategoryModel::CategoryWatch();
      else
         $Perms = self::CategoryPermissions();
      
      if($Perms !== TRUE) {
         $this->SQL->WhereIn('a.CategoryID', $Perms);
      }
      
      // Buid main query
      $this->SQL
         ->Select('a.*')
         ->Select('a.InsertUserID', '', 'FirstUserID')
         ->Select('a.DateInserted', '', 'FirstDate')
         ->Select('a.DateLastComment', '', 'LastDate')
         ->Select('a.LastCommentUserID', '', 'LastUserID')
         ->From('Article a');
      
      if ($Join) {
         $this->SQL
            ->Select('iu.Name', '', 'FirstName') // <-- Need these for rss!
            ->Select('iu.Photo', '', 'FirstPhoto')
            ->Select('iu.Email', '', 'FirstEmail')
            ->Join('User iu', 'a.InsertUserID = iu.UserID', 'left') // First comment author is also the article insertuserid
         
            ->Select('lcu.Name', '', 'LastName')
            ->Select('lcu.Photo', '', 'LastPhoto')
            ->Select('lcu.Email', '', 'LastEmail')
            ->Join('User lcu', 'a.LastCommentUserID = lcu.UserID', 'left') // Last comment user
         
            ->Select('ca.Name', '', 'Category')
            ->Select('ca.UrlCode', '', 'CategoryUrlCode')
            ->Select('ca.PermissionCategoryID')
            ->Join('Category ca', 'a.CategoryID = ca.CategoryID', 'left'); // Category
         
      }
		
		// Add any additional fields that were requested	
		if(is_array($AdditionalFields)) {
			foreach($AdditionalFields as $Alias => $Field) {
				// See if a new table needs to be joined to the query.
				$TableAlias = explode('.', $Field);
				$TableAlias = $TableAlias[0];
				if(array_key_exists($TableAlias, $Tables)) {
					$Join = $Tables[$TableAlias];
					$this->SQL->Join($Join[0], $Join[1]);
					unset($Tables[$TableAlias]);
				}
				
				// Select the field.
				$this->SQL->Select($Field, '', is_numeric($Alias) ? '' : $Alias);
			}
		}
         
      $this->FireEvent('AfterArticleSummaryQuery');
   }
   
   /**
    * Gets the data for multiple articles based on the given criteria.
    * 
    * Sorts results based on config options Articles.Articles.SortField
    * and Articles.Articles.SortDirection.
    * Events: BeforeGet, AfterAddColumns.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param int $Offset Number of articles to skip.
    * @param int $Limit Max number of articles to return.
    * @param array $Wheres SQL conditions.
    * @param array $AdditionalFields Allows selection of additional fields as Alias=>Table.Fieldname.
    * @return Gdn_DataSet SQL result.
    */
   public function Get($Offset = '0', $Limit = '', $Wheres = '', $AdditionalFields = NULL) {
      if ($Limit == '') 
         $Limit = Gdn::Config('Articles.Articles.PerPage', 50);

      $Offset = !is_numeric($Offset) || $Offset < 0 ? 0 : $Offset;
      
      $Session = Gdn::Session();
      $UserID = $Session->UserID > 0 ? $Session->UserID : 0;
      $this->ArticleSummaryQuery($AdditionalFields, FALSE);
         
      if ($UserID > 0) {
         $this->SQL
            ->Select('w.UserID', '', 'WatchUserID')
            ->Select('w.DateLastViewed, w.Dismissed, w.Bookmarked')
            ->Select('w.CountArticleComments', '', 'CountArticleCommentWatch')
            ->Join('UserArticle w', 'a.ArticleID = w.ArticleID and w.UserID = '.$UserID, 'left');
      } else {
			$this->SQL
				->Select('0', '', 'WatchUserID')
				->Select('now()', '', 'DateLastViewed')
				->Select('0', '', 'Dismissed')
				->Select('0', '', 'Bookmarked')
				->Select('0', '', 'CountArticleCommentWatch')
				->Select('a.Announce','','IsAnnounce');
      }
      
      if ($Offset !== FALSE && $Limit !== FALSE)
         $this->SQL->Limit($Limit, $Offset);
      
      // Get preferred sort order
      $SortField = 'a.DateInserted';

      $this->EventArguments['SortField'] = &$SortField; 
      $this->EventArguments['SortDirection'] = 'desc';
		$this->EventArguments['Wheres'] = &$Wheres;
		$this->FireEvent('BeforeGet'); // @see 'BeforeGetCount' for consistency in results vs. counts
      
      $IncludeAnnouncements = FALSE;
      if (strtolower(GetValue('Announce', $Wheres)) == 'all') {
         $IncludeAnnouncements = TRUE;
         unset($Wheres['Announce']);
      }

      if (is_array($Wheres))
         $this->SQL->Where($Wheres);
		
		$SortDirection = $this->EventArguments['SortDirection'];
		$SortDirection = 'desc';
		
		$this->SQL->OrderBy($SortField, $SortDirection);
      
      // Set range and fetch
      $Data = $this->SQL->Get();
         
      // If not looking at articles filtered by bookmarks or user, filter announcements out.
      if (!$IncludeAnnouncements) {
         if (!isset($Wheres['w.Bookmarked']) && !isset($Wheres['a.InsertUserID']))
            $this->RemoveAnnouncements($Data);
      }
      
      // Join in the users.
      Gdn::UserModel()->JoinUsers($Data, array('FirstUserID', 'LastUserID'));
      ArticleCategoryModel::JoinCategories($Data);
      
      // Change articles returned based on additional criteria	
		$this->AddArticleColumns($Data);
      
		// Prep and fire event
		$this->EventArguments['Data'] = $Data;
		$this->FireEvent('AfterAddColumns');
		
		return $Data;
   }
   
   public function GetWhere($Where = array(), $Offset = 0, $Limit = FALSE) {
      if (!$Limit)
         $Limit = C('Articles.Articles.PerPage', 30);
      
      if (!is_array($Where))
         $Where = array();
      
      $Sql = $this->SQL;

      // Determine category watching
      if ($this->Watching && !isset($Where['a.CategoryID'])) {
         $Watch = ArticleCategoryModel::CategoryWatch();
         if ($Watch !== TRUE)
            $Where['a.CategoryID'] = $Watch;
      }
      
      $this->EventArguments['SortField'] = 'a.DateInserted'; 
      $this->EventArguments['SortDirection'] = 'desc';
      $this->EventArguments['Wheres'] = &$Where;
      $this->FireEvent('BeforeGet');
      
      $SortField = $this->EventArguments['SortField'];
      $SortDirection = $this->EventArguments['SortDirection'];
      
      // Build up the base query. Self-join for optimization.
      $Sql->Select('a2.*')
         ->From('Article a')
         ->Join('Article a2', 'a.ArticleID = a2.ArticleID')
         ->OrderBy($SortField, $SortDirection)
         ->Limit($Limit, $Offset);
      
      // Verify permissions (restricting by category if necessary)
      $Perms = self::CategoryPermissions();
      
      if($Perms !== TRUE) {
         if(isset($Where['a.CategoryID'])) {
            $Where['a.CategoryID'] = array_values(array_intersect((array)$Where['a.CategoryID'], $Perms));
         } else {
            $Where['a.CategoryID'] = $Perms;
         }
      }
      
      // Check to see whether or not we are removing announcements.
      if(strtolower(GetValue('Announce', $Where)) ==  'all') {
         $RemoveAnnouncements = FALSE;
         unset($Where['Announce']);
      } elseif(strtolower(GetValue('a.Announce', $Where)) ==  'all') {
         $RemoveAnnouncements = FALSE;
         unset($Where['a.Announce']);
      } else {
         $RemoveAnnouncements = TRUE;
      }
      
      // Make sure there aren't any ambiguous article references.
      foreach($Where as $Key => $Value) {
         if (strpos($Key, '.') === FALSE) {
            $Where['a.'.$Key] = $Value;
            unset($Where[$Key]);
         }
      }
      
      $Sql->Where($Where);
      
      // Add the UserArticle query.
      if(($UserID = Gdn::Session()->UserID) > 0) {
         $Sql
            ->Join('UserArticle w', "w.ArticleID = a2.ArticleID and w.UserID = $UserID", 'left')
            ->Select('w.UserID', '', 'WatchUserID')
            ->Select('w.DateLastViewed, w.Dismissed, w.Bookmarked')
            ->Select('w.CountArticleComments', '', 'CountArticleCommentWatch');
      }
      
      $Data = $Sql->Get();
      $Result = &$Data->Result();
      
      // Change articles returned based on additional criteria	
		$this->AddArticleColumns($Data);
      
      // If not looking at articles filtered by bookmarks or user, filter announcements out.
      if ($RemoveAnnouncements && !isset($Where['w.Bookmarked']) && !isset($Wheres['a.InsertUserID']))
         $this->RemoveAnnouncements($Data);
      
      // Join in the users.
      Gdn::UserModel()->JoinUsers($Data, array('FirstUserID', 'LastUserID'));
      ArticleCategoryModel::JoinCategories($Data);
      
      // Prep and fire event
		$this->EventArguments['Data'] = $Data;
		$this->FireEvent('AfterAddColumns');
      
      return $Data;
   }
   
   /**
    * Gets announced articles.
    * 
    * @since 2.0.0
    * @access public
    * 
	 * @param array $Wheres SQL conditions.
	 * @return object SQL result.
	 */
   public function GetAnnouncements($Wheres = '') {
      $Session = Gdn::Session();
      $Limit = Gdn::Config('Articles.Articles.PerPage', 50);
      $Offset = 0;
      $UserID = $Session->UserID > 0 ? $Session->UserID : 0;

      // Get the article IDs of the announcements.
      $CacheKey = 'Announcements';
      
      $AnnouncementIDs = $this->SQL
         ->Cache($CacheKey)
         ->Select('a.ArticleID')
         ->From('Article a')
         ->Where('a.Announce >', '0')->Get()->ResultArray();

      $AnnouncementIDs = ConsolidateArrayValuesByKey($AnnouncementIDs, 'ArticleID');

      // Short circuit querying when there are no announcements.
      if (count($AnnouncementIDs) == 0)
         return new Gdn_DataSet();

      $this->ArticleSummaryQuery(array(), FALSE);
      
      if (!empty($Wheres))
         $this->SQL->Where($Wheres);

      if ($UserID) {
         $this->SQL->Select('w.UserID', '', 'WatchUserID')
         ->Select('w.DateLastViewed, w.Dismissed, w.Bookmarked')
         ->Select('w.CountArticleComments', '', 'CountArticleCommentWatch')
         ->Join('UserArticle w', 'a.ArticleID = w.ArticleID and w.UserID = '.$UserID, 'left');
      } else {
         // Don't join in the user table when we are a guest.
         $this->SQL->Select('null as WatchUserID, null as DateLastViewed, null as Dismissed, null as Bookmarked, null as CountArticleCommentWatch');
      }
      
      $this->SQL->WhereIn('a.ArticleID', $AnnouncementIDs);
      
      // If we aren't viewing announcements in a category then only show global announcements.
      if (!$Wheres) {
         $this->SQL->Where('a.Announce', 1);
      } else {
         $this->SQL->Where('a.Announce >', 0);
      }

      // If we allow users to dismiss articles, skip ones this user dismissed
      if (C('Articles.Articles.Dismiss', 1) && $UserID) {
         $this->SQL
            ->Where('coalesce(w.Dismissed, \'0\')', '0', FALSE);
      }

      $this->SQL
         ->OrderBy('a.DateInserted', 'desc')
         ->Limit($Limit, $Offset);

      $Data = $this->SQL->Get();
      
      // Save the announcements that were fetched for later removal.
      $AnnouncementIDs = array();
      foreach ($Data as $Row) {
         $AnnouncementIDs[] = GetValue('ArticleID', $Row);
      }
      $this->_AnnouncementIDs = $AnnouncementIDs;
			
		$this->AddArticleColumns($Data);
      
      Gdn::UserModel()->JoinUsers($Data, array('FirstUserID', 'LastUserID'));
      ArticleCategoryModel::JoinCategories($Data);
      
		// Prep and fire event
		$this->EventArguments['Data'] = $Data;
		$this->FireEvent('AfterAddColumns');

		return $Data;
   }
   
   /**
    * Return a url for an article.
    * @param object $Article
    * @return string
    */
   public function ArticleUrl($Article, $Page = '', $WithDomain = TRUE) {
      $Article = (object)$Article;
      $ArticleYear = date('Y', strtotime($Article->DateInserted));
      $Result = '/article/'.$ArticleYear.'/'.$Article->UrlCode;
      if ($Page) {
         if ($Page > 1 || Gdn::Session()->UserID)
            $Result .= '/p'.$Page;
      }
      return Url($Result, $WithDomain);
   }
   
   /**
    * Updates the CountArticles value on the category based on the CategoryID
    * being saved.
    *
    * @since 2.0.0
    * @access public
    *
    * @param int $CategoryID Unique ID of category we are updating.
    */
   public function UpdateArticleCount($CategoryID, $Article = FALSE) {
      $ArticleID = GetValue('ArticleID', $Article, FALSE);
		if (strcasecmp($CategoryID, 'All') == 0) {
			$Params = array();
			$Where = '';
			
			// Update all categories.
			$Sql = "update :_ArticleCategory c
            left join (
              select
                a.CategoryID,
                coalesce(count(a.ArticleID), 0) as CountArticles,
                coalesce(sum(a.CountArticleComments), 0) as CountArticleComments
              from :_Article a
              $Where
              group by a.CategoryID
            ) a
              on c.CategoryID = a.CategoryID
            set 
               c.CountArticles = coalesce(a.CountArticles, 0),
               c.CountArticleComments = coalesce(a.CountArticleComments, 0)";
			$Sql = str_replace(':_', $this->Database->DatabasePrefix, $Sql);
			$this->Database->Query($Sql, $Params, 'ArticleModel_UpdateArticleCount');
			
		} elseif (is_numeric($CategoryID)) {
         $this->SQL
            ->Select('a.ArticleID', 'count', 'CountArticles')
            ->Select('a.CountArticleComments', 'sum', 'CountArticleComments')
            ->From('Article a')
            ->Where('a.CategoryID', $CategoryID);
			
			$Data = $this->SQL->Get()->FirstRow();
         $CountArticles = (int)GetValue('CountArticles', $Data, 0);
         $CountArticleComments = (int)GetValue('CountArticleComments', $Data, 0);
         
         $CacheAmendment = array(
            'CountArticles'      => $CountArticles,
            'CountArticleComments'         => $CountArticleComments
         );
         
         if ($ArticleID) {
            $CacheAmendment = array_merge($CacheAmendment, array(
               'LastArticleID'   => $ArticleID,
               'LastCommentID'      => NULL,
               'LastDateInserted'   => GetValue('DateInserted', $Article)
            ));
         }
         
         $ArticleCategoryModel = new ArticleCategoryModel();
         $ArticleCategoryModel->SetField($CategoryID, $CacheAmendment);
         $ArticleCategoryModel->SetRecentPost($CategoryID);
      }
   }
   
   public function UpdateUserArticleCount($UserID) {
      $CountArticles = $this->SQL
         ->Select('ArticleID', 'count', 'CountArticles')
         ->From('Article')
         ->Where('InsertUserID', $UserID)
         ->Get()->Value('CountArticles', 0);
      
      // Save the count to the user table
      Gdn::UserModel()->SetField($UserID, 'CountArticles', $CountArticles);
   }

   /**
    *
    * @param type $Article
    * @param type $NotifiedUsers
    * @param ActivityModel $ActivityModel 
    */
   public function NotifyNewArticle($Article, $ActivityModel, $Activity) {
      if (is_numeric($Article)) {
         $Article = $this->GetID($Article);
      }
      
      $CategoryID = GetValue('CategoryID', $Article);
      
      // Figure out the category that governs this notification preference.
      $i = 0;
      $Category = ArticleCategoryModel::Categories($CategoryID);
      if (!$Category)
         return;
      
      while($Category['Depth'] > 2 && $i < 20) {
         if(!$Category || $Category['Archived'])
            return;
         $i++;
         $Category = ArticleCategoryModel::Categories($Category['ParentCategoryID']);
      } 


      // Grab all of the users that need to be notified.
      $Data = $this->SQL
         ->WhereIn('Name', array('Preferences.Email.NewArticle.'.$Category['CategoryID'], 'Preferences.Popup.NewArticle.'.$Category['CategoryID']))
         ->Get('UserMeta')->ResultArray();
      
      $NotifyUsers = array();
      foreach ($Data as $Row) {
         if (!$Row['Value'])
            continue;
         
         $UserID = $Row['UserID'];
         $Name = $Row['Name'];
         if (strpos($Name, '.Email.') !== FALSE) {
            $NotifyUsers[$UserID]['Emailed'] = ActivityModel::SENT_PENDING;
         } elseif (strpos($Name, '.Popup.') !== FALSE) {
            $NotifyUsers[$UserID]['Notified'] = ActivityModel::SENT_PENDING;
         }
      }
      
      $InsertUserID = GetValue('InsertUserID', $Article);
      foreach ($NotifyUsers as $UserID => $Prefs) {
         if ($UserID == $InsertUserID)
            continue;
         
         $Activity['NotifyUserID'] = $UserID;
         $Activity['Emailed'] = GetValue('Emailed', $Prefs, FALSE);
         $Activity['Notified'] = GetValue('Notified', $Prefs, FALSE);
         $ActivityModel->Queue($Activity);
      }
   }

   /**
	 * Convert tags from stored format to user-presentable format.
	 *
    * @since 2.1
    * @access protected
    *
    * @param string Serialized array.
    * @return string Comma-separated tags.
    */
   protected function FormatTags($Tags) {
      // Don't bother if there aren't any tags
      if (!$Tags)
         return '';
      
      // Get the array
      $TagsArray = Gdn_Format::Unserialize($Tags);     
      
      // Compensate for deprecated space-separated format 
      if (is_string($TagsArray) && $TagsArray == $Tags)
         $TagsArray = explode(' ', $Tags);
      
      // Safe format
      $TagsArray = Gdn_Format::Text($TagsArray);
      
      // Send back an comma-separated string
      return implode(',', $TagsArray);
   }
   
   public function CheckValidationResults($ValidationResults) {
      // If the summary is not required, remove it's validation errors.
      $SummaryRequired = C('Article.Article.SummaryRequired', FALSE);
      if (!$SummaryRequired && array_key_exists('Summary', $ValidationResults))
         unset($ValidationResults['Summary']);
         
      return $ValidationResults;
   }
   
   /**
    * Inserts or updates the article via form values.
    * 
    * Events: BeforeSaveArticle, AfterSaveArticle.
    *
    * @since 2.0.0
    * @access public
    * 
    * @param array $FormPostValues Data sent from the form model.
    * @return int $ArticleID Unique ID of the article.
    */
   public function Save($FormPostValues) {
      $Session = Gdn::Session();
      
      // Define the primary key in this model's table.
      $this->DefineSchema();
      
      // Add & apply any extra validation rules:
      $this->Validation->ApplyRule('UrlCode', 'UrlStringRelaxed');
      $this->Validation->ApplyRule('Body', 'Required');
      $this->Validation->ApplyRule('Summary', 'Required');
      $this->Validation->AddRule('MeAction', 'function:ValidateMeAction');
      $this->Validation->ApplyRule('Body', 'MeAction');
      $MaxCommentLength = Gdn::Config('Article.Comment.MaxLength');
      if (is_numeric($MaxCommentLength) && $MaxCommentLength > 0) {
         $this->Validation->SetSchemaProperty('Body', 'Length', $MaxCommentLength);
         $this->Validation->ApplyRule('Body', 'Length');
      }
      
      // Validate category permissions.
      $CategoryID = GetValue('CategoryID', $FormPostValues);
      if ($CategoryID > 0) {
         $Category = ArticleCategoryModel::Categories($CategoryID);
         if ($Category && !$Session->CheckPermission('Article.Articles.Add', TRUE, 'ArticleCategory', GetValue('PermissionCategoryID', $Category)))
            $this->Validation->AddValidationResult('CategoryID', 'You do not have permission to post in this category');
      }
      
      // Make sure that the UrlCode is unique among articles.
      $UrlCode = GetValue('UrlCode', $FormPostValues);
      if($UrlCode == '')
         $UrlCode = htmlspecialchars(GetValue('Name', $FormPostValues));
      $UrlCode = Gdn_Format::Url($UrlCode);
      $this->SQL->Select('ArticleID')
         ->From('Article')
         ->Where('UrlCode', $UrlCode);
      $InvalidUrlCode = $this->SQL->Get()->NumRows();
      
      // Get the ArticleID from the form so we know if we are inserting or updating.
      $ArticleID = ArrayValue('ArticleID', $FormPostValues, '');
      if($ArticleID)
         $Editing = TRUE;
      
      // Check if editing and if slug is same as one currently set in ArticleID.
      $ValidArticleID = $this->SQL
         ->Select('a.UrlCode')
         ->From('Article a')
         ->Where('a.ArticleID', $ArticleID)
         ->Get()->FirstRow();
      if($InvalidUrlCode && ($ValidArticleID->UrlCode != $UrlCode))
         $this->Validation->AddValidationResult('UrlCode', 'The specified url code is already in use by another article.');
      
      $Insert = $ArticleID == '' ? TRUE : FALSE;
		$this->EventArguments['Insert'] = $Insert;
      
      if ($Insert) {
         unset($FormPostValues['ArticleID']);
         // If no categoryid is defined, grab the first available.
         if (!GetValue('CategoryID', $FormPostValues) && !C('Articles.Categories.Use')) {
            $FormPostValues['CategoryID'] = GetValue('CategoryID', ArticleCategoryModel::DefaultCategory(), -1);
         }
            
         $this->AddInsertFields($FormPostValues);
         
         // The UpdateUserID used to be required. Just add it if it still is.
         if (!$this->Schema->GetProperty('UpdateUserID', 'AllowNull', TRUE)) {
            $FormPostValues['UpdateUserID'] = $FormPostValues['InsertUserID'];
         }
         
         // $FormPostValues['LastCommentUserID'] = $Session->UserID;
         $FormPostValues['DateLastComment'] = $FormPostValues['DateInserted'];
      } else {
         // Add the update fields.
         $this->AddUpdateFields($FormPostValues);
      }
      
      // Set checkbox values to zero if they were unchecked
      if (ArrayValue('Announce', $FormPostValues, '') === FALSE)
         $FormPostValues['Announce'] = 0;

      if (ArrayValue('Closed', $FormPostValues, '') === FALSE)
         $FormPostValues['Closed'] = 0;

		//	Prep and fire event
		$this->EventArguments['FormPostValues'] = &$FormPostValues;
		$this->EventArguments['ArticleID'] = $ArticleID;
		$this->FireEvent('BeforeSaveArticle');
      
      // Validate the form posted values
      $this->Validate($FormPostValues, $Insert);
      $ValidationResults = $this->CheckValidationResults($this->ValidationResults());
      
      if (count($ValidationResults) == 0) {
         // If the post is new and it validates
         if ($Insert || $Editing) {
            // Get all fields on the form that relate to the schema
            $Fields = $this->Validation->SchemaValidationFields(); 
            
            // Get ArticleID if one was sent
            $ArticleID = intval(ArrayValue('ArticleID', $Fields, 0));
            
            // Get UrlCode if one was sent
            $Fields['UrlCode'] = ArrayValue('UrlCode', $Fields, 0);
            if($Fields['UrlCode'] == '') {
               $Fields['UrlCode'] = htmlspecialchars($Fields['Name']);
               $Fields['UrlCode'] = Gdn_Format::Url($Fields['UrlCode']);
            }

            // Remove the primary key from the fields for saving
            $Fields = RemoveKeyFromArray($Fields, 'ArticleID');
            
            $StoredCategoryID = FALSE;
            
            if ($ArticleID > 0) {
               // Updating
               $Stored = $this->GetID($ArticleID, DATASET_TYPE_ARRAY);
               
               // Clear the cache if necessary.
               if (GetValue('Announce', $Stored) != GetValue('Announce', $Fields)) {
                  $CacheKeys = array('Announcements');
                  $this->SQL->Cache($CacheKeys);
               }

               self::SerializeRow($Fields);
               $this->SQL->Put($this->Name, $Fields, array($this->PrimaryKey => $ArticleID));

               SetValue('ArticleID', $Fields, $ArticleID);
               LogModel::LogChange('Edit', 'Article', (array)$Fields, $Stored);
               
               if (GetValue('CategoryID', $Stored) != GetValue('CategoryID', $Fields)) 
                  $StoredCategoryID = GetValue('CategoryID', $Stored);
            } else {
               // Inserting.
               if (!GetValue('Format', $Fields) || C('Garden.ForceInputFormatter'))
                  $Fields['Format'] = C('Garden.InputFormatter', '');
					
               // Create article
               $ArticleID = $this->SQL->Insert($this->Name, $Fields);
               $Fields['ArticleID'] = $ArticleID;
               
               // Update the cache.
               if ($ArticleID && Gdn::Cache()->ActiveEnabled()) {
                  $CategoryCache = array(
                     'LastArticleID' => $ArticleID,
                     'LastCommentID' => NULL,
                     'LastTitle' => Gdn_Format::Text($Fields['Name']), // kluge so JoinUsers doesn't wipe this out.
                     'LastUserID' => $Fields['InsertUserID'],
                     'LastDateInserted' => $Fields['DateInserted'],
                     'LastUrl' => $this->ArticleUrl($Fields)
                  );
                  ArticleCategoryModel::SetCache($Fields['CategoryID'], $CategoryCache);
                  
                  // Clear the cache if necessary.
                  if (GetValue('Announce', $Fields)) {
                     Gdn::Cache()->Remove('Announcements');
                  }
               }
               
               // Update the user's article count.
               $this->UpdateUserArticleCount(Gdn::Session()->UserID);
               
               // Assign the new ArticleID to the comment before saving.
               $FormPostValues['IsNewArticle'] = TRUE;
               $FormPostValues['ArticleID'] = $ArticleID;
               
               // Do data prep.
					$ArticleName = ArrayValue('Name', $Fields, '');
               $Story = ArrayValue('Body', $Fields, '');
               $NotifiedUsers = array();
               
               $UserModel = Gdn::UserModel();
               $ActivityModel = new ActivityModel();
               
               if (GetValue('Type', $FormPostValues))
                  $Code = 'HeadlineFormat.Article.'.$FormPostValues['Type'];
               else
                  $Code = 'HeadlineFormat.Article';
               
               $HeadlineFormat = T($Code, '{ActivityUserID,user} started a new article: <a href="{Url,html}">{Data.Name,text}</a>');
               $Category = ArticleCategoryModel::Categories(GetValue('CategoryID', $Fields));
               $Activity = array(
                  'ActivityType' => 'Article',
                  'ActivityUserID' => $Fields['InsertUserID'],
                  'HeadlineFormat' => $HeadlineFormat,
                  'RecordType' => 'Article',
                  'RecordID' => $ArticleID,
                  'Route' => $this->ArticleUrl($Fields),
                  'Data' => array(
                     'Name' => $ArticleName,
                     'Category' => GetValue('Name', $Category)
                  )
               );
               
               // Allow simple fulltext notifications
               if (C('Articles.Activity.ShowArticleBody', FALSE))
                  $Activity['Story'] = $Story;
               
               // Notify all of the users that were mentioned in the article.
               $Usernames = array_merge(GetMentions($ArticleName), GetMentions($Story));
               $Usernames = array_unique($Usernames);
               
               // Use our generic Activity for events, not mentions
               $this->EventArguments['Activity'] = $Activity;
               
               // Notifications for mentions
               foreach ($Usernames as $Username) {
                  $User = $UserModel->GetByUsername($Username);
                  if (!$User)
                     continue;
                  
                  // Check user can still see the article.
                  if (!$UserModel->GetCategoryViewPermission($User->UserID, GetValue('CategoryID', $Fields)))
                     continue;
                  
                  $Activity['HeadlineFormat'] = T('HeadlineFormat.Mention', '{ActivityUserID,user} mentioned you in <a href="{Url,html}">{Data.Name,text}</a>');
                  
                  $Activity['NotifyUserID'] = GetValue('UserID', $User);
                  $ActivityModel->Queue($Activity, 'Mention');
               }
                              
               // Notify everyone that has advanced notifications.
               try {
                  $Fields['ArticleID'] = $ArticleID;
                  $this->NotifyNewArticle($Fields, $ActivityModel, $Activity);
               } catch(Exception $Ex) {
                  throw $Ex;
               }
               
               // Throw an event for users to add their own events.
               $this->EventArguments['Article'] = $Fields;
               $this->EventArguments['NotifiedUsers'] = $NotifiedUsers;
               $this->EventArguments['MentionedUsers'] = $Usernames;
               $this->EventArguments['ActivityModel'] = $ActivityModel;
               $this->FireEvent('BeforeNotification');

               // Send all notifications.
               $ActivityModel->SaveQueue();
            }
            
            // Get CategoryID of this article
            
            $Article = $this->GetID($ArticleID, DATASET_TYPE_ARRAY);
            $CategoryID = GetValue('CategoryID', $Article, FALSE);
            
            // Update article counter for affected categories
            $this->UpdateArticleCount($CategoryID, $Insert ? $Article : FALSE);
            if ($StoredCategoryID)
               $this->UpdateArticleCount($StoredCategoryID);
				
				// Fire an event that the article was saved.
				$this->EventArguments['FormPostValues'] = $FormPostValues;
				$this->EventArguments['Fields'] = $Fields;
				$this->EventArguments['ArticleID'] = $ArticleID;
				$this->FireEvent('AfterSaveArticle');
         }
      }
      
      return $ArticleID;
   }
   
   /**
    * Gets number of bookmarks specified article has (all users).
    *
    * @since 2.0.0
    * @access public
    *
    * @param int $ArticleID Unique ID of article for which to tally bookmarks.
    * @return int Total number of bookmarks.
    */
   public function BookmarkCount($ArticleID) {
      $Data = $this->SQL
         ->Select('ArticleID', 'count', 'Count')
         ->From('UserArticle')
         ->Where('ArticleID', $ArticleID)
         ->Where('Bookmarked', '1')
         ->Get()
         ->FirstRow();
         
      return $Data !== FALSE ? $Data->Count : 0;
   }
   
   /**
    * Gets number of bookmarks specified user has.
    *
    * @since 2.0.0
    * @access public
    *
    * @param int $UserID Unique ID of user for which to tally bookmarks.
    * @return int Total number of article bookmarks.
    */
   public function UserBookmarkCount($UserID) {
      $Data = $this->SQL
         ->Select('ua.ArticleID', 'count', 'Count')
         ->From('UserArticle ua')
         ->Join('Article a', 'a.ArticleID = ua.ArticleID')
         ->Where('ua.UserID', $UserID)
         ->Where('ua.Bookmarked', '1')
         ->Get()
         ->FirstRow();
         
      return $Data !== FALSE ? $Data->Count : 0;
   }
   
   /**
    * Get data for a single article by UrlCode.
    * 
    * @since 2.0.0
    * @access public
    * 
	 * @param int $UrlCode Unique UrlCode of article to get.
	 * @return object SQL result.
	 */
   public function GetUrlCode($UrlCode) {
      $Session = Gdn::Session();
      $this->FireEvent('BeforeGetUrlCode');
      $Article = $this->SQL
         ->Select('a.*')
         ->Select('w.DateLastViewed, w.Dismissed, w.Bookmarked')
         ->Select('w.CountArticleComments', '', 'CountArticleCommentWatch')
         ->Select('a.DateLastComment', '', 'LastDate')
         ->Select('a.LastCommentUserID', '', 'LastUserID')
         ->From('Article a')
         ->Join('UserArticle w', 'a.ArticleID = w.ArticleID and w.UserID = '.$Session->UserID, 'left')
         ->Where('a.UrlCode', $UrlCode)
         ->Get()
         ->FirstRow();
      
      if (!$Article)
         return $Article;

      // Join in the users.
      $Article = array($Article);
      Gdn::UserModel()->JoinUsers($Article, array('LastUserID', 'InsertUserID'));
      $Article = $Article[0];
      
      $this->Calculate($Article);
		
		return $Article;
   }
   
   /**
    * Get data for a single article by ID.
    * 
    * @since 2.0.0
    * @access public
    * 
	 * @param int $ArticleID Unique ID of article to get.
	 * @return object SQL result.
	 */
   public function GetID($ArticleID) {
      $Session = Gdn::Session();
      $this->FireEvent('BeforeGetID');
      $Article = $this->SQL
         ->Select('a.*')
         ->Select('w.DateLastViewed, w.Dismissed, w.Bookmarked')
         ->Select('w.CountArticleComments', '', 'CountArticleCommentWatch')
         ->Select('a.DateLastComment', '', 'LastDate')
         ->Select('a.LastCommentUserID', '', 'LastUserID')
         ->From('Article a')
         ->Join('UserArticle w', 'a.ArticleID = w.ArticleID and w.UserID = '.$Session->UserID, 'left')
         ->Where('a.ArticleID', $ArticleID)
         ->Get()
         ->FirstRow();
      
      if (!$Article)
         return $Article;

      // Join in the users.
      $Article = array($Article);
      Gdn::UserModel()->JoinUsers($Article, array('LastUserID', 'InsertUserID'));
      $Article = $Article[0];
      
      $this->Calculate($Article);
		
		return $Article;
   }
   
   /**
    * Bookmarks (or unbookmarks) a article for specified user.
    * 
    * Events: AfterBookmarkArticle.
    *
    * @since 2.0.0
    * @access public
    *
    * @param int $ArticleID Unique ID of article to (un)bookmark.
    * @param int $UserID Unique ID of user doing the (un)bookmarking.
    * @param object $Article Article data.
    * @return bool Current state of the bookmark (TRUE for bookmarked, FALSE for unbookmarked).
    */
   public function BookmarkArticle($ArticleID, $UserID, &$Article = NULL) {
      $State = '1';

      $ArticleData = $this->SQL
         ->Select('a.*')
         ->Select('w.DateLastViewed, w.Dismissed, w.Bookmarked')
         ->Select('w.CountArticleComments', '', 'CountArticleCommentWatch')
         ->Select('w.UserID', '', 'WatchUserID')
         ->Select('a.DateLastComment', '', 'LastDate')
         ->Select('a.LastCommentUserID', '', 'LastUserID')
         ->Select('lcu.Name', '', 'LastName')
         ->From('Article a')
         ->Join('UserArticle w', "a.ArticleID = w.ArticleID and w.UserID = $UserID", 'left')
			->Join('User lcu', 'a.LastCommentUserID = lcu.UserID', 'left') // Last comment user
         ->Where('a.ArticleID', $ArticleID)
         ->Get();
			
		$this->AddArticleColumns($ArticleData);
		$Article = $ArticleData->FirstRow();

      if ($Article->WatchUserID == '') {
         $this->SQL->Options('Ignore', TRUE);
         $this->SQL
            ->Insert('UserArticle', array(
               'UserID' => $UserID,
               'ArticleID' => $ArticleID,
               'Bookmarked' => $State
            ));
         $Article->Bookmarked = TRUE;
      } else {
         $State = ($Article->Bookmarked == '1' ? '0' : '1');
         $this->SQL
            ->Update('UserArticle')
            ->Set('Bookmarked', $State)
            ->Where('UserID', $UserID)
            ->Where('ArticleID', $ArticleID)
            ->Put();
         $Article->Bookmarked = $State;
      }
		
		// Update the cached bookmark count on the article
		$BookmarkCount = $this->BookmarkCount($ArticleID);
		$this->SQL->Update('Article')
			->Set('CountArticleBookmarks', $BookmarkCount)
			->Where('ArticleID', $ArticleID)
			->Put();
      $this->CountArticleBookmarks = $BookmarkCount;
		
		
		// Prep and fire event	
      $this->EventArguments['Article'] = $Article;
      $this->EventArguments['State'] = $State;
      $this->FireEvent('AfterBookmarkArticle');
      
      return $State == '1' ? TRUE : FALSE;
   }
   
	/**
	 * Update and get article bookmark count for the specified user.
	 *
    * @since 2.0.0
    * @access public
    *
    * @param int $UserID Unique ID of user to update.
    * @return int Total number of article bookmarks user has.
    */
	public function SetUserBookmarkCount($UserID) {
		$Count = $this->UserBookmarkCount($UserID);
      Gdn::UserModel()->SetField($UserID, 'CountArticleBookmarks', $Count);
		
		return $Count;
	}
}
