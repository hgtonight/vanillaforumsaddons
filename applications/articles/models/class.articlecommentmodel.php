<?php if (!defined('APPLICATION')) exit();
/*
Copyright 2008, 2009 Articles Forums Inc.
This file is part of Garden.
Garden is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Garden is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Garden.  If not, see <http://www.gnu.org/licenses/>.
Contact Articles Forums Inc. at support [at] articlesforums [dot] com
*/
/**
 * Article Comments Model
 *
 * @package Articles
 */
 
/**
 * Manages feature comments.
 *
 * @since 2.0.0
 * @package Articles
 */
class ArticleCommentModel extends ArticlesModel {
   /**
    * List of fields to order results by.
    * 
    * @var array
    * @access protected
    * @since 2.0.0
    */
   protected $_OrderBy = array(array('c.DateInserted', ''));
   
   protected $_Where = array();
   
   /**
    * Class constructor. Defines the related database table name.
    * 
    * @since 2.0.0
    * @access public
    */
   public function __construct() {
      parent::__construct('ArticleComment');
      $this->FireEvent('AfterConstruct');
   }
   
   public function CachePageWhere($Result, $PageWhere, $ArticleID, $Page, $Limit = NULL) {
      if (!Gdn::Cache()->ActiveEnabled() || !empty($this->_Where) || $this->_OrderBy[0][0] != 'c.DateInserted' || $this->_OrderBy[0][1] == 'desc')
         return;
      
      if (count($Result) == 0)
         return;
      
      $ConfigLimit = C('Articles.Comments.PerPage', 30);
      
      if (!$Limit)
         $Limit = $ConfigLimit;
      
      if ($Limit != $ConfigLimit) {
         return;
      }
      
      if (is_array($PageWhere))
         $Curr = array_values($PageWhere);
      else
         $Curr = FALSE;
      
      $New = array(GetValueR('0.DateInserted', $Result));
      
      if (count($Result) >= $Limit)
         $New[] = GetValueR(($Limit - 1).'.DateInserted', $Result);
      
      if ($Curr != $New) {
         Trace('ArticleCommentModel->CachePageWhere()');
         
         $CacheKey = "ArticleComment.Page.$Limit.$ArticleID.$Page";
         Gdn::Cache()->Store($CacheKey, $New, array(Gdn_Cache::FEATURE_EXPIRY => 86400));
         
         Trace($New, $CacheKey);
      }
   }
   
   /**
    * Select the data for a single comment.
    * 
    * @since 2.0.0
    * @access public
    * 
    * @param bool $FireEvent Kludge to fix ArticleCommentReplies plugin.
    */
   public function CommentQuery($FireEvent = TRUE, $Join = TRUE) {
      $this->SQL->Select('c.*')
         ->From('ArticleComment c');
      
      if ($Join) {
         $this->SQL
            ->Select('iu.Name', '', 'InsertName')
            ->Select('iu.Photo', '', 'InsertPhoto')
            ->Select('iu.Email', '', 'InsertEmail')
            ->Join('User iu', 'c.InsertUserID = iu.UserID', 'left')
         
            ->Select('uu.Name', '', 'UpdateName')
            ->Select('uu.Photo', '', 'UpdatePhoto')
            ->Select('uu.Email', '', 'UpdateEmail')
            ->Join('User uu', 'c.UpdateUserID = uu.UserID', 'left');
      }
      
      if($FireEvent)
         $this->FireEvent('AfterCommentQuery');
   }
   
   /** 
    * Set the order of the comments or return current order. 
    * 
    * Getter/setter for $this->_OrderBy.
    * 
    * @since 2.0.0
    * @access public
    * 
    * @param mixed Field name(s) to order results by. May be a string or array of strings.
    * @return array $this->_OrderBy (optionally).
    */
   public function OrderBy($Value = NULL) {
      if ($Value === NULL)
         return $this->_OrderBy;

      if (is_string($Value))
         $Value = array($Value);

      if (is_array($Value)) {
         // Set the order of this object.
         $OrderBy = array();

         foreach($Value as $Part) {
            if (StringEndsWith($Part, ' desc', TRUE))
               $OrderBy[] = array(substr($Part, 0, -5), 'desc');
            elseif (StringEndsWith($Part, ' asc', TRUE))
               $OrderBy[] = array(substr($Part, 0, -4), 'asc');
            else
               $OrderBy[] = array($Part, 'asc');
         }
         $this->_OrderBy = $OrderBy;
      } elseif (is_a($Value, 'Gdn_SQLDriver')) {
         // Set the order of the given sql.
         foreach ($this->_OrderBy as $Parts) {
            $Value->OrderBy($Parts[0], $Parts[1]);
         }
      }
   }
   
   public function PageWhere($ArticleID, $Page, $Limit) {
      if (!Gdn::Cache()->ActiveEnabled() || !empty($this->_Where) || $this->_OrderBy[0][0] != 'c.DateInserted' || $this->_OrderBy[0][1] == 'desc')
         return FALSE;
      
      if ($Limit != C('Articles.Comments.PerPage', 30)) {
         return FALSE;
      }
      
      $CacheKey = "ArticleComment.Page.$Limit.$ArticleID.$Page";
      $Value = Gdn::Cache()->Get($CacheKey);
      Trace('ArticleCommentModel->PageWhere()');
      Trace($Value, $CacheKey);
      
      if ($Value === FALSE) {
         return FALSE;
      } elseif (is_array($Value)) {
         $Result = array('DateInserted >=' => $Value[0]);
         if (isset($Value[1])) {
            $Result['DateInserted <='] = $Value[1];
         }
         return $Result;
      }
      return FALSE;
   }
   
   /**
    * Get comments for an article.
    * 
    * @since 2.0.0
    * @access public
    * 
    * @param int $ArticleID Which article to get comment from.
    * @param int $Limit Max number to get.
    * @param int $Offset Number to skip.
    * @return object SQL results.
    */
   public function Get($ArticleID, $Limit, $Offset = 0) {
      $this->CommentQuery(TRUE, FALSE);
      $this->EventArguments['ArticleID'] =& $ArticleID;
      $this->EventArguments['Limit'] =& $Limit;
      $this->EventArguments['Offset'] =& $Offset;
      $this->FireEvent('BeforeGet');
      
      $Page = PageNumber($Offset, $Limit);
      $PageWhere = $this->PageWhere($ArticleID, $Page, $Limit);
      
      if ($PageWhere) {
         $this->SQL
            ->Where('c.ArticleID', $ArticleID);
         
         $this->SQL->Where($PageWhere)->Limit($Limit + 10);
         $this->OrderBy($this->SQL);
      } else {
         // Do an inner-query to force late-loading of comments.
         $Sql2 = clone $this->SQL;
         $Sql2->Reset();
         $Sql2->Select('CommentID')
            ->From('ArticleComment c')
            ->Where('c.ArticleID', $ArticleID, TRUE, FALSE)
            ->Limit($Limit, $Offset);
         $this->OrderBy($Sql2);
         $Select = $Sql2->GetSelect();
         
         $Px = $this->SQL->Database->DatabasePrefix;
         $this->SQL->Database->DatabasePrefix = '';
         
         $this->SQL->Join("($Select) c2", "c.CommentID = c2.CommentID");
         $this->SQL->Database->DatabasePrefix = $Px;
      }
      
      $this->Where($this->SQL);

      $Result = $this->SQL->Get();
      
      Gdn::UserModel()->JoinUsers($Result, array('InsertUserID', 'UpdateUserID'));
      
      $this->SetCalculatedFields($Result);
      
      $this->EventArguments['Comments'] =& $Result;
      $this->FireEvent('AfterGet');
      
      $this->CachePageWhere($Result->Result(), $PageWhere, $ArticleID, $Page, $Limit);
      
      return $Result;
   }
   
   /**
    * Modifies comment data before it is returned.
    * 
    * @since 2.1a32
    * @access public
    *
    * @param object $Data SQL result.
    */
	public function SetCalculatedFields(&$Data) {
		$Result = &$Data->Result();
		foreach($Result as &$Comment) {
         $this->Calculate($Comment);
      }
   }
   
   /**
    * Modifies comment data before it is returned.
    * 
    * @since 2.1a32
    * @access public
    *
    * @param object $Data SQL result.
    */
   public function Calculate($Comment) {
      $this->EventArguments['Comment'] = $Comment;
      $this->FireEvent('SetCalculatedFields');
   }
   
   public function Where($Value = NULL) {
      if ($Value === NULL)
         return $this->_Where;
      elseif (!$Value)
         $this->_Where = array();
      elseif (is_a($Value, 'Gdn_SQLDriver')) {
         if (!empty($this->_Where))
            $Value->Where($this->_Where);
      } else
         $this->_Where = $Value;
   }
   
   /**
	 * Record the user's watch data.
	 * 
    * @since 2.0.0
    * @access public
    * 
    * @param object $Article Article being watched.
    * @param int $Limit Max number to get.
    * @param int $Offset Number to skip.
    * @param int $TotalComments Total in entire article (hard limit).
	 */
   public function SetWatch($Article, $Limit, $Offset, $TotalComments) {
      $NewComments = FALSE;
      
      $Session = Gdn::Session();
      if ($Session->UserID > 0) {
         // Max comments we could have seen
         $CountWatch = $Limit * ($Offset + 1);
         if ($CountWatch > $TotalComments)
            $CountWatch = $TotalComments;
         
         // This dicussion looks familiar...
         if (is_numeric($Article->CountCommentWatch)) {
            
            if (isset($Article->DateLastViewed))
               $NewComments |= Gdn_Format::ToTimestamp($Article->DateLastComment) > Gdn_Format::ToTimestamp($Article->DateLastViewed);
            
            if ($TotalComments > $Article->CountCommentWatch)
               $NewComments |= TRUE;

            // Update the watch data.
				if ($NewComments) {
               
					// Only update the watch if there are new comments.
					$this->SQL->Put(
						'UserArticle',
						array(
							'CountArticleComments' => $CountWatch,
                     'DateLastViewed' => Gdn_Format::ToDateTime()
						),
						array(
							'UserID' => $Session->UserID,
							'ArticleID' => $Article->ArticleID
						)
					);
				}
            
         } else {
				// Make sure the article isn't archived.
				$ArchiveDate = C('Articles.Archive.Date', FALSE);
				if (!$ArchiveDate || (Gdn_Format::ToTimestamp($Article->DateLastComment) > Gdn_Format::ToTimestamp($ArchiveDate))) {
               
               $NewComments = TRUE;
               
					// Insert watch data.
               $this->SQL->Options('Ignore', TRUE);
					$this->SQL->Insert(
						'UserArticle',
						array(
							'UserID' => $Session->UserID,
							'ArticleID' => $Article->ArticleID,
							'CountArticleComments' => $CountWatch,
                     'DateLastViewed' => Gdn_Format::ToDateTime()
						)
					);
				}
			}
         
         /**
          * Fuzzy way of trying to automatically mark a cateogyr read again
          * if the user reads all the comments on the first few pages.
          */
         
         // If this article is in a category that has been marked read, 
         // check if reading this thread causes it to be completely read again
         $CategoryID = GetValue('CategoryID', $Article);
         if ($CategoryID) {
            
            $Category = ArticleCategoryModel::Categories($CategoryID);
            if ($Category) {
               
               $DateMarkedRead = GetValue('DateMarkedRead', $Category);
               if ($DateMarkedRead) {
                  
                  // Fuzzy way of looking back about 2 pages into the past
                  $LookBackCount = C('Articles.Articles.PerPage', 50) * 2;
                  
                  // Find all articles with content from after DateMarkedRead
                  $ArticleModel = new ArticleModel();
                  $Articles = $ArticleModel->Get(0, 101, array(
                     'CategoryID'         => $CategoryID,
                     'DateLastComment>'   => $DateMarkedRead
                  ));
                  unset($ArticleModel);
                  
                  // Abort if we get back as many as we asked for, meaning a 
                  // lot has happened.
                  $NumArticles = $Articles->NumRows();
                  if ($NumArticles <= $LookBackCount) {
                  
                     // Loop over these and see if any are still unread
                     $MarkAsRead = TRUE;
                     while ($Article = $Articles->NextRow(DATASET_TYPE_ARRAY)) {
                        if ($Article['Read']) continue;
                        $MarkAsRead = FALSE;
                        break;
                     }

                     // Mark this category read if all the new content is read
                     if ($MarkAsRead) {
                        $CategoryModel = new ArticleCategoryModel();
                        $CategoryModel->SaveUserTree($CategoryID, array('DateMarkedRead' => Gdn_Format::ToDateTime()));
                        unset($CategoryModel);
                     }
                     
                  }
               }
            }
         }
         
		}
   }
}
