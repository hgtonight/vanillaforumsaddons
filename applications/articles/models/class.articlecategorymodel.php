<?php if (!defined('APPLICATION')) exit();
/*
Copyright 2008, 2009 Articles Forums Inc.
This file is part of Garden.
Garden is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Garden is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Garden.  If not, see <http://www.gnu.org/licenses/>.
Contact Articles Forums Inc. at support [at] articlesforums [dot] com
*/
/**
 * Article Category Model
 *
 * @package Articles
 */
 
/**
 * Manages feature categories.
 *
 * @since 2.0.0
 * @package Articles
 */
class ArticleCategoryModel extends Gdn_Model {
   const CACHE_KEY = 'ArticleCategories';
   
   public $Watching = FALSE;
   
   /**
    * Merged Category data, including Pure + UserArticleCategory
    * 
    * @var array
    */
   public static $Categories = NULL;

   /**
    * Class constructor. Defines the related database table name.
    * 
    * @since 2.0.0
    * @access public
    */
   public function __construct() {
      parent::__construct('ArticleCategory');
   }
   
   /**
    * Retrieve the categories.
    * 
    * @since 2.0.18
    * @access public
    * @param int $ID
    * @return object DataObject
    */
   public static function Categories($ID = FALSE) {
      if (self::$Categories == NULL) {
         // Try and get the categories from the cache.
         self::$Categories = Gdn::Cache()->Get(self::CACHE_KEY);
         
         if (!self::$Categories) {
            $Sql = Gdn::SQL();
            $Sql = clone $Sql;
            $Sql->Reset();
            $Session = Gdn::Session();

            $Sql->Select('c.*')
               ->Select('lc.DateInserted', '', 'DateLastComment')
               ->From('ArticleCategory c')
               ->Join('ArticleComment lc', 'c.LastCommentID = lc.CommentID', 'left')
               ->OrderBy('c.TreeLeft');

            self::$Categories = array_merge(array(), $Sql->Get()->ResultArray());
            self::$Categories = Gdn_DataSet::Index(self::$Categories, 'CategoryID');
            self::BuildCache();
         }
         
         self::JoinUserData(self::$Categories, TRUE);
      }
      
      if ($ID !== FALSE) {
         if (!is_numeric($ID) && $ID) {
            foreach (self::$Categories as $Category) {
               if ($Category['UrlCode'] == $ID)
                  $ID = $Category['CategoryID'];
            }
         }

         if (isset(self::$Categories[$ID])) {
            $Result = self::$Categories[$ID];
            return $Result;
         } else {
            return NULL;
         }
      } else {
         $Result = self::$Categories;
         return $Result;
      }
   }
   
   /**
    * Build and augment the category cache
    * 
    * @param array $Categories
    */
   protected static function BuildCache() {
      self::CalculateData(self::$Categories);
      self::JoinRecentPosts(self::$Categories);
      Gdn::Cache()->Store(self::CACHE_KEY, self::$Categories, array(Gdn_Cache::FEATURE_EXPIRY => 600));
   }
   
   /**
    * Calculate data.
    * 
    * @since 2.0.18
    * @access public
    * @param array $Data Dataset.
    */
   protected static function CalculateData(&$Data) {
		foreach ($Data as &$Category) {
         $Category['CountAllArticles'] = $Category['CountArticles'];
         $Category['CountAllComments'] = $Category['CountComments'];
         $Category['Url'] = self::ArticleCategoryUrl($Category, FALSE, '//');
         $Category['ChildIDs'] = array();
         if (GetValue('Photo', $Category))
            $Category['PhotoUrl'] = Gdn_Upload::Url($Category['Photo']);
         else
            $Category['PhotoUrl'] = '';
         
         if ($Category['DisplayAs'] == 'Default') {
            if ($Category['Depth'] <= C('Articles.Categories.NavDepth', 0))
               $Category['DisplayAs'] = 'Categories';
            else
               $Category['DisplayAs'] = 'Articles';
         }
         
         if (!GetValue('CssClass', $Category))
            $Category['CssClass'] = 'Category-'.$Category['UrlCode'];
		}
      
      $Keys = array_reverse(array_keys($Data));
      foreach ($Keys as $Key) {
         $Cat = $Data[$Key];
         $ParentID = $Cat['ParentCategoryID'];

         if (isset($Data[$ParentID]) && $ParentID != $Key) {
            $Data[$ParentID]['CountAllArticles'] += $Cat['CountAllArticles'];
            $Data[$ParentID]['CountAllComments'] += $Cat['CountAllComments'];
            array_unshift($Data[$ParentID]['ChildIDs'], $Key);
         }
      }
	}
   
   /**
    * Add UserArticleCategory modifiers
    * 
    * Update &$Categories in memory by applying modifiers from UserArticleCategory for
    * the currently logged-in user.
    * 
    * @since 2.0.18
    * @access public
    * @param array &$Categories
    * @param bool $AddUserArticleCategory
    */
   public static function JoinUserData(&$Categories, $AddUserArticleCategory = TRUE) {
      $IDs = array_keys($Categories);
      $DoHeadings = C('Articles.Categories.DoHeadings');
      
      if ($AddUserArticleCategory) {
         $SQL = clone Gdn::SQL();
         $SQL->Reset();
         
         if (Gdn::Session()->UserID) {
            $Key = 'UserArticleCategory_'.Gdn::Session()->UserID;
            $UserData = Gdn::Cache()->Get($Key);
            if ($UserData === Gdn_Cache::CACHEOP_FAILURE) {
               $UserData = $SQL->GetWhere('UserArticleCategory', array('UserID' => Gdn::Session()->UserID))->ResultArray();
               $UserData = Gdn_DataSet::Index($UserData, 'CategoryID');
               Gdn::Cache()->Store($Key, $UserData);
            }
         } else
            $UserData = array();
         
         foreach ($IDs as $ID) {
            $Category = $Categories[$ID];
            
            $DateMarkedRead = GetValue('DateMarkedRead', $Category);
            $Row = GetValue($ID, $UserData);
            if ($Row) {
               $UserDateMarkedRead = $Row['DateMarkedRead'];
               
               if (!$DateMarkedRead || ($UserDateMarkedRead && Gdn_Format::ToTimestamp($UserDateMarkedRead) > Gdn_Format::ToTimestamp($DateMarkedRead))) {
                  $Categories[$ID]['DateMarkedRead'] = $UserDateMarkedRead;
                  $DateMarkedRead = $UserDateMarkedRead;
               }
            }

            // Calculate the read field.
            if ($DoHeadings && $Category['Depth'] <= 1) {
               $Categories[$ID]['Read'] = FALSE;
            } elseif ($DateMarkedRead) {
               if (GetValue('LastDateInserted', $Category))
                  $Categories[$ID]['Read'] = Gdn_Format::ToTimestamp($DateMarkedRead) >= Gdn_Format::ToTimestamp($Category['LastDateInserted']);
               else
                  $Categories[$ID]['Read'] = TRUE;
            } else {
               $Categories[$ID]['Read'] = FALSE;
            }
         }
         
      }
      
      // Add permissions.
      $Session = Gdn::Session();
      foreach ($IDs as $CID) {
         $Category = $Categories[$CID];
         $Categories[$CID]['PermsArticlesView'] = $Session->CheckPermission('Articles.Articles.View', TRUE, 'ArticleCategory', $Category['PermissionCategoryID']);
         $Categories[$CID]['PermsArticlesAdd'] = $Session->CheckPermission('Articles.Articles.Add', TRUE, 'ArticleCategory', $Category['PermissionCategoryID']);
         $Categories[$CID]['PermsArticlesEdit'] = $Session->CheckPermission('Articles.Articles.Edit', TRUE, 'ArticleCategory', $Category['PermissionCategoryID']);
         $Categories[$CID]['PermsCommentsAdd'] = $Session->CheckPermission('Articles.Comments.Add', TRUE, 'ArticleCategory', $Category['PermissionCategoryID']);
      }
   }
   
   public static function JoinRecentPosts(&$Data) {
      $ArticleIDs = array();
      $CommentIDs = array();
      $Joined = FALSE;
      
      foreach ($Data as &$Row) {
         if (isset($Row['LastTitle']) && $Row['LastTitle'])
            continue;
         
         if ($Row['LastArticleID'])
            $ArticleIDs[] = $Row['LastArticleID'];
         
         if ($Row['LastCommentID']) {
            $CommentIDs[] = $Row['LastCommentID'];
         }
         $Joined = TRUE;
      }
      
      // Create a fresh copy of the Sql object so as not to pollute.
      $Sql = clone Gdn::SQL();
      $Sql->Reset();
      
      // Grab the articles.
      if (count($ArticleIDs) > 0) {
         $Articles = $Sql->WhereIn('ArticleID', $ArticleIDs)->Get('Article')->ResultArray();
         $Articles = Gdn_DataSet::Index($Articles, array('ArticleID'));
      }
      
      if (count($CommentIDs) > 0) {
         $Comments = $Sql->WhereIn('CommentID', $CommentIDs)->Get('Comment')->ResultArray();
         $Comments = Gdn_DataSet::Index($Comments, array('CommentID'));
      }
      
      foreach ($Data as &$Row) {
         $Article = GetValue($Row['LastArticleID'], $Articles);
         $NameUrl = 'x';
         if ($Article) {
            $Row['LastTitle'] = Gdn_Format::Text($Article['Name']);
            $Row['LastUserID'] = $Article['InsertUserID'];
            $Row['LastArticleUserID'] = $Article['InsertUserID'];
            $Row['LastDateInserted'] = $Article['DateInserted'];
            $NameUrl = Gdn_Format::Text($Article['Name'], TRUE);
            $ArticleModel = new ArticleModel();
            $Row['LastUrl'] = $ArticleModel->ArticleUrl($Article, FALSE, '//').'#latest';
         }
         $Comment = GetValue($Row['LastCommentID'], $Comments);
         if ($Comment) {
            $Row['LastUserID'] = $Comment['InsertUserID'];
            $Row['LastDateInserted'] = $Comment['DateInserted'];
         } else {
            $Row['NoComment'] = TRUE;
         }
         
         TouchValue('LastTitle', $Row, '');
         TouchValue('LastUserID', $Row, NULL);
         TouchValue('LastArticleUserID', $Row, NULL);
         TouchValue('LastDateInserted', $Row, NULL);
         TouchValue('LastUrl', $Row, NULL);
      }
      return $Joined;
   }
   
   /**
    * 
    * 
    * @since 2.0.18
    * @access public
    * @param array $Data Dataset.
    * @param string $Column Name of database column.
    * @param array $Options 'Join' key may contain array of columns to join on.
    */
   public static function JoinCategories(&$Data, $Column = 'CategoryID', $Options = array()) {
      $Join = GetValue('Join', $Options, array('Name' => 'Category', 'PermissionCategoryID', 'UrlCode' => 'CategoryUrlCode'));
      foreach ($Data as &$Row) {
         $ID = GetValue($Column, $Row);
         $Category = self::Categories($ID);
         foreach ($Join as $N => $V) {
            if (is_numeric($N))
               $N = $V;
            
            if ($Category)
               $Value = $Category[$N];
            else
               $Value = NULL;
            
            SetValue($V, $Row, $Value);
         }
      }
   }
   
   /**
    * Get data for a single category selected by ID. Disregards permissions.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param int $CategoryID Unique ID of category we're getting data for.
    * @return object SQL results.
    */
   public function GetID($CategoryID, $DatasetType = DATASET_TYPE_OBJECT) {
      return $this->SQL->GetWhere('ArticleCategory', array('CategoryID' => $CategoryID))->FirstRow($DatasetType);
   }
   
   /**
    * Get data for a single category selected by Url Code. Disregards permissions.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param int $CodeID Unique Url Code of category we're getting data for.
    * @return object SQL results.
    */
   public function GetByCode($Code) {
      return $this->SQL->GetWhere('ArticleCategory', array('UrlCode' => $Code))->FirstRow();
   }
   
   /**
    * Get all of the ancestor categories above this one.
    * @param int|string $Category The category ID or url code.
    * @param bool $CheckPermissions Whether or not to only return the categories with view permission.
    * @return array
    */
   public static function GetAncestors($CategoryID, $CheckPermissions = TRUE) {
      $Categories = self::Categories();
      $Result = array();
      
      // Grab the category by ID or url code.
      if (is_numeric($CategoryID)) {
         if (isset($Categories[$CategoryID]))
            $Category = $Categories[$CategoryID];
      } else {
         foreach ($Categories as $ID => $Value) {
            if ($Value['UrlCode'] == $CategoryID) {
               $Category = $Categories[$ID];
               break;
            }
         }
      }

      if (!isset($Category))
         return $Result;

      // Build up the ancestor array by tracing back through parents.
      $Result[$Category['CategoryID']] = $Category;
      $Max = 20;
      while (isset($Categories[$Category['ParentCategoryID']])) {
         // Check for an infinite loop.
         if ($Max <= 0)
            break;
         $Max--;
         
         if ($CheckPermissions && !$Category['PermsArticlesView']) {
            $Category = $Categories[$Category['ParentCategoryID']];
            continue;
         }
         
         if ($Category['CategoryID'] == -1)
            break;

         // Return by ID or code.
         if (is_numeric($CategoryID))
            $ID = $Category['CategoryID'];
         else
            $ID = $Category['UrlCode'];

         $Result[$ID] = $Category;

         $Category = $Categories[$Category['ParentCategoryID']];
      }
      $Result = array_reverse($Result, TRUE); // order for breadcrumbs
      return $Result;
   }
   
   /**
    * Rebuilds the category tree. We are using the Nested Set tree model.
    * 
    * @ref http://articles.sitepoint.com/article/hierarchical-data-database/2
    * @ref http://en.wikipedia.org/wiki/Nested_set_model
    *  
    * @since 2.0.0
    * @access public
    */
   public function RebuildTree() {
      // Grab all of the categories.
      $Categories = $this->SQL->Get('ArticleCategory', 'TreeLeft, Sort, Name');
      $Categories = Gdn_DataSet::Index($Categories->ResultArray(), 'CategoryID');

      // Make sure the tree has a root.
      if (!isset($Categories[-1])) {
         $RootCat = array('CategoryID' => -1, 'TreeLeft' => 1, 'TreeRight' => 4, 'Depth' => 0, 'InsertUserID' => 1, 'UpdateUserID' => 1, 'DateInserted' => Gdn_Format::ToDateTime(), 'DateUpdated' => Gdn_Format::ToDateTime(), 'Name' => 'Root', 'UrlCode' => '', 'Description' => 'Root of category tree. Users should never see this.', 'PermissionCategoryID' => -1, 'Sort' => 0, 'ParentCategoryID' => NULL);
         $Categories[-1] = $RootCat;
         $this->SQL->Insert('ArticleCategory', $RootCat);
      }

      // Build a tree structure out of the categories.
      $Root = NULL;
      foreach ($Categories as &$Cat) {
         if (!isset($Cat['CategoryID']))
            continue;
         
         // Backup category settings for efficient database saving.
         try {
            $Cat['_TreeLeft'] = $Cat['TreeLeft'];
            $Cat['_TreeRight'] = $Cat['TreeRight'];
            $Cat['_Depth'] = $Cat['Depth'];
            $Cat['_PermissionCategoryID'] = $Cat['PermissionCategoryID'];
            $Cat['_ParentCategoryID'] = $Cat['ParentCategoryID'];
         } catch (Exception $Ex) {
         }

         if ($Cat['CategoryID'] == -1) {
            $Root =& $Cat;
            continue;
         }

         $ParentID = $Cat['ParentCategoryID'];
         if (!$ParentID) {
            $ParentID = -1;
            $Cat['ParentCategoryID'] = $ParentID;
         }
         if (!isset($Categories[$ParentID]['Children']))
            $Categories[$ParentID]['Children'] = array();
         $Categories[$ParentID]['Children'][] =& $Cat;
      }
      unset($Cat);

      // Set the tree attributes of the tree.
      $this->_SetTree($Root);
      unset($Root);

      // Save the tree structure.
      foreach ($Categories as $Cat) {
         if (!isset($Cat['CategoryID']))
            continue;
         if ($Cat['_TreeLeft'] != $Cat['TreeLeft'] || $Cat['_TreeRight'] != $Cat['TreeRight'] || $Cat['_Depth'] != $Cat['Depth'] || $Cat['PermissionCategoryID'] != $Cat['PermissionCategoryID'] || $Cat['_ParentCategoryID'] != $Cat['ParentCategoryID'] || $Cat['Sort'] != $Cat['TreeLeft']) {
            $this->SQL->Put('ArticleCategory',
               array('TreeLeft' => $Cat['TreeLeft'], 'TreeRight' => $Cat['TreeRight'], 'Depth' => $Cat['Depth'], 'PermissionCategoryID' => $Cat['PermissionCategoryID'], 'ParentCategoryID' => $Cat['ParentCategoryID'], 'Sort' => $Cat['TreeLeft']),
               array('CategoryID' => $Cat['CategoryID']));
         }
      }
      $this->SetCache();
   }
   
   /**
    * Saves the category.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param array $FormPostValue The values being posted back from the form.
    * @return int ID of the saved category.
    */
   public function Save($FormPostValues) {
      // Define the primary key in this model's table.
      $this->DefineSchema();
      
      // Get data from form
      $CategoryID = ArrayValue('CategoryID', $FormPostValues);
      $NewName = ArrayValue('Name', $FormPostValues, '');
      $UrlCode = ArrayValue('UrlCode', $FormPostValues, '');
      $AllowArticles = ArrayValue('AllowArticles', $FormPostValues, '');
      $CustomPermissions = (bool)GetValue('CustomPermissions', $FormPostValues);
      $CustomPoints = GetValue('CustomPoints', $FormPostValues, NULL);
      
      // Is this a new category?
      $Insert = $CategoryID > 0 ? FALSE : TRUE;
      if ($Insert)
         $this->AddInsertFields($FormPostValues);               

      $this->AddUpdateFields($FormPostValues);
      $this->Validation->ApplyRule('UrlCode', 'Required');
      $this->Validation->ApplyRule('UrlCode', 'UrlStringRelaxed');

      // Make sure that the UrlCode is unique among categories.
      $this->SQL->Select('CategoryID')
         ->From('ArticleCategory')
         ->Where('UrlCode', $UrlCode);

      if ($CategoryID)
         $this->SQL->Where('CategoryID <>', $CategoryID);

      if ($this->SQL->Get()->NumRows())
         $this->Validation->AddValidationResult('UrlCode', 'The specified url code is already in use by another category.');

		//	Prep and fire event.
		$this->EventArguments['FormPostValues'] = &$FormPostValues;
		$this->EventArguments['CategoryID'] = $CategoryID;
		$this->FireEvent('BeforeSaveCategory');
      
      // Validate the form posted values
      if ($this->Validate($FormPostValues, $Insert)) {
         $Fields = $this->Validation->SchemaValidationFields();
         $Fields = RemoveKeyFromArray($Fields, 'CategoryID');
         $AllowArticles = ArrayValue('AllowArticles', $Fields) == '1' ? TRUE : FALSE;
         $Fields['AllowArticles'] = $AllowArticles ? '1' : '0';

         if ($Insert === FALSE) {
            $OldCategory = $this->GetID($CategoryID, DATASET_TYPE_ARRAY);
            $AllowArticles = $OldCategory['AllowArticles']; // Force the allowarticles property
            $Fields['AllowArticles'] = $AllowArticles ? '1' : '0';
            
            // Figure out custom points.
            if ($CustomPoints !== NULL) {
               if ($CustomPoints) {
                  $Fields['PointsCategoryID'] = $CategoryID;
               } else {
                  $Parent = self::Categories(GetValue('ParentCategoryID', $Fields, $OldCategory['ParentCategoryID']));
                  $Fields['PointsCategoryID'] = GetValue('PointsCategoryID', $Parent, 0);
               }
            }
            
            $this->Update($Fields, array('CategoryID' => $CategoryID));
            
            // Check for a change in the parent category.
            if (isset($Fields['ParentCategoryID']) && $OldCategory['ParentCategoryID'] != $Fields['ParentCategoryID']) {
               $this->RebuildTree();
            } else {
               $this->SetCache($CategoryID, $Fields);
            }
         } else {
            $CategoryID = $this->Insert($Fields);

            if ($CategoryID) {
               if ($CustomPermissions) {
                  $this->SQL->Put('ArticleCategory', array('PermissionCategoryID' => $CategoryID), array('CategoryID' => $CategoryID));
               }
               if ($CustomPoints) {
                  $this->SQL->Put('ArticleCategory', array('PointsCategoryID' => $CategoryID), array('CategoryID' => $CategoryID));
               }
            }

            $this->RebuildTree(); // Safeguard to make sure that treeleft and treeright cols are added
         }
         
         // Save the permissions
         if ($AllowArticles && $CategoryID) {
            // Check to see if this category uses custom permissions.
            if ($CustomPermissions) {
               $PermissionModel = Gdn::PermissionModel();
               $Permissions = $PermissionModel->PivotPermissions(GetValue('Permission', $FormPostValues, array()), array('JunctionID' => $CategoryID));
               $PermissionModel->SaveAll($Permissions, array('JunctionID' => $CategoryID, 'JunctionTable' => 'Category'));

               if (!$Insert) {
                  // Figure out my last permission and tree info.
                  $Data = $this->SQL->Select('PermissionCategoryID, TreeLeft, TreeRight')->From('ArticleCategory')->Where('CategoryID', $CategoryID)->Get()->FirstRow(DATASET_TYPE_ARRAY);

                  // Update this category's permission.
                  $this->SQL->Put('ArticleCategory', array('PermissionCategoryID' => $CategoryID), array('CategoryID' => $CategoryID));

                  // Update all of my children that shared my last category permission.
                  $this->SQL->Put('ArticleCategory',
                     array('PermissionCategoryID' => $CategoryID),
                     array('TreeLeft >' => $Data['TreeLeft'], 'TreeRight <' => $Data['TreeRight'], 'PermissionCategoryID' => $Data['PermissionCategoryID']));
                  
                  self::ClearCache();
               }
            } elseif (!$Insert) {
               // Figure out my parent's permission.
               $NewPermissionID = $this->SQL
                  ->Select('p.PermissionCategoryID')
                  ->From('ArticleCategory c')
                  ->Join('ArticleCategory p', 'c.ParentCategoryID = p.CategoryID')
                  ->Where('c.CategoryID', $CategoryID)
                  ->Get()->Value('PermissionCategoryID', 0);

               if ($NewPermissionID != $CategoryID) {
                  // Update all of my children that shared my last permission.
                  $this->SQL->Put('ArticleCategory',
                     array('PermissionCategoryID' => $NewPermissionID),
                     array('PermissionCategoryID' => $CategoryID));
                  
                  self::ClearCache();
               }

               // Delete my custom permissions.
               $this->SQL->Delete('Permission',
                  array('JunctionTable' => 'Category', 'JunctionColumn' => 'PermissionCategoryID', 'JunctionID' => $CategoryID));
            }
         }
         
         // Force the user permissions to refresh.
         Gdn::UserModel()->ClearPermissions();
         
         // $this->RebuildTree();
      } else {
         $CategoryID = FALSE;
      }
      
      return $CategoryID;
   }
   
   /**
    * Grab the Category IDs of the tree.
    * 
    * @since 2.0.18
    * @access public
    * @param int $CategoryID
    * @param mixed $Set
    */
   public function SaveUserTree($CategoryID, $Set) {
      $Categories = $this->GetSubtree($CategoryID);
      foreach ($Categories as $Category) {
         $this->SQL->Replace(
            'UserArticleCategory',
            $Set,
            array('UserID' => Gdn::Session()->UserID, 'CategoryID' => $Category['CategoryID']));
      }
      $Key = 'UserArticleCategory_'.Gdn::Session()->UserID;
      Gdn::Cache()->Remove($Key);
   }
   
	/**
    * Modifies category data before it is returned.
    *
    * Adds CountAllArticles column to each category representing the sum of
    * articles within this category as well as all subcategories.
    * 
    * @since 2.0.17
    * @access public
    *
    * @param object $Data SQL result.
    */
	public static function AddCategoryColumns($Data) {
		$Result = &$Data->Result();
      $Result2 = $Result;
		foreach ($Result as &$Category) {
         if (!property_exists($Category, 'CountAllArticles'))
            $Category->CountAllArticles = $Category->CountArticles;
            
         if (!property_exists($Category, 'CountAllComments'))
            $Category->CountAllComments = $Category->CountComments;

         // Calculate the following field.
         $Following = !((bool)GetValue('Archived', $Category) || (bool)GetValue('Unfollow', $Category));
         $Category->Following = $Following;
            
         $DateMarkedRead = GetValue('DateMarkedRead', $Category);
         $UserDateMarkedRead = GetValue('UserDateMarkedRead', $Category);
         
         if (!$DateMarkedRead)
            $DateMarkedRead = $UserDateMarkedRead;
         elseif ($UserDateMarkedRead && Gdn_Format::ToTimestamp($UserDateMarkedRead) > Gdn_Format::ToTimeStamp($DateMarkedRead))
            $DateMarkedRead = $UserDateMarkedRead;
         
         // Set appropriate Last* columns.
         SetValue('LastTitle', $Category, GetValue('LastArticleTitle', $Category, NULL));
         $LastDateInserted = GetValue('LastDateInserted', $Category, NULL);
         
         $ArticleModel = new ArticleModel();
         
         if (GetValue('LastCommentUserID', $Category) == NULL) {
            SetValue('LastCommentUserID', $Category, GetValue('LastArticleUserID', $Category, NULL));
            SetValue('DateLastComment', $Category, GetValue('DateLastArticle', $Category, NULL));
            SetValue('LastUserID', $Category, GetValue('LastArticleUserID', $Category, NULL));
            
            $LastArticle = ArrayTranslate($Category, array(
                'LastArticleID' => 'ArticleID', 
                'CategoryID' => 'CategoryID',
                'LastTitle' => 'Name'));
            
            SetValue('LastUrl', $Category, $ArticleModel->ArticleUrl($LastArticle, FALSE, '//').'#latest');
            
            if (is_null($LastDateInserted))
               SetValue('LastDateInserted', $Category, GetValue('DateLastArticle', $Category, NULL));
         } else {
            $LastArticle = ArrayTranslate($Category, array(
               'LastArticleID' => 'ArticleID', 
               'CategoryID' => 'CategoryID',
               'LastTitle' => 'Name'
            ));
            
            SetValue('LastUserID', $Category, GetValue('LastCommentUserID', $Category, NULL));
            SetValue('LastUrl', $Category, $ArticleModel->ArticleUrl($LastArticle, FALSE, '//').'#latest');
            
            if (is_null($LastDateInserted))
               SetValue('LastDateInserted', $Category, GetValue('DateLastComment', $Category, NULL));
         }
         
         $LastDateInserted = GetValue('LastDateInserted', $Category, NULL);
         if ($DateMarkedRead) {
            if ($LastDateInserted)
               $Category->Read = Gdn_Format::ToTimestamp($DateMarkedRead) >= Gdn_Format::ToTimestamp($LastDateInserted);
            else
               $Category->Read = TRUE;
         } else {
            $Category->Read = FALSE;
         }

         foreach ($Result2 as $Category2) {
            if ($Category2->TreeLeft > $Category->TreeLeft && $Category2->TreeRight < $Category->TreeRight) {
               $Category->CountAllArticles += $Category2->CountArticles;
               $Category->CountAllComments += $Category2->CountComments;
            }
         }
		}
	}
   
   /**
    * Get list of categories (respecting user permission).
    * 
    * @since 2.0.0
    * @access public
    *
    * @param string $OrderFields Ignored.
    * @param string $OrderDirection Ignored.
    * @param int $Limit Ignored.
    * @param int $Offset Ignored.
    * @return Gdn_DataSet SQL results.
    */
   public function Get($OrderFields = '', $OrderDirection = 'asc', $Limit = FALSE, $Offset = FALSE) {
      $this->SQL
         ->Select('c.ParentCategoryID, c.CategoryID, c.TreeLeft, c.TreeRight, c.Depth, c.Name, c.Description, c.CountArticles, c.AllowArticles, c.UrlCode')
         ->From('ArticleCategory c')
         ->BeginWhereGroup()
         ->Permission('Articles.Articles.View', 'c', 'PermissionCategoryID', 'ArticleCategory')
         ->EndWhereGroup()
         ->OrWhere('AllowArticles', '0')
         ->OrderBy('TreeLeft', 'asc');
         
         // Note: we are using the Nested Set tree model, so TreeLeft is used for sorting.
         // Ref: http://articles.sitepoint.com/article/hierarchical-data-database/2
         // Ref: http://en.wikipedia.org/wiki/Nested_set_model
         
      $CategoryData = $this->SQL->Get();
      $this->AddCategoryColumns($CategoryData);
      return $CategoryData;
   }
   
   /**
    * Protected Function _SetTree()
    *
    * @since 2.0.18
    * @access protected
    * @param array $Node
    * @param int $Left
    * @param int $Depth
    */
   protected function _SetTree(&$Node, $Left = 1, $Depth = 0) {
      $Right = $Left + 1;
      
      if (isset($Node['Children'])) {
         foreach ($Node['Children'] as &$Child) {
            $Right = $this->_SetTree($Child, $Right, $Depth + 1);
            $Child['ParentCategoryID'] = $Node['CategoryID'];
            if ($Child['PermissionCategoryID'] != $Child['CategoryID']) {
               $Child['PermissionCategoryID'] = GetValue('PermissionCategoryID', $Node, $Child['CategoryID']);
            }
         }
         unset($Node['Children']);
      }

      $Node['TreeLeft'] = $Left;
      $Node['TreeRight'] = $Right;
      $Node['Depth'] = $Depth;

      return $Right + 1;
   }
   
   public function SetField($ID, $Property, $Value = FALSE) {
      if (!is_array($Property))
         $Property = array($Property => $Value);
      
      $this->SQL->Put($this->Name, $Property, array('CategoryID' => $ID));
      
      // Set the cache.
      self::SetCache($ID, $Property);

		return $Property;
   }
   
   public function SetRecentPost($CategoryID) {
      $Row = $this->SQL->GetWhere('Article', array('CategoryID' => $CategoryID), 'DateLastComment', 'desc', 1)->FirstRow(DATASET_TYPE_ARRAY);
      
      $Fields = array('LastCommentID' => NULL, 'LastArticleID' => NULL);
      
      if ($Row) {
         $Fields['LastCommentID'] = $Row['LastCommentID'];
         $Fields['LastArticleID'] = $Row['ArticleID'];
      }
      $this->SetField($CategoryID, $Fields);
      $this->SetCache($CategoryID, array('LastTitle' => NULL, 'LastUserID' => NULL, 'LastDateInserted' => NULL, 'LastUrl' => NULL));
   }
   
   public static function DefaultCategory() {
      foreach (self::Categories() as $Category) {
         if ($Category['CategoryID'] > 0)
            return $Category;
      }
   }
   
   /**
    * Grab and update the category cache
    * 
    * @since 2.0.18
    * @access public
    * @param int $ID
    * @param array $Data
    */
   public static function SetCache($ID = FALSE, $Data = FALSE) {
      $Categories = Gdn::Cache()->Get(self::CACHE_KEY);
      self::$Categories = NULL;
      
      if (!$Categories)
         return;
      
      if (!$ID || !is_array($Categories)) {
         Gdn::Cache()->Remove(self::CACHE_KEY);
         return;
      }
      
      if (!array_key_exists($ID, $Categories)) {
         Gdn::Cache()->Remove(self::CACHE_KEY);
         return;
      }
      
      $Category = $Categories[$ID];
      $Category = array_merge($Category, $Data);
      $Categories[$ID] = $Category;
      
      self::$Categories = $Categories;
      unset($Categories);
      self::BuildCache();
      self::JoinUserData(self::$Categories, TRUE);
   }
   
   /**
    * Get watched category IDs.
    * 
    * @since 2.0.18
    * @access public
    * @return array Category IDs.
    */
   public static function CategoryWatch() {
      $Categories = self::Categories();
      $AllCount = count($Categories);
      
      $Watch = array();
      
      foreach ($Categories as $CategoryID => $Category) {
         if ($Category['PermsArticlesView'] && !GetValue('HideAllArticles', $Category)) {
            $Watch[] = $CategoryID;
         }
      }

      Gdn::PluginManager()->EventArguments['CategoryIDs'] =& $Watch;
      Gdn::PluginManager()->FireEvent('CategoryWatch');
      
      if ($AllCount == count($Watch))
         return TRUE;

      return $Watch;
   }
   
   /**
    * Get list of article categories (disregarding user permission for admins).
    * 
    * @since 2.0.0
    * @access public
    *
    * @param string $OrderFields Ignored.
    * @param string $OrderDirection Ignored.
    * @param int $Limit Ignored.
    * @param int $Offset Ignored.
    * @return object SQL results.
    */
   public function GetAll() {
      $CategoryData = $this->SQL
         ->Select('c.*')
         ->From('ArticleCategory c')
         ->OrderBy('TreeLeft', 'asc')
         ->Get();
         
      $this->AddCategoryColumns($CategoryData);
      return $CategoryData;
   }
   
   /**
    * Delete a single category and assign its articles to another.
    * 
    * @since 2.0.0
    * @access public
    *
    * @param object $Category
    * @param int $ReplacementCategoryID Unique ID of category all articles are being move to.
    */
   public function Delete($Category, $ReplacementCategoryID) {
      // Don't do anything if the required category object & properties are not defined.
      if (
         !is_object($Category)
         || !property_exists($Category, 'CategoryID')
         || !property_exists($Category, 'ParentCategoryID')
         || !property_exists($Category, 'AllowArticles')
         || !property_exists($Category, 'Name')
         || $Category->CategoryID <= 0
      ) {
         throw new Exception(T('Invalid category for deletion.'));
      } else {
         // Remove permissions related to category
         $PermissionModel = Gdn::PermissionModel();
         $PermissionModel->Delete(NULL, 'ArticleCategory', 'CategoryID', $Category->CategoryID);
         
         // If there is a replacement category...
         if ($ReplacementCategoryID > 0) {
            // Update children categories
            $this->SQL
               ->Update('ArticleCategory')
               ->Set('ParentCategoryID', $ReplacementCategoryID)
               ->Where('ParentCategoryID', $Category->CategoryID)
               ->Put();

            // Update permission categories.
            $this->SQL
               ->Update('ArticleCategory')
               ->Set('PermissionCategoryID', $ReplacementCategoryID)
               ->Where('PermissionCategoryID', $Category->CategoryID)
               ->Where('CategoryID <>', $Category->CategoryID)
               ->Put();
               
            // Update articles
            $this->SQL
               ->Update('ArticleCategory')
               ->Set('CategoryID', $ReplacementCategoryID)
               ->Where('CategoryID', $Category->CategoryID)
               ->Put();
               
            // Update the article count
            $Count = $this->SQL
               ->Select('ArticleID', 'count', 'ArticleCount')
               ->From('ArticleCategory')
               ->Where('CategoryID', $ReplacementCategoryID)
               ->Get()
               ->FirstRow()
               ->ArticleCount;
               
            if (!is_numeric($Count))
               $Count = 0;
               
            $this->SQL
               ->Update('ArticleCategory')->Set('CountArticles', $Count)
               ->Where('CategoryID', $ReplacementCategoryID)
               ->Put();
            
            // Update tags
            $this->SQL
               ->Update('Tag')
               ->Set('CategoryID', $ReplacementCategoryID)
               ->Where('CategoryID', $Category->CategoryID)
               ->Put();
            
            $this->SQL
               ->Update('TagArticle')
               ->Set('CategoryID', $ReplacementCategoryID)
               ->Where('CategoryID', $Category->CategoryID)
               ->Put();
         } else {
            // Delete comments in this category
            $this->SQL
               ->From('ArticleComment c')
               ->Join('Article a', 'c.ArticleID = a.ArticleID')
               ->Where('a.CategoryID', $Category->CategoryID)
               ->Delete();
               
            // Delete articles in this category
            $this->SQL->Delete('Article', array('CategoryID' => $Category->CategoryID));

            // Make inherited permission local permission
            $this->SQL
               ->Update('ArticleCategory')
               ->Set('PermissionCategoryID', 0)
               ->Where('PermissionCategoryID', $Category->CategoryID)
               ->Where('CategoryID <>', $Category->CategoryID)
               ->Put();
            
            // Delete tags
            $this->SQL->Delete('Tag', array('CategoryID' => $Category->CategoryID));
            $this->SQL->Delete('TagArticle', array('CategoryID' => $Category->CategoryID));
         }
         
         // Delete the category
         $this->SQL->Delete('ArticleCategory', array('CategoryID' => $Category->CategoryID));
      }
      // Make sure to reorganize the categories after deletes
      $this->RebuildTree();
   }
   
   public function GetFull($CategoryID = FALSE, $Permissions = FALSE) {
      // Get the current category list
      $Categories = self::Categories();
      
      // Filter out the categories we aren't supposed to view.
      if ($CategoryID && !is_array($CategoryID))
         $CategoryID = array($CategoryID);
      elseif ($this->Watching)
         $CategoryID = self::CategoryWatch();
      
      switch ($Permissions) {
         case 'Articles.Articles.Add':
            $Permissions = 'PermsArticlesAdd';
            break;
         case 'Articles.Articles.Edit':
            $Permissions = 'PermsArticlesEdit';
            break;
         default:
            $Permissions = 'PermsArticlesView';
            break;
      }
      
      $IDs = array_keys($Categories);
      foreach ($IDs as $ID) {
         if ($ID < 0)
            unset($Categories[$ID]);
         elseif (!$Categories[$ID][$Permissions])
            unset($Categories[$ID]);
         elseif (is_array($CategoryID) && !in_array($ID, $CategoryID))
            unset($Categories[$ID]);
      }
      
      foreach ($Categories as &$Category) {
         if ($Category['ParentCategoryID'] <= 0)
            self::JoinRecentChildPosts($Category, $Categories);
      }
      
      Gdn::UserModel()->JoinUsers($Categories, array('LastUserID'));
      
      $Result = new Gdn_DataSet($Categories, DATASET_TYPE_ARRAY);
      $Result->DatasetType(DATASET_TYPE_OBJECT);
      return $Result;
   }
   
   public static function JoinRecentChildPosts(&$Category = NULL, &$Categories = NULL) {
      if ($Categories === NULL)
         $Categories = &self::$Categories;
      
      if ($Category === NULL)
         $Category = &$Categories[-1];
      
      if (!isset($Category['ChildIDs']))
         return;
      
      $LastTimestamp = Gdn_Format::ToTimestamp($Category['LastDateInserted']);;
      $LastCategoryID = NULL;
      
      if ($Category['DisplayAs'] == 'Categories') {
         // This is an overview category so grab it's recent data from it's children.
         foreach ($Category['ChildIDs'] as $CategoryID) {
            if (!isset($Categories[$CategoryID]))
               continue;
            
            $ChildCategory =& $Categories[$CategoryID];
            if ($ChildCategory['DisplayAs'] == 'Categories') {
               self::JoinRecentChildPosts($ChildCategory, $Categories);
            }
            $Timestamp = Gdn_Format::ToTimestamp($ChildCategory['LastDateInserted']);
            
            if ($LastTimestamp === FALSE || $LastTimestamp < $Timestamp) {
               $LastTimestamp = $Timestamp;
               $LastCategoryID = $CategoryID;
            }
         }
         
         if ($LastCategoryID) {
            $LastCategory = $Categories[$LastCategoryID];
            
            $Category['LastCommentID'] = $LastCategory['LastCommentID'];
            $Category['LastArticleID'] = $LastCategory['LastArticleID'];
            $Category['LastDateInserted'] = $LastCategory['LastDateInserted'];
            $Category['LastTitle'] = $LastCategory['LastTitle'];
            $Category['LastUserID'] = $LastCategory['LastUserID'];
            $Category['LastArticleUserID'] = $LastCategory['LastArticleUserID'];
            $Category['LastUrl'] = $LastCategory['LastUrl'];
            $Category['LastCategoryID'] = $LastCategory['CategoryID'];
         }
      }
   }
   
   /**
    * Return a url for a category.
    * @param array $Category
    * @return string
    */
   public function ArticleCategoryUrl($Category, $Page = '', $WithDomain = TRUE) {
      if (is_string($Category))
         $Category = self::Categories($Category);
      $Category = (array)$Category;
      
      $Result = '/articles/categories/'.rawurlencode($Category['UrlCode']);
      if ($Page && $Page > 1) {
            $Result .= '/p'.$Page;
      }
      return Url($Result, $WithDomain);
   }
   
   /**
    *
    *
    * @since 2.0.18
    * @acces public
    * @param int $ID
    * @return array
    */
   public static function GetSubtree($ID) {
      $Result = array();
      $Category = self::Categories($ID);
      if ($Category) {
         $Result[$Category['CategoryID']] = $Category;
         $ChildIDs = GetValue('ChildIDs', $Category);
         
         foreach ($ChildIDs as $ChildID) {
            $Result = array_merge($Result, self::GetSubtree($ChildID));
         }
      }
      return $Result;
   }
   
   /**
    * Saves the category tree based on a provided tree array. We are using the
    * Nested Set tree model.
    * 
    * @ref http://articles.sitepoint.com/article/hierarchical-data-database/2
    * @ref http://en.wikipedia.org/wiki/Nested_set_model
    *
    * @since 2.0.16
    * @access public
    *
    * @param array $TreeArray A fully defined nested set model of the category tree. 
    */
   public function SaveTree($TreeArray) {
      /*
        TreeArray comes in the format:
      '0' ...
        'item_id' => "root"
        'parent_id' => "none"
        'depth' => "0"
        'left' => "1"
        'right' => "34"
      '1' ...
        'item_id' => "1"
        'parent_id' => "root"
        'depth' => "1"
        'left' => "2"
        'right' => "3"
      etc...
      */

      // Grab all of the categories so that permissions can be properly saved.
      $PermTree = $this->SQL->Select('CategoryID, PermissionCategoryID, TreeLeft, TreeRight, Depth, Sort, ParentCategoryID')->From('ArticleCategory')->Get();
      $PermTree = $PermTree->Index($PermTree->ResultArray(), 'CategoryID');

      // The tree must be walked in order for the permissions to save properly.
      usort($TreeArray, array('ArticleCategoryModel', '_TreeSort'));
      $Saves = array();
      
      foreach($TreeArray as $I => $Node) {
         $CategoryID = GetValue('item_id', $Node);
         if ($CategoryID == 'root')
            $CategoryID = -1;
            
         $ParentCategoryID = GetValue('parent_id', $Node);
         if (in_array($ParentCategoryID, array('root', 'none')))
            $ParentCategoryID = -1;

         $PermissionCategoryID = GetValueR("$CategoryID.PermissionCategoryID", $PermTree, 0);
         $PermCatChanged = FALSE;
         if ($PermissionCategoryID != $CategoryID) {
            // This category does not have custom permissions so must inherit its parent's permissions.
            $PermissionCategoryID = GetValueR("$ParentCategoryID.PermissionCategoryID", $PermTree, 0);
            if ($CategoryID != -1 && !GetValueR("$ParentCategoryID.Touched", $PermTree)) {
               throw new Exception("Category $ParentCategoryID not touched before touching $CategoryID.");
            }
            if ($PermTree[$CategoryID]['PermissionCategoryID'] != $PermissionCategoryID)
               $PermCatChanged = TRUE;
            $PermTree[$CategoryID]['PermissionCategoryID'] = $PermissionCategoryID;
         }
         $PermTree[$CategoryID]['Touched'] = TRUE;

         // Only update if the tree doesn't match the database.
         $Row = $PermTree[$CategoryID];
         if ($Node['left'] != $Row['TreeLeft'] || $Node['right'] != $Row['TreeRight'] || $Node['depth'] != $Row['Depth'] || $ParentCategoryID != $Row['ParentCategoryID'] || $Node['left'] != $Row['Sort'] || $PermCatChanged) {
            $Set = array(
                  'TreeLeft' => $Node['left'],
                  'TreeRight' => $Node['right'],
                  'Depth' => $Node['depth'],
                  'Sort' => $Node['left'],
                  'ParentCategoryID' => $ParentCategoryID,
                  'PermissionCategoryID' => $PermissionCategoryID
               );
            
            $this->SQL->Update(
               'ArticleCategory',
               $Set,
               array('CategoryID' => $CategoryID)
            )->Put();
            
            $Saves[] = array_merge(array('CategoryID' => $CategoryID), $Set);
         }
      }
      self::ClearCache();
      return $Saves;
   }
   
   /**
    * Utility method for sorting via usort.
    *
    * @since 2.0.18
    * @access protected
    * @param $A First element to compare.
    * @param $B Second element to compare.
    * @return int -1, 1, 0 (per usort)
    */
   protected function _TreeSort($A, $B) {
      if ($A['left'] > $B['left'])
         return 1;
      elseif ($A['left'] < $B['left'])
         return -1;
      else
         return 0;
   }
   
   public static function ClearCache() {
      Gdn::Cache()->Remove(self::CACHE_KEY);
   }
   
   public static function MakeTree($Categories, $Root = NULL) {
      $Result = array();
      
      $Categories = (array)$Categories;
      
      if ($Root) {
         $Root = (array)$Root;
         // Make the tree out of this category as a subtree.
         $DepthAdjust = C('Articles.Categories.DoHeadings') ? -$Root['Depth'] : 0;
         $Result = self::_MakeTreeChildren($Root, $Categories, $DepthAdjust);
      } else {
         // Make a tree out of all categories.
         foreach ($Categories as $Category) {
            if (isset($Category['Depth']) && $Category['Depth'] == 1) {
               $Row = $Category;
               $Row['Children'] = self::_MakeTreeChildren($Row, $Categories);
               $Result[] = $Row;
            }
         }
      }
      return $Result;
   }
   
   protected static function _MakeTreeChildren($Category, $Categories, $DepthAdj = null) {
      if (is_null($DepthAdj))
         $DepthAdjust = C('Articles.Categories.DoHeadings') ? -1 : 0;
      $Result = array();
      foreach ($Category['ChildIDs'] as $ID) {
         if (!isset($Categories[$ID]))
            continue;
         $Row = $Categories[$ID];
         $Row['Depth'] += $DepthAdj;
         $Row['Children'] = self::_MakeTreeChildren($Row, $Categories);
         $Result[] = $Row;
      }
      return $Result;
   }
}
