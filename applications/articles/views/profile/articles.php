<?php if (!defined('APPLICATION')) exit();

echo '<div class="DataListWrap">';
echo '<h2 class="H">'.T('Articles').'</h2>';
echo '<ul class="DataList Articles">';

// Create some variables so that they aren't defined in every loop.
//$ViewLocation = $this->FetchViewLocation('articles', 'articles', 'articles');

if(!is_object($this->ArticleData) || $this->ArticleData->NumRows() <= 0) {
   echo Wrap(T("This user has not made any articles yet."), 'li', array('Class' => 'Item Empty'));
} else {
   //include($ViewLocation); 
   echo $this->Pager->ToString('more');
}
echo '</ul>';
echo '</div>';
