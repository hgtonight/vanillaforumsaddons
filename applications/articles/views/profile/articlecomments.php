<?php if (!defined('APPLICATION')) exit();

echo '<div class="DataListWrap">';
echo '<h2 class="H">'.T('Article Comments').'</h2>';
echo '<ul class="DataList SearchResults">';

if(sizeof($this->Data('ArticleComments'))) {
   foreach($this->Data('ArticleComments') as $Comment) {
      $Permalink = '/article/articlecomment/'.$Comment->CommentID.'/#Comment_'.$Comment->CommentID;
      $User = UserBuilder($Comment, 'Insert');
      $this->EventArguments['User'] = $User;
?>
      <li id="<?php echo 'Comment_'.$Comment->CommentID; ?>" class="Item">
         <?php $this->FireEvent('BeforeItemContent'); ?>
         <div class="ItemContent">
            <div class="Message"><?php
               echo SliceString(Gdn_Format::Text(Gdn_Format::To($Comment->Body, $Comment->Format), FALSE), 250);
            ?></div>
            <div class="Meta">
               <span class="MItem"><?php echo T('Article Comment in', 'in').' '; ?><b><?php echo Anchor(Gdn_Format::Text($Comment->DiscussionName), $Permalink); ?></b></span>
               <span class="MItem"><?php printf(T('Article Comment by %s'), UserAnchor($User)); ?></span>
               <span class="MItem"><?php echo Anchor(Gdn_Format::Date($Comment->DateInserted), $Permalink); ?></span>
            </div>
         </div>
      </li>
<?php
   }
   echo $this->Pager->ToString('more');
} else {
   echo '<li class="Item Empty">'.T('This user has not commented on any articles yet.').'</li>';
}

echo '</ul>';
echo '</div>';
