<?php if (!defined('APPLICATION')) exit();
$Session = Gdn::Session();

$CancelUrl = $this->Data('_CancelUrl');
if (!$CancelUrl) {
   $CancelUrl = '/articles/settings/all';
}
?>
<div id="ArticleForm" class="FormTitleWrapper ArticleForm">
   <?php
		if ($this->DeliveryType() == DELIVERY_TYPE_ALL)
			echo Wrap($this->Data('Title'), 'h1', array('class' => 'H'));
	
      if(!C('Articles.Article.SummaryRequired', FALSE)) { 
         echo '<div class="Info">';
         echo 'Article summary is optional.';
         echo '</div>';
      }
   
      echo $this->Form->Open();
      echo $this->Form->Errors();

      echo '<ul>';
      
      echo '<li>';
         echo '<div class="Category">';
         echo $this->Form->Label('Category', 'CategoryID'), ' ';
         echo $this->ArticleCategoryDropDown('CategoryID', array('Value' => GetValue('CategoryID', $this->Category)));
         echo '</div>';
      echo '</li>';
      
      echo '<li>';
			echo $this->Form->Label('Article Title', 'Name');
			echo Wrap($this->Form->TextBox('Name', array('maxlength' => 100, 'class' => 'InputBox BigInput')), 'div', array('class' => 'TextBoxWrapper'));
		echo '</li>';
      
      echo '<li id="UrlCode">';
         echo Wrap(T('Article Url:'), 'strong');
         echo ' ';
         echo Wrap(htmlspecialchars($this->Form->GetValue('UrlCode')));
         echo $this->Form->TextBox('UrlCode');
         echo Anchor(T('edit'), '#', 'Edit');
         echo Anchor(T('OK'), '#', 'Save SmallButton');
		echo '</li>';
      
		echo '<li>';
         echo $this->Form->BodyBox('Body', array('Table' => 'Article'));
		echo '</li>';
      
		echo '<li>';
			echo $this->Form->Label('Article Summary', 'Summary');
         echo $this->Form->BodyBox('Summary', array('Table' => 'Article'));
		echo '</li>';

      $Options = '';
      // If the user has any of the following permissions (regardless of junction), show the options
      // Note: I need to validate that they have permission in the specified category on the back-end
      // TODO: hide these boxes depending on which category is selected in the dropdown above.
      if ($Session->CheckPermission('Articles.Articles.Announce')) {
         $Options .= '<li>'.$this->CheckOrRadio('Announce', 'Announce', $this->Data('_AnnounceOptions')).'</li>';
      }

//      if ($Session->CheckPermission('Articles.Articles.Close'))
//         $Options .= '<li>'.$this->Form->CheckBox('Closed', T('Close'), array('value' => '1')).'</li>';

		$this->EventArguments['Options'] = &$Options;
		$this->FireEvent('ArticleFormOptions');

      if ($Options != '') {
			echo '<li class="P">';
	         echo '<ul class="List Inline PostOptions">' . $Options .'</ul>';
			echo '</li>';
      }
      
      $this->FireEvent('AfterArticleFormOptions');

      echo '<div class="Buttons">';
      echo $this->Form->Button((property_exists($this, 'Article')) ? 'Save' : 'Post Article', array('class' => 'Button Primary ArticleButton'));
      if (!property_exists($this, 'Article') || !is_object($this->Article) || (property_exists($this, 'Draft') && is_object($this->Draft))) {
         echo $this->Form->Button('Save Draft', array('class' => 'Button DraftButton'));
      }
      echo $this->Form->Button('Preview', array('class' => 'Button PreviewButton'));
      echo Anchor(T('Cancel'), $CancelUrl, 'Button Cancel');
      echo '</div>';
      
      echo '</ul>';
      
      echo $this->Form->Close();
   ?>
</div>
