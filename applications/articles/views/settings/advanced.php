<?php if (!defined('APPLICATION')) exit(); ?>
<h1><?php echo T('Articles: Advanced Settings'); ?></h1>
<?php
echo $this->Form->Open();
echo $this->Form->Errors();
?>
<ul>
   <li>
      <?php
         $Options = array('10' => '10', '15' => '15', '20' => '20', '25' => '25', '30' => '30', '40' => '40', '50' => '50', '100' => '100');
         $Fields = array('TextField' => 'Code', 'ValueField' => 'Code');
         echo $this->Form->Label('Articles per Page', 'Articles.Articles.PerPage');
         echo $this->Form->DropDown('Articles.Articles.PerPage', $Options, $Fields);
      ?>
   </li>
   <li>
      <?php
         echo $this->Form->Label('Comments per Page', 'Articles.Comments.PerPage');
         echo $this->Form->DropDown('Articles.Comments.PerPage', $Options, $Fields);
      ?>
   </li>
</ul>
<?php echo $this->Form->Close('Save');