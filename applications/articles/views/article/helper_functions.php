<?php if (!defined('APPLICATION')) exit();

/**
 * Format content of comment or article.
 *
 * Event argument for $Object will be 'Comment' or 'Article'.
 *
 * @since 2.1
 * @param DataSet $Object Comment or article.
 * @return string Parsed body.
 */
if (!function_exists('FormatBody')):
function FormatBody($Object) {
   Gdn::Controller()->FireEvent('BeforeCommentBody'); 
   $Object->FormatBody = Gdn_Format::To($Object->Body, $Object->Format);
   Gdn::Controller()->FireEvent('AfterCommentFormat');
   
   //return $Object->FormatBody;
   return $Object->Body;
}
endif;

/**
 * Output link to (un)boomark a article.
 */
if (!function_exists('WriteBookmarkLink')):
function WriteBookmarkLink() {
   if (!Gdn::Session()->IsValid())
      return '';
   
   $Article = Gdn::Controller()->Data('Article');

   // Bookmark link
   $Title = T($Article->Bookmarked == '1' ? 'Unbookmark' : 'Bookmark');
   echo Anchor(
      $Title,
      '/article/bookmark/'.$Article->ArticleID.'/'.Gdn::Session()->TransientKey().'?Target='.urlencode(Gdn::Controller()->SelfUrl),
      'Hijack Bookmark' . ($Article->Bookmarked == '1' ? ' Bookmarked' : ''),
      array('title' => $Title)
   );
}
endif;

/**
 * Outputs a formatted comment.
 *
 * Prior to 2.1, this also output the article ("FirstComment") to the browser.
 * That has moved to the article.php view.
 * 
 * @param DataSet $Comment.
 * @param Gdn_Controller $Sender.
 * @param Gdn_Session $Session.
 * @param int $CurrentOffet How many comments into the article we are (for anchors).
 */
if (!function_exists('WriteComment')):
function WriteComment($Comment, $Sender, $Session, $CurrentOffset) {
   static $UserPhotoFirst = NULL;
   if ($UserPhotoFirst === NULL)
      $UserPhotoFirst = C('Articles.Comment.UserPhotoFirst', TRUE);
   $Author = Gdn::UserModel()->GetID($Comment->InsertUserID); //UserBuilder($Comment, 'Insert');
   $Permalink = GetValue('Url', $Comment, '/article/comment/'.$Comment->CommentID.'/#Comment_'.$Comment->CommentID);

   // Set CanEditComments (whether to show checkboxes)
   if (!property_exists($Sender, 'CanEditComments'))
		$Sender->CanEditComments = $Session->CheckPermission('Articles.Comments.Edit', TRUE, 'Category', 'any') && C('Articles.AdminCheckboxes.Use');
   
   // Prep event args
   $CssClass = CssClass($Comment, $CurrentOffset);
   $Sender->EventArguments['Comment'] = &$Comment;
   $Sender->EventArguments['Author'] = &$Author;
   $Sender->EventArguments['CssClass'] = &$CssClass;
   
   // First comment template event
   $Sender->FireEvent('BeforeCommentDisplay'); ?>
<li class="<?php echo $CssClass; ?>" id="<?php echo 'Comment_'.$Comment->CommentID; ?>">
   <div class="Comment">
      
      <?php
      // Write a stub for the latest comment so it's easy to link to it from outside.
      if ($CurrentOffset == Gdn::Controller()->Data('_LatestItem')) {
         echo '<span id="latest"></span>';
      }
      ?>
      <div class="Options">
         <?php WriteCommentOptions($Comment); ?>
      </div>
      <?php $Sender->FireEvent('BeforeCommentMeta'); ?>
      <div class="Item-Header CommentHeader">
         <div class="AuthorWrap">
            <span class="Author">
               <?php
               if ($UserPhotoFirst) {
                  echo UserPhoto($Author);
                  echo UserAnchor($Author, 'Username');
               } else {
                  echo UserAnchor($Author, 'Username');
                  echo UserPhoto($Author);
               }
               echo FormatMeAction($Comment);
               $Sender->FireEvent('AuthorPhoto'); 
               ?>
            </span>
            <span class="AuthorInfo">
               <?php
               echo ' '.WrapIf(htmlspecialchars(GetValue('Title', $Author)), 'span', array('class' => 'MItem AuthorTitle'));
               echo ' '.WrapIf(htmlspecialchars(GetValue('Location', $Author)), 'span', array('class' => 'MItem AuthorLocation'));
               $Sender->FireEvent('AuthorInfo'); 
               ?>
            </span>   
         </div>
         <div class="Meta CommentMeta CommentInfo">
            <span class="MItem DateCreated">
               <?php echo Anchor(Gdn_Format::Date($Comment->DateInserted, 'html'), $Permalink, 'Permalink', array('name' => 'Item_'.($CurrentOffset), 'rel' => 'nofollow')); ?>
            </span>
            <?php
               echo DateUpdated($Comment, array('<span class="MItem">', '</span>'));
            ?>
            <?php
            // Include source if one was set
            if ($Source = GetValue('Source', $Comment))
               echo Wrap(sprintf(T('via %s'), T($Source.' Source', $Source)), 'span', array('class' => 'MItem Source'));

            $Sender->FireEvent('CommentInfo');

            // Include IP Address if we have permission
            if ($Session->CheckPermission('Garden.PersonalInfo.View')) 
               echo Wrap(IPAnchor($Comment->InsertIPAddress), 'span', array('class' => 'MItem IPAddress'));

            ?>
         </div>
      </div>
      <div class="Item-BodyWrap">
         <div class="Item-Body">
            <div class="Message">
               <?php 
                  echo FormatBody($Comment);
               ?>
            </div>
            <?php 
            $Sender->FireEvent('AfterCommentBody');
            WriteReactions($Comment);
            ?>
         </div>
      </div>
   </div>
</li>
<?php
	$Sender->FireEvent('AfterComment');
}
endif;

if (!function_exists('WriteReactions')):
function WriteReactions($Row, $Type = 'Comment') {
   list($RecordType, $RecordID) = RecordType($Row);
   
   Gdn::Controller()->EventArguments['RecordType'] = strtolower($RecordType);
   Gdn::Controller()->EventArguments['RecordID'] = $RecordID;
   
   echo '<div class="Reactions">';
      Gdn_Theme::BulletRow();
      Gdn::Controller()->FireEvent('AfterFlag');
   
      Gdn::Controller()->FireEvent('AfterReactions');
   echo '</div>';
   Gdn::Controller()->FireEvent('Replies');
}
endif;

/**
 * Get options for the current article.
 *
 * @since 2.1
 * @param DataSet $Article.
 * @return array $Options Each element must include keys 'Label' and 'Url'.
 */
if (!function_exists('GetArticleOptions')):
function GetArticleOptions($Article = NULL) {
   $Options = array();
   
   $Sender = Gdn::Controller();
   $Session = Gdn::Session();
   
   if ($Article == NULL)
      $Article = $Sender->Data('Article');
	
	$CategoryID = GetValue('CategoryID', $Article);
	if(!$CategoryID && property_exists($Sender, 'Article'))
		$CategoryID = GetValue('CategoryID', $Sender->Article);
   $PermissionCategoryID = GetValue('PermissionCategoryID', $Article, GetValue('PermissionCategoryID', $Article));
   
   // Determine if we still have time to edit
   $EditContentTimeout = C('Garden.EditContentTimeout', -1);
	$CanEdit = $EditContentTimeout == -1 || strtotime($Article->DateInserted) + $EditContentTimeout > time();
   $CanEdit = ($CanEdit && $Session->UserID == $Article->InsertUserID) || $Session->CheckPermission('Articles.Articles.Edit', TRUE, 'Category', $PermissionCategoryID);
   
	$TimeLeft = '';
   
	if ($CanEdit && $EditContentTimeout > 0 && !$Session->CheckPermission('Articles.Articles.Edit', TRUE, 'Category', $PermissionCategoryID)) {
		$TimeLeft = strtotime($Article->DateInserted) + $EditContentTimeout - time();
		$TimeLeft = $TimeLeft > 0 ? ' ('.Gdn_Format::Seconds($TimeLeft).')' : '';
	}
	
	// Build the $Options array based on current user's permission.
   // Can the user edit the article?
   if ($CanEdit)
      $Options['EditArticle'] = array('Label' => T('Edit').' '.$TimeLeft, 'Url' => '/articles/settings/editarticle/'.$Article->ArticleID);

   // Can the user announce?
   if ($Session->CheckPermission('Articles.Articles.Announce', TRUE, 'Category', $PermissionCategoryID))
      $Options['AnnounceArticle'] = array('Label' => T('Announce...'), 'Url' => 'vanilla/article/announce?articleid='.$Article->ArticleID.'&Target='.urlencode($Sender->SelfUrl.'#Head'), 'Class' => 'Popup');

   // Can the user close?
   if ($Session->CheckPermission('Articles.Articles.Close', TRUE, 'Category', $PermissionCategoryID)) {
      $NewClosed = (int)!$Article->Closed;
      $Options['CloseArticle'] = array('Label' => T($Article->Closed ? 'Reopen' : 'Close'), 'Url' => "/article/close?articleid={$Article->ArticleID}&close=$NewClosed", 'Class' => 'Hijack');
   }
      
   if ($CanEdit && GetValueR('Attributes.ForeignUrl', $Article)) {
      $Options['RefetchPage'] = array('Label' => T('Refetch Page'), 'Url' => '/article/refetchpageinfo.json?articleid='.$Article->ArticleID, 'Class' => 'Hijack');
   }

   // Can the user delete?
   if ($Session->CheckPermission('Articles.Articles.Delete', TRUE, 'Category', $PermissionCategoryID)) {
      $Category = ArticleCategoryModel::Categories($CategoryID);
      
      $Options['DeleteArticle'] = array('Label' => T('Delete Article'), 'Url' => '/article/delete?articleid='.$Article->ArticleID.'&target='.urlencode(CategoryUrl($Category)), 'Class' => 'Popup');
   }
   
   // Allow plugins to add options.
   $Sender->EventArguments['ArticleOptions'] = &$Options;
   $Sender->EventArguments['Article'] = $Article;
   $Sender->FireEvent('ArticleOptions');
   
   return $Options;
}
endif;

/**
 * Output moderation checkbox.
 *
 * @since 2.1
 */
if (!function_exists('WriteAdminCheck')):
function WriteAdminCheck($Object = NULL) {
   if (!Gdn::Controller()->CanEditComments || !C('Articles.AdminCheckboxes.Use'))
      return;
   
   echo '<span class="AdminCheck"><input type="checkbox" name="Toggle"></span>';
}
endif;

/**
 * Output article options.
 *
 * @since 2.1
 */
if (!function_exists('WriteArticleOptions')):
function WriteArticleOptions($Article = NULL) {
   $Options = GetArticleOptions($Article);
   
   if (empty($Options))
      return; 

   echo ' <span class="ToggleFlyout OptionsMenu">';
      echo '<span class="OptionsTitle" title="'.T('Options').'">'.T('Options').'</span>';
		echo Sprite('SpFlyoutHandle', 'Arrow'); 
      echo '<ul class="Flyout MenuItems" style="display: none;">';
      foreach ($Options as $Code => $Option):
			echo Wrap(Anchor($Option['Label'], $Option['Url'], GetValue('Class', $Option, $Code)), 'li');
		endforeach;
      echo '</ul>';
   echo '</span>';
}
endif;

/**
 * Get comment options.
 *
 * @since 2.1
 * @param DataSet $Comment.
 * @return array $Options Each element must include keys 'Label' and 'Url'.
 */
if (!function_exists('GetCommentOptions')):
function GetCommentOptions($Comment) {
	$Options = array();
   
   if (!is_numeric(GetValue('CommentID', $Comment)))
      return $Options;
   
   $Sender = Gdn::Controller();
   $Session = Gdn::Session();
	$Article = Gdn::Controller()->Data('Article');
	
	$CategoryID = GetValue('CategoryID', $Article);
   $PermissionCategoryID = GetValue('PermissionCategoryID', $Article);
   
   // Determine if we still have time to edit
   $EditContentTimeout = C('Garden.EditContentTimeout', -1);
	$CanEdit = $EditContentTimeout == -1 || strtotime($Comment->DateInserted) + $EditContentTimeout > time();
	$TimeLeft = '';
	if ($CanEdit && $EditContentTimeout > 0 && !$Session->CheckPermission('Articles.Articles.Edit', TRUE, 'Category', $PermissionCategoryID)) {
		$TimeLeft = strtotime($Comment->DateInserted) + $EditContentTimeout - time();
		$TimeLeft = $TimeLeft > 0 ? ' ('.Gdn_Format::Seconds($TimeLeft).')' : '';
	}
	
	// Can the user edit the comment?
	if (($CanEdit && $Session->UserID == $Comment->InsertUserID) || $Session->CheckPermission('Articles.Comments.Edit', TRUE, 'Category', $PermissionCategoryID))
		$Options['EditComment'] = array('Label' => T('Edit').' '.$TimeLeft, 'Url' => '/vanilla/post/editcomment/'.$Comment->CommentID, 'EditComment');

	// Can the user delete the comment?
	// if (($CanEdit && $Session->UserID == $Comment->InsertUserID) || $Session->CheckPermission('Articles.Comments.Delete', TRUE, 'Category', $PermissionCategoryID))
   if ($Session->CheckPermission('Articles.Comments.Delete', TRUE, 'Category', $PermissionCategoryID))
		$Options['DeleteComment'] = array('Label' => T('Delete'), 'Url' => 'vanilla/article/deletecomment/'.$Comment->CommentID.'/'.$Session->TransientKey().'/?Target='.urlencode("/article/{$Comment->ArticleID}/x"), 'Class' => 'DeleteComment');

   // Allow plugins to add options
   $Sender->EventArguments['CommentOptions'] = &$Options;
   $Sender->EventArguments['Comment'] = $Comment;
   $Sender->FireEvent('CommentOptions');
   
	return $Options;
}
endif;
/**
 * Output comment options.
 *
 * @since 2.1
 * @param DataSet $Comment.
 */
if (!function_exists('WriteCommentOptions')):
function WriteCommentOptions($Comment) {
	$Controller = Gdn::Controller();
	$Session = Gdn::Session();
	
   $Id = $Comment->CommentID;
	$Options = GetCommentOptions($Comment);
	if (empty($Options))
		return;

   echo '<span class="ToggleFlyout OptionsMenu">';
      echo '<span class="OptionsTitle" title="'.T('Options').'">'.T('Options').'</span>';
		echo Sprite('SpFlyoutHandle', 'Arrow'); 
      echo '<ul class="Flyout MenuItems">';
      foreach ($Options as $Code => $Option):
         echo Wrap(Anchor($Option['Label'], $Option['Url'], GetValue('Class', $Option, $Code)), 'li');
      endforeach;
      echo '</ul>';
   echo '</span>';
   if (C('Articles.AdminCheckboxes.Use')) {
      // Only show the checkbox if the user has permission to affect multiple items
      $Article = Gdn::Controller()->Data('Article');
      $PermissionCategoryID = GetValue('PermissionCategoryID', $Article);
      if ($Session->CheckPermission('Articles.Comments.Delete', TRUE, 'Category', $PermissionCategoryID)) {
         if (!property_exists($Controller, 'CheckedComments'))
            $Controller->CheckedComments = $Session->GetAttribute('CheckedComments', array());

         $ItemSelected = InSubArray($Id, $Controller->CheckedComments);
         echo '<span class="AdminCheck"><input type="checkbox" name="'.'Comment'.'ID[]" value="'.$Id.'"'.($ItemSelected?' checked="checked"':'').' /></span>';
      }
   }
}
endif;

/**
 * Output comment form.
 *
 * @since 2.1
 */
if (!function_exists('WriteCommentForm')):
function WriteCommentForm() {
	$Session = Gdn::Session();
	$Controller = Gdn::Controller();
	
	$Article = $Controller->Data('Article');
	$PermissionCategoryID = GetValue('PermissionCategoryID', $Article);
	$UserCanClose = $Session->CheckPermission('Articles.Articles.Close', TRUE, 'ArticleCategory', $PermissionCategoryID);
	$UserCanComment = $Session->CheckPermission('Articles.Comments.Add', TRUE, 'ArticleCategory', $PermissionCategoryID);
	
	// Closed notification
	if ($Article->Closed == '1') {
		?>
		<div class="Foot Closed">
			<div class="Note Closed"><?php echo T('This article has been closed.'); ?></div>
			<?php //echo Anchor(T('All Articles'), 'articles', 'TabLink'); ?>
		</div>
		<?php
	} else if (!$UserCanComment) {
      if (!Gdn::Session()->IsValid()) {
		?>
		<div class="Foot Closed">
			<div class="Note Closed SignInOrRegister"><?php 
			   $Popup =  (C('Garden.SignIn.Popup')) ? ' class="Popup"' : '';
            echo FormatString(
               T('Sign In or Register to Comment.', '<a href="{SignInUrl,html}"{Popup}>Sign In</a> or <a href="{RegisterUrl,html}">Register</a> to comment.'), 
               array(
                  'SignInUrl' => Url(SignInUrl(Url(''))),
                  'RegisterUrl' => Url(RegisterUrl(Url(''))),
                  'Popup' => $Popup
               )
            ); ?>
         </div>
			<?php //echo Anchor(T('All Articles'), 'articles', 'TabLink'); ?>
		</div>
		<?php
      }
	}
   
	if (($Article->Closed == '1' && $UserCanClose) || ($Article->Closed == '0' && $UserCanComment))
		echo $Controller->FetchView('comment', 'post');
}
endif;

if (!function_exists('WriteCommentFormHeader')):
function WriteCommentFormHeader() {
   $Session = Gdn::Session();
   if (C('Articles.Comment.UserPhotoFirst', TRUE)) {
      echo UserPhoto($Session->User);
      echo UserAnchor($Session->User, 'Username');
   } else {
      echo UserAnchor($Session->User, 'Username');
      echo UserPhoto($Session->User);
   }
}  
endif;

if (!function_exists('WriteEmbedCommentForm')):
function WriteEmbedCommentForm() {
 	$Session = Gdn::Session();
	$Controller = Gdn::Controller();
	$Article = $Controller->Data('Article');

   if ($Article && $Article->Closed == '1') { 
   ?>
   <div class="Foot Closed">
      <div class="Note Closed"><?php echo T('This article has been closed.'); ?></div>
   </div>
   <?php } else { ?>
   <h2><?php echo T('Leave a comment'); ?></h2>
   <div class="MessageForm CommentForm EmbedCommentForm">
      <?php
      echo $Controller->Form->Open(array('id' => 'Form_Comment'));
      echo $Controller->Form->Errors();
      echo $Controller->Form->Hidden('Name');
      echo Wrap($Controller->Form->TextBox('Body', array('MultiLine' => TRUE)), 'div', array('class' => 'TextBoxWrapper'));
      echo "<div class=\"Buttons\">\n";
      
      $AllowSigninPopup = C('Garden.SignIn.Popup');
      $Attributes = array('tabindex' => '-1');
      $ReturnUrl = Gdn::Request()->PathAndQuery();
      if ($Session->IsValid()) {
         $AuthenticationUrl = Gdn::Authenticator()->SignOutUrl($ReturnUrl);
         echo Wrap(
            sprintf(
               T('Commenting as %1$s (%2$s)', 'Commenting as %1$s <span class="SignOutWrap">(%2$s)</span>'),
               Gdn_Format::Text($Session->User->Name),
               Anchor(T('Sign Out'), $AuthenticationUrl, 'SignOut', $Attributes)
            ),
            'div',
            array('class' => 'Author')
         );
         echo $Controller->Form->Button('Post Comment', array('class' => 'Button CommentButton'));
      } else {
         $AuthenticationUrl = SignInUrl($ReturnUrl); 
         if ($AllowSigninPopup) {
            $CssClass = 'SignInPopup Button Stash';
         } else {
            $CssClass = 'Button Stash';
         }
         
         echo Anchor(T('Comment As ...'), $AuthenticationUrl, $CssClass, $Attributes);
      }
      echo "</div>\n";
      echo $Controller->Form->Close();
      ?>
   </div>
   <?php
   }
}
endif;

if (!function_exists('IsMeAction')):
   function IsMeAction($Row) {
      if (!C('Garden.Format.MeActions'))
         return;
      $Row = (array)$Row;
      if (!array_key_exists('Body', $Row))
         return FALSE;
      
      return strpos(trim($Row['Body']), '/me ') === 0;
   }
endif; 

if (!function_exists('FormatMeAction')):
   function FormatMeAction($Comment) {
      if (!IsMeAction($Comment))
         return;
      
      // Maxlength (don't let people blow up the forum)
      $Comment->Body = substr($Comment->Body, 4);
      $Maxlength = C('Articles.MeAction.MaxLength', 100);
      $Body = FormatBody($Comment);
      if (strlen($Body) > $Maxlength)
         $Body = substr($Body, 0, $Maxlength).'...';
      
      return '<div class="AuthorAction">'.$Body.'</div>';
   }
endif; 