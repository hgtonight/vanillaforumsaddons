<?php if (!defined('APPLICATION')) exit(); 
$UserPhotoFirst = C('Articles.Comment.UserPhotoFirst', TRUE);

$Article = $this->Data('Article');
$Author = Gdn::UserModel()->GetID($Article->InsertUserID); // UserBuilder($Article, 'Insert');

// Prep event args.
$CssClass = CssClass($Article, FALSE);
$this->EventArguments['Article'] = &$Article;
$this->EventArguments['Author'] = &$Author;
$this->EventArguments['CssClass'] = &$CssClass;

// Article template event
$this->FireEvent('BeforeArticleDisplay');
?>
<div id="<?php echo 'Article_'.$Article->ArticleID; ?>" class="<?php echo $CssClass; ?>">
   <div class="Article">
      <div class="Item-Header ArticleHeader">
         <div class="AuthorWrap">
            <span class="Author">
               <?php
               if ($UserPhotoFirst) {
                  echo UserPhoto($Author);
                  echo UserAnchor($Author, 'Username');
               } else {
                  echo UserAnchor($Author, 'Username');
                  echo UserPhoto($Author);
               }
               ?>
            </span>
            <span class="AuthorInfo">
               <?php
               echo WrapIf(htmlspecialchars(GetValue('Title', $Author)), 'span', array('class' => 'MItem AuthorTitle'));
               echo WrapIf(htmlspecialchars(GetValue('Location', $Author)), 'span', array('class' => 'MItem AuthorLocation'));
               $this->FireEvent('AuthorInfo'); 
               ?>
            </span>
         </div>
         <div class="Meta ArticleMeta">
            <span class="MItem DateCreated">
               <?php
               echo Anchor(Gdn_Format::Date($Article->DateInserted, 'html'), $Article->Url, 'Permalink', array('rel' => 'nofollow'));
               ?>
            </span>
            <?php
               echo DateUpdated($Article, array('<span class="MItem">', '</span>'));
            ?>
            <?php
            // Include source if one was set
            if ($Source = GetValue('Source', $Article))
               echo ' '.Wrap(sprintf(T('via %s'), T($Source.' Source', $Source)), 'span', array('class' => 'MItem MItem-Source')).' ';
            
            // Category
            if (C('Articles.Categories.Use')) {
               echo ' <span class="MItem Category">';
               echo ' '.T('in').' ';
               echo Anchor(htmlspecialchars($this->Data('Article.Category')), CategoryUrl($this->Data('Article.CategoryUrlCode')));
               echo '</span> ';
            }
            $this->FireEvent('ArticleInfo');
            $this->FireEvent('AfterArticleMeta'); // DEPRECATED
            ?>
         </div>
      </div>
      <?php $this->FireEvent('BeforeArticleBody'); ?>
      <div class="Item-BodyWrap">
         <div class="Item-Body">
            <div class="Message">   
               <?php
                  echo FormatBody($Article);
               ?>
            </div>
            <?php 
            $this->FireEvent('AfterArticleBody');
            WriteReactions($Article);
            ?>
         </div>
      </div>
   </div>
</div>