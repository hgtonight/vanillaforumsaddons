<?php if (!defined('APPLICATION')) exit();
$Session = Gdn::Session();
if (!function_exists('WriteArticle'))
   include($this->FetchViewLocation('helper_functions', 'articles', 'articles'));

$Alt = '';
if (property_exists($this, 'AnnounceData') && is_object($this->AnnounceData)) {
	foreach ($this->AnnounceData->Result() as $Article) {
		$Alt = $Alt == ' Alt' ? '' : ' Alt';
		WriteArticle($Article, $this, $Session, $Alt);
	}
}

$Alt = '';
foreach ($this->ArticleData->Result() as $Article) {
   $Alt = $Alt == ' Alt' ? '' : ' Alt';
   WriteArticle($Article, $this, $Session, $Alt);
}
