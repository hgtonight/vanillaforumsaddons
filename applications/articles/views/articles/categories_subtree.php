<?php

$Category = $this->Data('Category');
if (!$Category)
   return;

$SubCategories = ArticleCategoryModel::MakeTree(ArticleCategoryModel::Categories(), $Category);

if (!$SubCategories)
   return;
   
require_once $this->FetchViewLocation('categories_helper_functions', 'articles', 'articles');

?>
<h2 class="ChildCategories-Title Hidden"><?php echo T('Child Categories'); ?></h2>
<ul class="DataList ChildCategoryList">
   <?php
   foreach ($SubCategories as $Row):
      if (!$Row['PermsArticlesView'])
         continue;
      
      $Row['Depth'] = 1;
      ?>
      <li id="Category_<?php echo $Row['CategoryID']; ?>" class="Item Category">
         <div class="ItemContent Category">
            <h3 class="CategoryName TitleWrap"><?php 
               echo Anchor(htmlspecialchars($Row['Name']), $Row['Url'], 'Title');
               Gdn::Controller()->EventArguments['Category'] = $Row;
               Gdn::Controller()->FireEvent('AfterCategoryTitle'); 
            ?></h3>
            
            <?php if ($Row['Description']): ?>
            <div class="CategoryDescription">
               <?php echo $Row['Description']; ?>
            </div>
            <?php endif; ?>
            
            <div class="Meta Hidden">
               <span class="MItem MItem-Count ArticleCount"><?php
                  echo Plural(
                     $Row['CountArticles'],
                     '%s article',
                     '%s articles',
                     Gdn_Format::BigNumber($Row['CountArticles'], 'html'));
               ?></span>

               <span class="MItem MItem-Count CommentCount"><?php
                  echo Plural(
                     $Row['CountComments'],
                     '%s comment',
                     '%s comments',
                     Gdn_Format::BigNumber($Row['CountComments'], 'html'));
               ?></span>
            </div>
         </div>
      </li>
      <?php
   endforeach;
   ?>
</ul>