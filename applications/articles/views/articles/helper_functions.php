<?php if (!defined('APPLICATION')) exit();

include(PATH_APPLICATIONS.'/articles/views/article/helper_functions.php');

if (!function_exists('AdminCheck')) {
function AdminCheck($Article = NULL, $Wrap = FALSE) {
   static $UseAdminChecks = NULL;
   if ($UseAdminChecks === NULL)
      $UseAdminChecks = C('Articles.AdminCheckboxes.Use') && Gdn::Session()->CheckPermission('Garden.Moderation.Manage');

   if (!$UseAdminChecks)
      return '';

   static $CanEdits = array(), $Checked = NULL;
   $Result = '';

   if ($Article) {
      if (!isset($CanEdits[$Article->CategoryID]))
         $CanEdits[$Article->CategoryID] = GetValue('PermsArticlesEdit', ArticleCategoryModel::Categories($Article->CategoryID));



      if ($CanEdits[$Article->CategoryID]) {   
         // Grab the list of currently checked articles.
         if ($Checked === NULL) {
            $Checked = (array)Gdn::Session()->GetAttribute('CheckedArticles', array());

            if (!is_array($Checked))
               $Checked = array();
         }

         if (in_array($Article->ArticleID, $Checked))
            $ItemSelected = ' checked="checked"';
         else
            $ItemSelected = '';

         $Result = <<<EOT
<span class="AdminCheck"><input type="checkbox" name="ArticleID[]" value="{$Article->ArticleID}" $ItemSelected /></span>
EOT;
      }
   } else {
      $Result = '<span class="AdminCheck"><input type="checkbox" name="Toggle" /></span>';
   }

   if ($Wrap) {
      $Result = $Wrap[0].$Result.$Wrap[1];
   }

   return $Result;
}
}

if (!function_exists('BookmarkButton')) {
   function BookmarkButton($Article) {
      if (!Gdn::Session()->IsValid())
         return '';
      
      // Bookmark link
      $Title = T($Article->Bookmarked == '1' ? 'Unbookmark' : 'Bookmark');
      return Anchor(
         $Title,
         '/article/bookmark/'.$Article->ArticleID.'/'.Gdn::Session()->TransientKey(),
         'Hijack Bookmark' . ($Article->Bookmarked == '1' ? ' Bookmarked' : ''),
         array('title' => $Title)
      );
   }
}

if (!function_exists('CategoryLink')):
   
function CategoryLink($Article, $Prefix = ' ') {
//   if (!$Force && Gdn::Controller()->Data('Category')) {
//      return;
//   }
   $Category = ArticleCategoryModel::Categories(GetValue('CategoryID', $Article));
   
   if ($Category) {
      return Wrap($Prefix.Anchor(htmlspecialchars($Category['Name']), $Category['Url']), 'span', array('class' => 'MItem Category'));
   }
}

endif;

if (!function_exists('ArticleHeading')):
   
function ArticleHeading() {
   return T('Article');
}

endif;

if (!function_exists('WriteArticle')):
function WriteArticle($Article, &$Sender, &$Session) {
   $CssClass = CssClass($Article);
   $ArticleUrl = $Article->Url;
   $Category = ArticleCategoryModel::Categories($Article->CategoryID);
   
   if ($Session->UserID)
      $ArticleUrl .= '#latest';
   
   $Sender->EventArguments['ArticleUrl'] = &$ArticleUrl;
   $Sender->EventArguments['Article'] = &$Article;
   $Sender->EventArguments['CssClass'] = &$CssClass;
   
   $First = UserBuilder($Article, 'First');
   $Last = UserBuilder($Article, 'Last');
   $Sender->EventArguments['FirstUser'] = &$First;
   $Sender->EventArguments['LastUser'] = &$Last;
   
   $Sender->FireEvent('BeforeArticleName');
   
   $ArticleName = $Article->Name;
   if ($ArticleName == '')
      $ArticleName = T('Blank Article Topic');
      
   $Sender->EventArguments['ArticleName'] = &$ArticleName;
   
   $ArticleSummary = $Article->Summary;
   if ($ArticleSummary == '')
      $ArticleSummary = $Article->Body;
      
   $Sender->EventArguments['ArticleSummary'] = &$ArticleSummary;

   static $FirstArticle = TRUE;
   if (!$FirstArticle)
      $Sender->FireEvent('BetweenArticle');
   else
      $FirstArticle = FALSE;
      
   $Article->CountPages = ceil($Article->CountComments / $Sender->CountCommentsPerPage);
?>
<li id="Article_<?php echo $Article->ArticleID; ?>" class="<?php echo $CssClass; ?>">
   <?php
   if (!property_exists($Sender, 'CanEditArticles'))
      $Sender->CanEditArticles = GetValue('PermsArticlesEdit', ArticleCategoryModel::Categories($Article->CategoryID)) && C('Articles.AdminCheckboxes.Use');

   $Sender->FireEvent('BeforeArticleContent');

//   WriteOptions($Article, $Sender, $Session);
   ?>
   <span class="Options">
      <?php
      WriteArticleOptions($Article);
      echo BookmarkButton($Article);
      ?>
   </span>
   <div class="ItemContent Article">
      <div class="Title">
      <h2><?php 
         echo AdminCheck($Article, array('', ' ')).
            Anchor($ArticleName, $ArticleUrl);
         $Sender->FireEvent('AfterArticleTitle'); 
      ?></h2>
      </div>
      <div class="Meta Meta-Article">
         <?php 
         WriteTags($Article);
         ?>
         <span class="MItem MCount ViewCount"><?php
            printf(PluralTranslate($Article->CountViews, 
               '%s view html', '%s views html', '%s view', '%s views'),
               BigPlural($Article->CountViews, '%s view'));
         ?></span>
         <span class="MItem MCount CommentCount"><?php
            printf(PluralTranslate($Article->CountComments, 
               '%s comment html', '%s comments html', '%s comment', '%s comments'),
               BigPlural($Article->CountComments, '%s comment'));
         ?></span>
         <span class="MItem MCount ArticleScore Hidden"><?php
         $Score = $Article->Score;
         if ($Score == '') $Score = 0;
         printf(Plural($Score, 
            '%s point', '%s points',
            BigPlural($Score, '%s point')));
         ?></span>
         <?php
            echo NewComments($Article);
         
            $Sender->FireEvent('AfterCountMeta');

            if ($Article->LastCommentID != '') {
               echo ' <span class="MItem LastCommentBy">'.sprintf(T('Most recent by %1$s'), UserAnchor($Last)).'</span> ';
               echo ' <span class="MItem LastCommentDate">'.Gdn_Format::Date($Article->LastDate, 'html').'</span>';
            } else {
               echo ' <span class="MItem LastCommentBy">'.sprintf(T('Started by %1$s'), UserAnchor($First)).'</span> ';
               echo ' <span class="MItem LastCommentDate">'.Gdn_Format::Date($Article->FirstDate, 'html');
               
               if ($Source = GetValue('Source', $Article)) {
                  echo ' '.sprintf(T('via %s'), T($Source.' Source', $Source));
               }
               
               echo '</span> ';
            }
         
            if ($Sender->Data('_ShowCategoryLink', TRUE) && C('Articles.Categories.Use') && $Category)
               echo Wrap(Anchor(htmlspecialchars($Article->Category), ArticleCategoryModel::ArticleCategoryUrl($Article->CategoryUrlCode)), 'span', array('class' => 'MItem Category '.$Category['CssClass']));
               
            $Sender->FireEvent('ArticleMeta');
         ?>
      </div>
      <div>
         <?php echo $ArticleSummary; ?>
      </div>
   </div>
   <?php $Sender->FireEvent('AfterArticleContent'); ?>
</li>
<?php
}
endif;

if (!function_exists('ArticleSorter')):

function WriteArticleSorter($Selected = NULL, $Options = NULL) {
   if ($Selected === NULL) {
      $Selected = Gdn::Session()->GetPreference('Articles.SortField', 'DateLastComment');
   }
   $Selected = StringBeginsWith($Selected, 'd.', TRUE, TRUE);
   
   $Options = array(
      'DateLastComment' => T('Sort by Last Comment', 'by Last Comment'),
      'DateInserted' => T('Sort by Start Date', 'by Start Date')
   );
   
   ?>
   <span class="ToggleFlyout SelectFlyout">
   <?php
      if (isset($Options[$Selected])) {
         $Text = $Options[$Selected];
      } else {
         $Text = reset($Options);
      }
      echo Wrap($Text.' '.Sprite('', 'DropHandle'), 'span', array('class' => 'Selected'));
   ?>
   <div class="Flyout MenuItems">
      <ul>
         <?php 
            foreach ($Options as $SortField => $SortText) {
               echo Wrap(Anchor($SortText, '#', array('class' => 'SortArticles', 'data-field' => $SortField)), 'li'); 
            }
         ?>
      </ul>
   </div>
   </span>
   <?php
}

endif;

if (!function_exists('WriteMiniPager')):
function WriteMiniPager($Article) {
   if (!property_exists($Article, 'CountPages'))
      return;
   
   if ($Article->CountPages > 1) {
      echo '<span class="MiniPager">';
         if ($Article->CountPages < 5) {
            for ($i = 0; $i < $Article->CountPages; $i++) {
               WritePageLink($Article, $i+1);
            }
         } else {
            WritePageLink($Article, 1);
            WritePageLink($Article, 2);
            echo '<span class="Elipsis">...</span>';
            WritePageLink($Article, $Article->CountPages-1);
            WritePageLink($Article, $Article->CountPages);
            // echo Anchor('Go To Page', '#', 'GoToPageLink');
         }
      echo '</span>';
   }
}
endif;

if (!function_exists('WritePageLink')):
function WritePageLink($Article, $PageNumber) {
   echo Anchor($PageNumber, ArticleUrl($Article, $PageNumber));
}
endif;

if (!function_exists('NewComments')):
function NewComments($Article) {
   if (!Gdn::Session()->IsValid())
      return '';
   
   if ($Article->CountUnreadComments === TRUE) {
      $Title = htmlspecialchars(T("You haven't read this yet."));
      
      return ' <strong class="HasNew JustNew NewCommentCount" title="'.$Title.'">'.T('new article', 'new').'</strong>';
   } elseif ($Article->CountUnreadComments > 0) {
      $Title = htmlspecialchars(Plural($Article->CountUnreadComments, "%s new comment since you last read this.", "%s new comments since you last read this."));
      
      return ' <strong class="HasNew NewCommentCount" title="'.$Title.'">'.Plural($Article->CountUnreadComments, '%s new', '%s new plural', BigPlural($Article->CountUnreadComments, '%s new', '%s new plural')).'</strong>';
   }
   return '';
}
endif;

if (!function_exists('Tag')):
function Tag($Article, $Column, $Code, $CssClass = FALSE) {
   $Article = (object)$Article;
   
   if (is_numeric($Article->$Column) && !$Article->$Column)
      return '';
   if (!is_numeric($Article->$Column) && strcasecmp($Article->$Column, $Code) != 0)
      return;

   if (!$CssClass)
      $CssClass = "Tag-$Code";

   return ' <span class="Tag '.$CssClass.'" title="'.htmlspecialchars(T($Code)).'">'.T($Code).'</span> ';
}
endif;

if (!function_exists('WriteTags')):
function WriteTags($Article) {
   Gdn::Controller()->FireEvent('BeforeArticleMeta');

   echo Tag($Article, 'Announce', 'Announcement');
   echo Tag($Article, 'Closed', 'Closed');

   Gdn::Controller()->FireEvent('AfterArticleLabels');
}
endif;

if (!function_exists('WriteFilterTabs')):
function WriteFilterTabs($Sender) {
   $Session = Gdn::Session();
   $Title = property_exists($Sender, 'Category') ? GetValue('Name', $Sender->Category, '') : '';
   if ($Title == '')
      $Title = T('All Articles');
      
   $Bookmarked = T('My Bookmarks');
   $MyArticles = T('My Articles');
   $MyDrafts = T('My Drafts');
   $CountBookmarks = 0;
   $CountArticles = 0;
   $CountDrafts = 0;
   
   if ($Session->IsValid()) {
      $CountBookmarks = $Session->User->CountBookmarks;
      $CountArticles = $Session->User->CountArticles;
      $CountDrafts = $Session->User->CountDrafts;
   }
   
   if (C('Articles.Articles.ShowCounts', TRUE)) {
      $Bookmarked .= CountString($CountBookmarks, Url('/articles/UserBookmarkCount'));
      $MyArticles .= CountString($CountArticles);
      $MyDrafts .= CountString($CountDrafts);
   }
      
   ?>
<div class="Tabs ArticlesTabs">
   <?php
   if (!property_exists($Sender, 'CanEditArticles'))
      $Sender->CanEditArticles = $Session->CheckPermission('Articles.Articles.Edit', TRUE, 'Category', 'any') && C('Articles.AdminCheckboxes.Use');
   
   if ($Sender->CanEditArticles) {
   ?>
   <span class="Options"><span class="AdminCheck">
      <input type="checkbox" name="Toggle" />
   </span></span>
   <?php } ?>
   <ul>
      <?php $Sender->FireEvent('BeforeArticleTabs'); ?>
      <li<?php echo strtolower($Sender->ControllerName) == 'articlescontroller' && strtolower($Sender->RequestMethod) == 'index' ? ' class="Active"' : ''; ?>><?php echo Anchor(T('All Articles'), 'articles', 'TabLink'); ?></li>
      <?php $Sender->FireEvent('AfterAllArticlesTab'); ?>

      <?php
      if (C('Articles.Categories.ShowTabs')) {
         $CssClass = '';
         if (strtolower($Sender->ControllerName) == 'articlecategoriescontroller' && strtolower($Sender->RequestMethod) == 'all') {
            $CssClass = 'Active';
         }

         echo " <li class=\"$CssClass\">".Anchor(T('Categories'), '/articlecategories/all', 'TabLink').'</li> ';
      }
      ?>
      <?php if ($CountBookmarks > 0 || $Sender->RequestMethod == 'bookmarked') { ?>
      <li<?php echo $Sender->RequestMethod == 'bookmarked' ? ' class="Active"' : ''; ?>><?php echo Anchor($Bookmarked, '/articles/bookmarked', 'MyBookmarks TabLink'); ?></li>
      <?php
         $Sender->FireEvent('AfterBookmarksTab');
      }
      if (($CountArticles > 0 || $Sender->RequestMethod == 'mine') && C('Articles.Articles.ShowMineTab', TRUE)) {
      ?>
      <li<?php echo $Sender->RequestMethod == 'mine' ? ' class="Active"' : ''; ?>><?php echo Anchor($MyArticles, '/articles/mine', 'MyArticles TabLink'); ?></li>
      <?php
      }
      if ($CountDrafts > 0 || $Sender->ControllerName == 'draftscontroller') {
      ?>
      <li<?php echo $Sender->ControllerName == 'draftscontroller' ? ' class="Active"' : ''; ?>><?php echo Anchor($MyDrafts, '/drafts', 'MyDrafts TabLink'); ?></li>
      <?php
      }
      $Sender->FireEvent('AfterArticleTabs');
      ?>
   </ul>
</div>
   <?php
}
endif;

if (!function_exists('OptionsList')):
function OptionsList($Article) {
   throw new Exception('DEPRECATED');
      // Allow plugins to add options.
      $Sender->EventArguments['Article'] = $Article;
}

endif;


if (!function_exists('WriteOptions')):
/**
 * Render options that the user has for this article.
 */
function WriteOptions($Article) {
   if (!Gdn::Session()->IsValid() || !Gdn::Controller()->ShowOptions)
      return;
   
   
   echo '<span class="Options">';
   
   // Options list.
   WriteArticleOptions($Article);

   // Bookmark button.
   echo BookmarkButton($Article);

   // Admin check.
   echo AdminCheck($Article);

   echo '</span>';
}
endif;