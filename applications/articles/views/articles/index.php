<?php if (!defined('APPLICATION')) exit();
$Session = Gdn::Session();
include_once $this->FetchViewLocation('helper_functions', 'articles', 'articles');

echo '<h1 class="H HomepageTitle">'.
   AdminCheck(NULL, array('', ' ')).
   $this->Data('Title').
   '</h1>';

if ($Description = $this->Description()) {
   echo Wrap($Description, 'div', array('class' => 'P PageDescription'));
}

$this->FireEvent('AfterPageTitle');

include $this->FetchViewLocation('categories_subtree', 'Articles', 'Articles');


$PagerOptions = array('Wrapper' => '<span class="PagerNub">&#160;</span><div %1$s>%2$s</div>', 'RecordCount' => $this->Data('CountArticles'), 'CurrentRecords' => $this->Data('Articles')->NumRows());
if ($this->Data('_PagerUrl'))
   $PagerOptions['Url'] = $this->Data('_PagerUrl');

echo '<div class="PageControls Top">';
   PagerModule::Write($PagerOptions);
   echo Gdn_Theme::Module('NewArticleModule', $this->Data('_NewArticleProperties', array('CssClass' => 'Button Action Primary')));
echo '</div>';

if ($this->ArticleData->NumRows() > 0 || (isset($this->AnnounceData) && is_object($this->AnnounceData) && $this->AnnounceData->NumRows() > 0)) {
?>
<ul class="DataList Articles">
   <?php include($this->FetchViewLocation('articles')); ?>
</ul>
<?php
   
echo '<div class="PageControls Bottom">';
   PagerModule::Write($PagerOptions);
   echo Gdn_Theme::Module('NewArticleModule', $this->Data('_NewArticleProperties', array('CssClass' => 'Button Action Primary')));
echo '</div>';

} else {
   ?>
   <div class="Empty"><?php echo T('No articles were found.'); ?></div>
   <?php
}
