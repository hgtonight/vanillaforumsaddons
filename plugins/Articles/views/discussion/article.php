<?php if (!defined('APPLICATION')) exit(); 
$UserPhotoFirst = C('Vanilla.Comment.UserPhotoFirst', TRUE);

$Article = $this->Data('Discussion');
$Author = Gdn::UserModel()->GetID($Article->InsertUserID);

// Prep event args.
$CssClass = CssClass($Article, FALSE);
$this->EventArguments['Discussion'] = &$Article;
$this->EventArguments['Author'] = &$Author;
$this->EventArguments['CssClass'] = &$CssClass;

// Article template event
$this->FireEvent('BeforeDiscussionDisplay');
?>
<div id="<?php echo 'Article_'.$Article->ArticleID; ?>" class="<?php echo $CssClass; ?>">
   <div class="Article">
      <div class="Item-Header ArticleHeader">
         <div class="AuthorWrap">
            <span class="Author">
               <?php
               if ($UserPhotoFirst) {
                  echo UserPhoto($Author);
                  echo UserAnchor($Author, 'Username');
               } else {
                  echo UserAnchor($Author, 'Username');
                  echo UserPhoto($Author);
               }
               ?>
            </span>
            <span class="AuthorInfo">
               <?php
               echo WrapIf(htmlspecialchars(GetValue('Title', $Author)), 'span', array('class' => 'MItem AuthorTitle'));
               echo WrapIf(htmlspecialchars(GetValue('Location', $Author)), 'span', array('class' => 'MItem AuthorLocation'));
               $this->FireEvent('AuthorInfo'); 
               ?>
            </span>
         </div>
         <div class="Meta ArticleMeta">
            <span class="MItem DateCreated">
               <?php
               echo Anchor(Gdn_Format::Date($Article->DateInserted, 'html'), $Article->Url, 'Permalink', array('rel' => 'nofollow'));
               ?>
            </span>
            <?php
               echo DateUpdated($Article, array('<span class="MItem">', '</span>'));
            ?>
            <?php
            // Category
            if (C('Vanilla.Categories.Use')) {
               echo ' <span class="MItem Category">';
               echo ' '.T('in').' ';
               echo Anchor(htmlspecialchars($this->Data('Discussion.Category')), CategoryUrl($this->Data('Discussion.CategoryUrlCode')));
               echo '</span> ';
            }
            $this->FireEvent('DiscussionInfo');
            ?>
         </div>
      </div>
      <?php $this->FireEvent('BeforeDiscussionBody'); ?>
      <div class="Item-BodyWrap">
         <div class="Item-Body">
            <div class="Message">   
               <?php
                  echo FormatBody($Article);
               ?>
            </div>
            <?php 
            $this->FireEvent('AfterDiscussionBody');
            WriteReactions($Article);
            ?>
         </div>
      </div>
   </div>
</div>