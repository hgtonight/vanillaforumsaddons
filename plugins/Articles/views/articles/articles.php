<?php if (!defined('APPLICATION')) exit(); ?>

<h1>All Articles!</h1>
<?php

foreach($this->DiscussionData as $Article) {
   $ArticleID = $Article->DiscussionID;
   $Year = date('Y', strtotime($Article->DateInserted));
   $ArticleUrl = str_replace('discussion/', 'article/' . $Year . '/', $Article->Url);
   echo Anchor($Article->Name, $ArticleUrl) . '<br />';
   echo '' . $Article->Body;
}

?>