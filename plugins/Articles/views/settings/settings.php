<?php if (!defined('APPLICATION')) exit();

echo $this->Form->Open();
echo $this->Form->Errors();
?>
<h1>Articles Settings</h1>

<div class='Info'>
   <?php echo Wrap(T('Articles is a plugin that provides a way to write and publish articles through discussions in Vanilla.', '<strong>Articles</strong> is a plugin that provides a way to write and publish articles through discussions in Vanilla.'), 'div'); ?>
</div>

<h3><?php echo T('Article Categories'); ?></h3>
<ul>
   <li>
      <?php
         echo $this->Form->Label('Select categories to be used for articles.', 'Categories');
         echo $this->Form->CheckBoxList('Plugins.Articles.CategoryIDs', $this->CategoryData, NULL,
            array('TextField' => 'Name', 
                  'ValueField' => 'CategoryID'));
      ?>
   </li>
</ul>

<h3><?php echo T('Article Settings'); ?></h3>
<ul>
   <li>
      <?php
         echo $this->Form->CheckBox('Plugins.Articles.MenuLink', T('Show menu link?'));
      ?>
   </li>
   <li>
      <?php
         echo $this->Form->CheckBox('Plugins.Articles.SidebarMenuLink', T('Show sidebar menu link?'));
      ?>
   </li>
</ul>

<?php 
   echo $this->Form->Close('Save');
