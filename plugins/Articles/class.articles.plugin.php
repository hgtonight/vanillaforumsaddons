<?php if (!defined('APPLICATION')) exit();

// Define the plugin:
$PluginInfo['Articles'] = array(
   'Description' => 'Articles is a plugin that provides a way to write and publish articles through discussions in Vanilla.',
   'Version' => '0.0.1',
   'Author' => "Livid Tech",
   'AuthorUrl' => 'http://lividtech.com',
   'SettingsUrl' => 'dashboard/settings/articles',
   'SettingsPermission' => FALSE,
	'RequiredApplications' => array('Vanilla' => '2.2.3.3')
);

// To-do:
// Redirect discussion on discussions list to article.
// Setting to hide from category and discussions list.
// Post Excerpt - Structure / validation Integer if no excerpt
// Upload images.
// Modules with Smarty tags
// Handle draft display in /articles
// Handle permissions in /articles and other pages.
// Handle article/category pages OR make a route.
// Sitemaps

class ArticlesPlugin extends Gdn_Plugin {
   public function Base_Render_Before($Sender) {
      if(C('Plugins.Articles.MenuLink') && $Sender->Menu)
         $Sender->Menu->AddLink('Articles', T('Articles'), '/articles');
   }
   
   public function Base_BeforeDiscussionFilters_Handler($Sender) {
      if(C('Plugins.Articles.SidebarMenuLink')) {
         $CurrentURI = Gdn::Request()->RequestURI();
         echo '<li class="Articles' . (strpos($CurrentURI, '/articles') !== FALSE ? ' Active' : '') . '">' . Gdn_Theme::Link('articles', Sprite('SpArticles') . ' ' . T('Articles')) . '</li>';
      }
   }
   
   public function Base_GetAppSettingsMenuItems_Handler($Sender) {
      $Menu = &$Sender->EventArguments['SideMenu'];
      $Menu->AddLink('Articles', T('Settings'), 'dashboard/settings/articles', 'Articles.Settings.View');
   }
   
   public function SettingsController_Articles_Create($Sender) {
      $Sender->Title(T('Articles Settings'));
      $Sender->AddSideMenu('dashboard/settings/articles');
      $Sender->Permission('Garden.Settings.Manage');
      
      $Validation = new Gdn_Validation();
      $ConfigurationModel = new Gdn_ConfigurationModel($Validation);
      $ConfigurationModel->SetField(array(
         'Plugins.Articles.CategoryIDs',
         'Plugins.Articles.MenuLink',
         'Plugins.Articles.SidebarMenuLink'
      ));
      $Sender->Form->SetModel($ConfigurationModel);
      
      if($Sender->Form->AuthenticatedPostBack() == FALSE) {
         $Sender->Form->SetData($ConfigurationModel->Data);
      } else {
         $Data = $Sender->Form->FormValues();
         $ConfigurationModel->Validation->ApplyRule('Plugins.Articles.MenuLink', 'Boolean');
         $ConfigurationModel->Validation->ApplyRule('Plugins.Articles.SidebarMenuLink', 'Boolean');
         
         if ($Sender->Form->Save() !== FALSE)
            $Sender->InformMessage('<span class="InformSprite Sliders"></span>' . T("Your changes have been saved."), 'HasSprite');
      }
      
      $CategoryModel = new CategoryModel();
      $Sender->SetData('CategoryData', $CategoryModel->GetAll(), TRUE);
      array_shift($Sender->CategoryData->Result());
      
      $ViewFile = $this->GetView('settings/settings.php');
      $Sender->Render($ViewFile);
   }
   
   public function PluginController_Articles_Create($Sender) {
      $Sender->ClearCssFiles();
      $Sender->AddCssFile('style.css');
      $Sender->AddModule('NewDiscussionModule');
      $Sender->AddModule('DiscussionFilterModule');
      $Sender->MasterView = 'default';
      
      // Set criteria & get discussions data
      $Sender->SetData('Category', FALSE, TRUE);
      $DiscussionModel = new DiscussionModel();
      
      // Get Discussions
      $Offset = '0';
      $Limit = '';
      $Where = array('d.CategoryID' => C('Plugins.Articles.CategoryIDs'));
      $Sender->DiscussionData = $DiscussionModel->GetWhere($Where, $Offset, $Limit);
      $Sender->SetData('Discussions', $Sender->DiscussionData, TRUE);
      
      $ViewFile = $Sender->FetchViewLocation('Discussions', 'Discussions', 'Vanilla');
      $Sender->Render($ViewFile);
   }
   
   public function DiscussionController_Render_Before($Sender) {
      if(in_array($Sender->CategoryID, C('Plugins.Articles.CategoryIDs'))) {
         $CSSFile = $this->GetResource('design/articles.css', FALSE, FALSE);
         $Sender->AddCssFile($CSSFile);
         $Sender->View = $Sender->FetchViewLocation('Article', 'Discussion', 'Vanilla', FALSE);
      }
   }
   
   public function Structure() {
      $Structure = Gdn::Structure();
      $Structure
         ->Table('Discussion')
         ->Column('ArticleStatus', 'tinyint(1)', '0')
         ->Set(FALSE, FALSE);
   }
   
   public function Setup() {
      $Routes = array('^articles(/.*)?$' => 'plugin/articles$1',
                      '^article\/(\d+)\/(\d+)\/(.*)' => 'discussion/$2/$3');
      
      foreach($Routes as $Expression => $Target) {
         $Type = 'Internal';
      
         if(!Gdn::Router()->MatchRoute($Expression))
            Gdn::Router()->SetRoute($Expression, $Target, $Type);
      }
      
      //$this->Structure();
      
      SaveToConfig('Plugins.Articles.CategoryIDs', array('1'));
      SaveToConfig('Plugins.Articles.MenuLink', TRUE);
      SaveToConfig('Plugins.Articles.SidebarMenuLink', TRUE);
   }
   
   public function OnDisable() {
      $Routes = array('^articles(/.*)?$',
                          '^article\/(\d+)\/(\d+)\/(.*)');
      foreach($Routes as $Expression) {
         Gdn::Router()->DeleteRoute($Expression);
      }
      
      RemoveFromConfig('Plugins.Articles.CategoryIDs');
      RemoveFromConfig('Plugins.Articles.MenuLink');
      RemoveFromConfig('Plugins.Articles.SidebarMenuLink');
   }
}
